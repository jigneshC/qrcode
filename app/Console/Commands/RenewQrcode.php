<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Notifications\QrcodeRenew;
use App\Notifications\QrcodeCredits;
use Carbon\Carbon;
use App\User;
use App\Qrcode;
use App\Log;

class RenewQrcode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'RenewQrcode:renew_qrcode';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "renew qrcode if expired or Send Mail if user don't have enough point ";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = date('Y-m-d');
        
        $qrcodes = Qrcode::with('user')->where('expiry_date','<=',$date)->where('points','>',0)->get();
        //dd($qrcodes);
        $user_ids = array();
        
        foreach ($qrcodes as $qrcode){

            $user = User::where('id',$qrcode->user_id)->where('type','=','user')->where('renew','=','on')->first();

            if($user){

                if($user->points < $qrcode->points ){
                    //mail user to get credits to renew expired qrcodes
                    
                    $user_ids[] = $user->id;
    
                }else{
                    
                    // update the validity of qrcode & deduct the points from user's account
                    $qrcode->status = 1;
                    $qrcode->expiry_date = Carbon::now()->addYear($qrcode->years)->addMonth($qrcode->months)->addDays($qrcode->days);
                    $qrcode->save();
    
                    $log = new Log;
                    $log->user_id = $user->id;
                    $log->type = "Debit";
                    $log->points = $qrcode->points;
                    $log->save();
    
                    $points = $user->points;
                    $user->points = $points - $qrcode->points;
                    $user->save();

                    $user->notify(new QrcodeRenew($qrcode));
    
                }

            }
            
        }

        $mail_user = User::whereIn('id',$user_ids)->get();

        foreach($mail_user as $users){
            $notify_user = User::find($users->id);
            $notify_user->notify(new QrcodeCredits());
        }

        echo "Qr Codes Renewed Successfully";
        
    }
}


