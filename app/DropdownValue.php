<?php

namespace App;

use Illuminate\Contracts\Session\Session;
use Illuminate\Database\Eloquent\Model;

use App\BaseModel;
use Illuminate\Http\Request;


class DropdownValue extends BaseModel
{


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dropdown_values';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', '_website_id', 'type_id', 'active', 'created_by', 'updated_by', 'parent_id', 'lang_code'];


    /**
     * Each Dropdown value have one type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\DropdownsType', 'type_id', 'id');
    }


    public function child()
    {
        return $this->hasMany('App\DropdownValue', 'parent_id', 'id')->whereRaw('id != parent_id');
    }


    /**
     * @param $q
     * @param string $lang
     * @return mixed
     */
    public function scopeLanguage($q, $lang = 'en')
    {

        if ($lang == 'en') {
            //session
            if (session()->has('_lang')) {
                $lang = session()->get('_lang');
            } else if (\App::getLocale()) {
                $lang = \App::getLocale();
            } else {
                $lang = 'en';
            }
        }

        return $q->where('lang_code', $lang);
    }


    public static function getValue($id = null)
    {

        if (is_null($id)) {
            return '';
        }

        $data = self::whereParentId($id)->language()->first();
        if ($data) {
            return $data->name;
        }

        return '';

    }

    /**
     * @param $q
     * @param bool $yes
     * @return mixed ->language()->get();
     */
    public function scopeParent($q, $yes = true)
    {
        //return if two column values are same
        return $q->whereRaw('id = parent_id');

//        if ($yes == true) {
//            return $q->where('parent_id', '0');
//        }
//        return $q->where('parent_id', '!=', '0');
    }

}
