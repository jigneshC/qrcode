<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;

class ApiAccessController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:access.api');
    }

    public function index()
    {
        return view('admin.api_access.index');
    }


    public function postPass(Request $request)
    {
        $this->validate($request, [
            'password' => 'required'
        ]);

        $user = Auth::user();

        if (Hash::check($request->input('password'), $user->password)) {
            // The passwords match...
            return [
                'status' => true,
                'api_token' => $user->api_token
            ];
        }

        return [
            'status' => false,
            'api_token' => ''
        ];
    }


    public function regenerateToken(Request $request)
    {

        $user = \Auth::user();


        $token = User::genApiKey($user->id);

        $user->api_token = $token;

        if ($user->save()) {
            return [
                'status' => true,
                'api_token' => $token
            ];
        }

        return [
            'status' => false,
        ];
    }
}
