<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Package;
use Session;
use Auth;
use App\Language;
use Dedicated\GoogleTranslate\Translator;



class PackagesController extends Controller
{
    function __construct()
    {   
        
        $this->middleware('permission:access.package');
        $this->middleware('permission:access.packages.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.packages.create')->only(['create', 'store']);
        $this->middleware('permission:access.packages.delete')->only('destroy');

    }
    public function doTranslator($from,$to,$data)
    {
        $translator = new Translator;
        try{
            $data = $translator->setSourceLang($from)
                ->setTargetLang($to)
                ->translate($data);

        }catch (\Exception $e){
            //$desc =  $e->getMessage();
        }
        return html_entity_decode($data, ENT_QUOTES, "utf-8");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        // $lang = Auth::user()->language;
        // $keyword = $request->get('search');
        // $perPage = 10;

        // if (!empty($keyword)) {
        //     $packages = Package::where('title', 'LIKE', "%$keyword%")
        //         ->orWhere('qr_code_no', 'LIKE', "%$keyword%")    
        //         ->orWhere('expiry_year', 'LIKE', "%$keyword%")  
        //         ->orWhere('amount', 'LIKE', "%$keyword%")  
        //         ->orWhere('currency', 'LIKE', "%$keyword%")           
        //         ->paginate($perPage);
        // } else {
        //     $packages = Package::where('lang_code',$lang)->paginate($perPage);
        // }

        return view('admin.packages.index');
    }

    public function datatable(Request $request){

        $lang = Auth::user()->language;
        $packages = Package::where('lang_code',$lang)->get();

        return Datatables::of($packages)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $languages = Language::active()->where('lang_code','!=',Auth::user()->language)->get();
        
        $languages_user = Language::where('lang_code',Auth::user()->language)->first();
        
        $languages = $languages->prepend($languages_user);
        
        return view('admin.packages.create',compact('languages','languages_user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $lang = Auth::user()->language;
        $validationArr = [
            $lang .'.title' => 'required'
        ];

        $this->validate($request,$validationArr);
        
        $lang = Auth::user()->language;

        $requestData = $request->all();
        
        $requestData['lang_code'] = $lang;

        if($request->type == "Free"){
           // $requestData['expiry_year'] = 0;
           // $requestData['expiry_months'] = 0;
            $requestData['amount'] = 0;
            $requestData['qr_code_no'] = 0;
        }
        /*
        if($request->type == "Pricing"){
            if($request->expiry_year == null || $request->expiry_year == ''){
                $requestData['expiry_year'] = 0;
            }
            if($request->expiry_months == '' || $request->expiry_months == null){
                $requestData['expiry_months'] = 0;
            }
        } 
        */
        if($request->has('is_feature') && $request->is_feature > 0){
            if($request->is_feature == 1){
                Package::where('is_feature',1)->update(['is_feature' => 0]);
            }
            if($request->is_feature == 2){
                Package::where('is_feature',2)->update(['is_feature' => 0]);   
            }
           
            
        }

        $requestData['title'] = $requestData[$lang]['title'];
        $requestData['description'] = $requestData[$lang]['description'];
        $package = Package::create($requestData);

        if ($package) {
            $package_id = $package->id;
            $package->ref_id = $package_id;
            $package->save();

            Package::where('ref_id', $package_id)->update(['is_feature' => $request->is_feature ]);

            $languages = Language::active()->where('lang_code', '!=', Auth::user()->language)->get();
            
            foreach ($languages as $language) {

                $code = $language->lang_code;
                
                if ($code != $lang) {

                    if (isset($requestData[$code])) {
                       
                        $requestData['title'] = (isset($requestData[$code]['title']) && $requestData[$code]['title'] != "") ? $requestData[$code]['title'] : $this->doTranslator($lang, $code, $requestData[$lang]['title']);
                        $requestData['description'] = (isset($requestData[$code]['description']) && $requestData[$code]['description'] != "") ? $requestData[$code]['description'] : $this->doTranslator($lang, $code, $requestData[$lang]['description']);
                        $requestData['lang_code'] = $code;
                        $requestData['ref_id'] = $package_id;
        
                        Package::create($requestData);
    
                    }
                }
                
                
            }
        }
        
        Session::flash('flash_message', __('Package added!'));

        return redirect('admin/packages');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $package = Package::find($id);

        if($package == ''){
            return redirect()->back();
        }

        return view('admin.packages.show', compact('package'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lang = Auth::user()->language;

        $data = Package::where('ref_id',$id)->get();


        $package = new \stdClass();

        foreach ($data as $row) {
            $package->id = $id;
            $package->title = $row->title;
            $package->description = $row->description;
            $package->type = $row->type;
            $package->qr_code_no = $row->qr_code_no;
         //   $package->expiry_year = $row->expiry_year;
         //   $package->expiry_months = $row->expiry_months;
            $package->amount = $row->amount;
            $package->currency = $row->currency;
            $package->is_feature = $row->is_feature;
            $package->{$row->lang_code} = $row;

        }

        $languages = Language::active()->where('lang_code','!=',Auth::user()->language)->get();

        $languages_user = Language::where('lang_code',Auth::user()->language)->first();

        $languages = $languages->prepend($languages_user);
        
        return view('admin.packages.edit', compact('package','languages','languages_user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        
        $lang = Auth::user()->language;
        $validationArr = [
            $lang.'.title' => 'required'
        ];

        $this->validate($request,$validationArr);
        

        $requestData = $request->all();
        
        if($request->type == "Free"){
         //   $requestData['expiry_year'] = 0;
         //   $requestData['expiry_months'] = 0;
            $requestData['amount'] = 0;
            $requestData['currency'] = "";
            $requestData['qr_code_no'] = 0;
        }
        /*
        if($request->type == "Pricing"){
            if($request->expiry_year == null || $request->expiry_year == ''){
                $requestData['expiry_year'] = 0;
            }
            if($request->expiry_months == '' || $request->expiry_months == null){
                $requestData['expiry_months'] = 0;
            }
        }
        */
        if(!$request->has('is_feature') ) {
            
            Package::where('ref_id', $id)->update(['is_feature' => 0 ]); 
        }

        if($request->has('is_feature') && $request->is_feature > 0){
            if($request->is_feature == 1){
                Package::where('is_feature',1)->update(['is_feature' => 0]);
            }
            if($request->is_feature == 2){
                Package::where('is_feature',2)->update(['is_feature' => 0]);   
            }
            Package::where('ref_id', $id)->update(['is_feature' => $request->is_feature ]);
            
        }
        
       

         if (isset($requestData[$lang])) {

            $pk = $requestData[$lang];
            $pk['title'] = $requestData[$lang]['title'];
            $pk['description'] = html_entity_decode($requestData[$lang]['description']);
            $pk['type'] = $requestData['type'];
            $pk['qr_code_no'] = $requestData['qr_code_no'];
            $pk['amount'] = $requestData['amount'];
           // $pk['expiry_year'] = $requestData['expiry_year'];
          //  $pk['expiry_months'] = $requestData['expiry_months'];
            $pk['currency'] = $requestData['currency'];
          
            $ref_package = Package::where('ref_id', $id)->where('lang_code', $lang)->first();

            if($ref_package)
                $ref_package->update($pk);

            $languages = Language::active()->get();

            foreach ($languages as $language) {

                $code = $language->lang_code;

                if ($code != $lang) {

                    if (isset($requestData[$code])) {

                        $requestData['title'] = (isset($requestData[$code]['title']) && $requestData[$code]['title']!="")? $requestData[$code]['title'] : $requestData[$lang]['title'];
                        $requestData['description'] = (isset($requestData[$code]['description']) && $requestData[$code]['description']!="")? $requestData[$code]['description'] : $requestData[$lang]['description'];
                       
                        $requestData['lang_code'] = $code;

                        $instance = Package::where('ref_id', $id)->where('lang_code', $code)->first();
                        if($instance){
                            $instance->update($requestData);
                        }else{
                            $requestData['ref_id'] = $id;
                            Package::create($requestData);

                        }

                    }
                }

            }
        }
        
        Session::flash('flash_message', __('Package Updated!'));

        return redirect('admin/packages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //Package::destroy($id); 
        $lang = Auth::user()->language;

        $package = Package::where('lang_code',$lang)->find($id);
            
        $free_package = Package::where('lang_code',$lang)->where('type','=','Free')->count();

        $popular_package = Package::where('lang_code',$lang)->where('type','=','Pricing')->where('is_feature','=',1)->get();

        $bussiness_package = Package::where('lang_code',$lang)->where('type','=','Pricing')->where('is_feature','=',2)->get();
        
        if($package->type == "Free" && $free_package <= 1  ){

            if($request->ajax()){
                $message = "You have Only Free Package left, You can not delete this!";
                return response()->json(['messages' => $message],200);
            }else{
                Session::flash('flash_message', __('You have Only Free Package left, You can not delete this!'));
                return redirect('admin/packages');
            }       

        }
        if($package->type == "Free" && $free_package > 1  )
        {
            Package::where('ref_id', $id)->delete();

            if($request->ajax()){
                $message = "Package deleted!";
                return response()->json(['messages' => $message],200);
            }else{
                Session::flash('flash_message', __('Package deleted!'));
                return redirect('admin/packages');
            }
            
        }


        if($package->type = "Pricing"){

            if(!$popular_package->isEmpty()) {
                
                if( !$bussiness_package->isEmpty() ){
            

                    if(count($popular_package) == 1 && $package->is_feature == 1){

                        if($request->ajax()){
                            $message = "You are deleting the Most Popular Package , Please select another to display on Home & try to delete again! ";
                            return response()->json(['messages' => $message],200);
                        }else{
                            Session::flash('flash_message', __('You are deleting the Most Popular Package , Please select another to display on Home & try to delete again! '));     
                            return redirect('admin/packages');
                        }
                         
    
                    }
                    else if(count($bussiness_package) == 1 && $package->is_feature == 2){
    
                        if($request->ajax()){
                            $message = "You are deleting the Bussiness Package , Please select another to display on Home & try to delete again!";
                            return response()->json(['messages' => $message],200);
                        }else{
                            Session::flash('flash_message', __('You are deleting the Bussiness Package , Please select another to display on Home & try to delete again! ')); 
                            return redirect('admin/packages'); 
                        }
                       
    
                    }
                    else{
                        Package::where('ref_id', $id)->delete();
                        if($request->ajax()){
                            $message = "Package deleted!";
                            return response()->json(['messages' => $message],200);
                        }else{
                            Session::flash('flash_message', __('Package deleted!'));
                            return redirect('admin/packages');
                        }
                        
    
                    }

                }else{
                    if($request->ajax()){
                        $message = "Please Select atleast one Package as Business Package to display on Home & try to delete again!";
                        return response()->json(['messages' => $message],200);
                    }else{
                        Session::flash('flash_message', __('Please Select atleast one Package as Business Package to display on Home & try to delete again! ')); 
                        return redirect('admin/packages');
                    }
                   
                }
                

            }else{

                if($request->ajax()){
                    $message = "Please Select atleast one Package as Most Popular Package to display on Home & try to delete again!";
                    return response()->json(['messages' => $message],200);
                }else{
                    Session::flash('flash_message', __('Please Select atleast one Package as Most Popular Package to display on Home & try to delete again! ')); 
                    return redirect('admin/packages');
                }

                
            }


        } 



    }
}
