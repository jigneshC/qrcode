<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Qrcode;
use Illuminate\Http\Request;
use Session;

use Milon\Barcode\DNS2D;
use Image;
class QrcodeController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.qrcode');
        $this->middleware('permission:access.qrcode.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.qrcode.create')->only(['create', 'store']);
        $this->middleware('permission:access.qrcode.delete')->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        //Qrcode::genQrCodeJpg("hash_token");

       // echo DNS2D::getBarcodeHTML("2218dfdc40aacf44e05e7edcdc429b37", "QRCODE");        exit;
    //    $user_id = $request->get('uid');
    //     $keyword = $request->get('search');
    //     $perPage = 25;

    //     if (!empty($keyword)) {
    //         if($user_id != '' || $user_id != 0){
    //             $qrcode = Qrcode::with('user')
    //             ->where('user_id','=',$user_id)
    //             ->where('name', 'LIKE', "%$keyword%")
    //             ->paginate($perPage);
                
    //         }else{
    //             $qrcode = Qrcode::with('user')
    //             ->where('user_id','!=',0)
    //             ->where('name', 'LIKE', "%$keyword%")
    //             ->paginate($perPage);
               
    //         }
            
    //     } else {
    //         if($user_id != '' || $user_id != 0){
    //             $qrcode = Qrcode::with('user')->where('user_id',$user_id)->paginate($perPage);
               
    //         }else{
    //             $qrcode = Qrcode::with('user')->where('user_id','!=',0)->paginate($perPage);
    //         }
            
    //     }
        
        $user_id = 0 ;
        $user_id = $request->uid;
        return view('admin.qrcode.index',compact('user_id'));
    }

    public function datatable(Request $request){

        $qrcode = Qrcode::with('user')->where('user_id','!=',0);

        if($request->user_id != 0){
            $qrcode->where('user_id',$request->user_id);
        }

         $qrcode->get();

        return Datatables::of($qrcode)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.qrcode.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required'
		]);

        $hash_token = Qrcode::genHashCode($request->redirectlink);
        Qrcode::genQrCodeJpg($hash_token);

        $requestData = $request->all();
        $requestData['hash'] = $hash_token;

        Qrcode::create($requestData);

        Session::flash('flash_message', 'Qrcode added!');

        return redirect('admin/qrcode');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $qrcode = Qrcode::find($id);

        if($qrcode == ''){
            return redirect()->back();
        }

        return view('admin.qrcode.show', compact('qrcode'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $qrcode = Qrcode::findOrFail($id);

        return view('admin.qrcode.edit', compact('qrcode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->except('hash');
        
        $qrcode = Qrcode::findOrFail($id);
        $qrcode->update($requestData);

        Session::flash('flash_message', 'Qrcode updated!');

        return redirect('admin/qrcode');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $qrcode = Qrcode::findOrFail($id);

        Qrcode::destroy($id);

        $path = public_path().'/uploads/qrcode/' . $qrcode->hash.".jpg";
        \File::delete($path);

        if($request->ajax()){
            $message = "Qr Code Deleted Successfully !!";
            return response()->json(['messages' => $message],200);
        }else{
            
            Session::flash('flash_message', 'Qrcode deleted Successfully !!');

            return redirect('admin/qrcode');
        } 

        
    }
}
