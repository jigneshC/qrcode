<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Setting;
use Illuminate\Http\Request;
use Session;

class SettingsController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:access.settings');
        $this->middleware('permission:access.setting.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.setting.create')->only(['create', 'store']);
        $this->middleware('permission:access.setting.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // $keyword = $request->get('search');
        // $perPage = 25;

        // if (!empty($keyword)) {
        //     $settings = Setting::where('duration', 'LIKE', "%$keyword%")
        //         ->orWhere('points', 'LIKE', "%$keyword%")
        //         ->paginate($perPage);
        // } else {
        //     $settings = Setting::paginate($perPage);
        // }

        return view('admin.settings.index');
    }

    public function datatable(Request $request){

        $settings = Setting::get();

        return Datatables::of($settings)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'points' => 'required'
        ]);
        $requestData = $request->all();

        if($request->years == 0 && $request->months == 0 && $request->days == 0){
            $requestData['duration'] = 'Lifetime'; 
        }else{
            if($request->years != 0 && $request->months != 0 && $request->days != 0){
                $requestData['duration'] =  $request->years.' years '.$request->months.' months '.$request->days.' days ';
            }elseif($request->years != 0 && $request->months == 0 && $request->days == 0){
                $requestData['duration'] =  $request->years.' years ';
            }elseif($request->years == 0 && $request->months != 0 && $request->days == 0){
                $requestData['duration'] =  $request->months.' months ';
            }elseif($request->years == 0 && $request->months == 0 && $request->days != 0){
                $requestData['duration'] =  $request->days.' days ';
            }elseif($request->years != 0 && $request->months != 0 && $request->days == 0){
                $requestData['duration'] =  $request->years.' years '.$request->months.' months ';
            }elseif($request->years != 0 && $request->months == 0 && $request->days != 0){
                $requestData['duration'] =  $request->years.' years '.$request->days.' days ';
            }elseif($request->years == 0 && $request->months != 0 && $request->days != 0){
                $requestData['duration'] =  $request->months.' months '.$request->days.' days ';
            }
        }
        

        Setting::create($requestData);

        Session::flash('flash_message', __('Setting added!'));

        return redirect('admin/settings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $setting = Setting::find($id);

        if($setting == ''){
            return redirect()->back();
        }

        return view('admin.settings.show', compact('setting'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $setting = Setting::findOrFail($id);

        return view('admin.settings.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'points' => 'required'
        ]);
        $requestData = $request->all();

        if($request->years == 0 && $request->months == 0 && $request->days == 0){
            $requestData['duration'] = 'Lifetime'; 
        }else{
            if($request->years != 0 && $request->months != 0 && $request->days != 0){
                $requestData['duration'] =  $request->years.' years '.$request->months.' months '.$request->days.' days ';
            }elseif($request->years != 0 && $request->months == 0 && $request->days == 0){
                $requestData['duration'] =  $request->years.' years ';
            }elseif($request->years == 0 && $request->months != 0 && $request->days == 0){
                $requestData['duration'] =  $request->months.' months ';
            }elseif($request->years == 0 && $request->months == 0 && $request->days != 0){
                $requestData['duration'] =  $request->days.' days ';
            }elseif($request->years != 0 && $request->months != 0 && $request->days == 0){
                $requestData['duration'] =  $request->years.' years '.$request->months.' months ';
            }elseif($request->years != 0 && $request->months == 0 && $request->days != 0){
                $requestData['duration'] =  $request->years.' years '.$request->days.' days ';
            }elseif($request->years == 0 && $request->months != 0 && $request->days != 0){
                $requestData['duration'] =  $request->months.' months '.$request->days.' days ';
            }
        }

        $setting = Setting::findOrFail($id);
        $setting->update($requestData);

        Session::flash('flash_message', __('Setting updated!'));

        return redirect('admin/settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, $id)
    {
        Setting::destroy($id);
        if($request->ajax()){
            $message = "Qr Code credits Deleted Successfully !!";
            return response()->json(['messages' => $message],200);
        }else{
            Session::flash('flash_message', __('Setting deleted!'));
            return redirect('admin/settings');
        }  
    }
}
