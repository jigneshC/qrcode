<?php

namespace App\Http\Controllers\Api;

use App\Equipment;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Qrcode;
use Auth;

class QrcodeController extends Controller
{


    protected $user;

    public function __construct()
    {

        $this->user = Auth::guard('api')->user();
    }


    protected function toJson($status = false, $data = [], $errors = [], $code = 200)
    {
        return response()->json([
            'messages' => $errors,
            'data' => $data,
            'status' => $status
        ], $code);
    }

    /**
     * //did getSubjects
     * @return \Illuminate\Http\JsonResponse
     */

    public function getQrcodes()
    {
        return $this->toJson(true, Qrcode::get()->toArray());
    }

    /**
     * did getTicket(ticketId)
     * @param $ticket_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getQrcode($hash_code)
    {
        $qrcode = Qrcode::where("hash",$hash_code)->first();
        if ($qrcode) {
            return $this->toJson(true, $qrcode->toArray());
        }
        //
        return $this->toJson(false, [], ['error' => 'No Qrcode Found!'], 404);
    }

    /**
     * Create Ticket
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postQrcode(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'status' => 'required|in:array(0,1,"0","1")',
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages();
            return $this->toJson(false, [], $errors);
        }

        $hash_token = Qrcode::genHashCode($request->redirectlink);
        Qrcode::genQrCodeJpg($hash_token);

        $requestData = $request->all();
        $requestData['hash'] = $hash_token;

        $qrcode = Qrcode::create($requestData);

        if ($qrcode) {
            return $this->toJson(true, $qrcode, ['message' => 'QrCode Created!']);
        }
        return $this->toJson(false, [], ['error' => 'Something wrong!']);

    }


    public function putQrcode($hash_code, Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'status' => 'in:array(0,1,"0","1")',
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages();
            return $this->toJson(false, [], $errors);
        }

        $requestData = $request->all();

        $qrcode = Qrcode::where("hash",$hash_code)->first();

        if($qrcode){
            $update = $qrcode->update($requestData);
            if ($update) {
                return $this->toJson(true, $qrcode, ['message' => 'Qrcode Updated!']);
            }
        }else{
            return $this->toJson(false, [], ['error' => 'No Qrcode Found!'], 404);
        }

        return $this->toJson(false, [], ['error' => 'Something wrong!']);
    }

    /**
     *
     * @param $ticket_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteQrcode($ticket_id)
    {

    }

}
