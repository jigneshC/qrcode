<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;
use App\Qrcode;
use Carbon\Carbon;

class UsersController extends Controller
{   

    protected function toJson($status = false, $data = [], $message = [], $validation = [], $code = 200)
    {
        return response()->json([
            'status' => $status,
            'data' => $data,
            'message' => $message,
            'validation' => $validation,
            'code' => $code            
        ]);
    }


    protected function register(Request $request)
    {
        $data = [];
        $message = "";
        $status = true;
        $validation = [];
        $code = 200;
        

        $rules = array(
            'email' => 'required|email',
            'password' => 'required',
        );


        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            $validation = $validator;
            $status = false;
            $code = \Config::get('constants.responce_code.validation_failed');
            $message = __(' Not Valid data');

        } else {

            $user = User::where('email',$request->email)->first();

            if($user){

                if($user->type == "api_user"){
                    $data = $user;
                    $message = __('User found with the given Email Id');
                }else{
                    $message = __('No User Found');
            
                }
            }else{
                $data['email'] = $request->email;
                $pwd = $request->password ;
                $data['password'] = bcrypt($pwd);
                $data['api_token'] = md5(uniqid());
                $data['language'] = 'en';
                $data['type'] = "api_user";
                $data['points'] = 10000000 ;
    
    
                $user = User::create($data);
                if ($user) {
                    $data = $user;
                    $message = __('Registration with the given Email is Done Successful');
                } else {
                    $status = false;
                    $code = \Config::get('constants.responce_code.bad_request');
                    $message = __('Something went wrong ! Please try again later');
                }
    
            }

            
        }
        return $this->toJson($status, $data, $message, $validation, $code);
    }

    public function generateQrcode(Request $request)
    {
        $data = [];
        $message = "";
        $status = true;
        $validation = [];
        $code = 200;
        

        $rules = array(
            'api_token' => 'required',
            'redirect_link' => 'required'
        );


        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            $validation = $validator;
            $status = false;
            $code = \Config::get('constants.responce_code.validation_failed');
            $message = __(' Not Valid data');

        } else {

            $user = User::where('api_token',$request->api_token)->where('type','api_user')->first();

            if($user){

                $qrcode = Qrcode::where('redirectlink',$request->redirect_link)->first();

                if($qrcode){

                    $data = $qrcode;
                    $message = __('Already found Qr Code with the given link');

                }else{
                     //Generate 1 qrCode  
                    $qrcode = new Qrcode;
                    $qrcode->name = "Api-Qrcode";
                    $qrcode->redirectlink = $request->redirect_link ;
                    $hash_token = Qrcode::genHashCode($request->redirect_link);
                    Qrcode::genQrCodeJpg($hash_token);
                    $qrcode->hash = $hash_token;
                    $qrcode->status = 1;
                    $qrcode->user_id = $user->id;
                    $qrcode->expiry_date = Carbon::now()->addYears(100);
                    $qrcode->save();

                    $data = $qrcode;
                    $message = __('Qr Code generated with the given link');
                }

               
            }

            
        }
        return $this->toJson($status, $data, $message, $validation, $code);
    }

    // public function getQrcode(Request $request)
    // {
    //     $data = [];
    //     $message = "";
    //     $status = true;
    //     $validation = [];
    //     $code = 200;
        

    //     $rules = array(
    //         'api_token' => 'required',
    //     );


    //     $validator = \Validator::make($request->all(), $rules);

    //     if ($validator->fails()) {

    //         $validation = $validator;
    //         $status = false;
    //         $code = \Config::get('constants.responce_code.validation_failed');
    //         $message = __(' Not Valid data');

    //     } else {

    //         $user = User::where('api_token',$request->api_token)->first();

    //         if($user){

    //             $qrcode = Qrcode::where('user_id',$user->id)->get();
    //         }

            
    //     }
    //     return $this->toJson($status, $data, $message, $validation, $code);
    // }



}
