<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Package;
use App\Order;
use DB;
use App\Language;
use App\Logic\ApiCall;

class HomeController extends Controller
{
    /**
     * HomeController constructor.
     */
    /*
    public function __construct()
    {
        $this->middleware('auth');
    }
    */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	public function info()
    {
		echo phpinfo();
	}	
    public function index()
    {   
        $lang = \App::getLocale();

        $toCode = Language::where('lang_code',$lang)->pluck('currency_code')->first();

        $free = Package::where('type','Free')->where('lang_code',$lang)->latest()->first();

        $popular = Package::where('is_feature',1)->where('lang_code',$lang)->latest()->first();

        if(!$popular){
            $popular = Package::where('lang_code',$lang)->where('type','Pricing')->get()->random(1)->first();    
        }

        $popular_rate=  ApiCall::convertRate($popular->amount , $popular->currency ,$toCode);

        $pricing = Package::where('is_feature',2)->where('lang_code',$lang)->latest()->first();

        if(!$pricing){
            $pricing = Package::where('lang_code',$lang)->where('type','Pricing')->get()->random(1)->first();    
        }

        $pricing_rate=  ApiCall::convertRate($pricing->amount , $pricing->currency ,$toCode);

        /*

        $pricing = Package::where('type','Pricing')->where('lang_code',$lang)->latest()->first();
        $pricing_rate=  ApiCall::convertRate($pricing->amount , $pricing->currency ,$toCode);

        $package_id = Order::select(DB::raw('count(package_id) as total, package_id'))->groupBy('package_id')
        ->orderBy('total','desc')->pluck('package_id');
        
        $popular = Package::whereIn('ref_id',$package_id)->where('lang_code',$lang)->where('type','Pricing')->first();
        $popular_rate=  ApiCall::convertRate($popular->amount , $popular->currency ,$toCode);
        */

        return view('home',compact('free','pricing','popular','pricing_rate','popular_rate'));
    }

    public function subDomain($account)
    {
        return $account;
    }

    public function redirect()
    {   
        if(Auth::user()->type == 'admin'){
            return redirect('/admin');
        }else{
            return redirect('/home');
        }

        
    }
}
