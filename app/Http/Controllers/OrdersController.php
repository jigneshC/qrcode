<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Logic\ApiCall;

use App\Notifications\NewUser;
use App\Notifications\FreeQr;

use App\Log;
use App\Package;
use Session;
use App\Order;
use Auth;
use App\User;
use App\Qrcode;
use Carbon\Carbon;
use App\Language;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
       // $this->middleware('permission:access.order');
       // $this->middleware('permission:access.orders.edit')->only(['edit', 'update']);
       // $this->middleware('permission:access.orders.create')->only(['create', 'store']);
       // $this->middleware('permission:access.orders.delete')->only('destroy');
    }

    public function index(Request $request)
    {   
        if(Auth::check()){
            $user_id = Auth::user()->id;
        }else{
            $user_id = \Session::get('user_id');
        }
        $lang = \App::getLocale();
        $toCode = Language::where('lang_code',$lang)->pluck('currency_code')->first();

        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $orders = Order::where('user_id',$user_id)->with('user','package')
                         ->where( function($query) use ($keyword)   { 
                                $query->where('package_name', 'LIKE', "%$keyword%")
                                ->orwhere('order_net_amount', 'LIKE', "%$keyword%")
                                ->orwhere('order_status', 'LIKE', "%$keyword%");
                            })
                ->where('order_status','!=','Pending')
                ->latest()
                ->paginate($perPage);
        } else {
            $orders = Order::where('user_id',$user_id)->with('user','package')->where('order_status','!=','Pending')->latest()->paginate($perPage);
        }

        
        return view('orders.index', compact('orders'));
    }

    public function create($id){
        
        $lang = \App::getLocale();
        $toCode = Language::where('lang_code',$lang)->pluck('currency_code')->first();

        $package = Package::findOrFail($id); 
        $rate =  ApiCall::convertRate($package->amount , $package->currency ,$toCode);

        $user = [];

        if(\Auth::check()){
            $user = Auth::user();
        }


        return view('orders.checkout',compact('user','package','rate'));
    }

    // public function store(Request $request)
    // {    
    //    // dd($request->all());
    //     $lang = \App::getLocale();
    //     $toCode = Language::where('lang_code',$lang)->pluck('currency_code')->first();
    //     $package = Package::findOrFail($request->package_id); 
    //     $rate =  ApiCall::convertRate($package->amount , $package->currency ,$toCode);
    //     $user = [];

    //         if(\Auth::check()){
    //             // User data 
    //             $user_id = \Auth::user()->id;
    //             $user = Auth::user();
    //             $user_obj = json_encode($user);
    //             $data['user_id'] = $user_id;
    //             $data['user_obj'] = $user_obj;
    //         }else{
    //             $data['user_id'] = 0; 
    //             $data['user_obj'] = 0;
    //         }
    //         // package data    
    //         $package_id = $request->package_id;
    //         $package = Package::findOrFail($package_id); 
    //         $package_obj = json_encode($package);

    //         $data['package_name']  = $package->title;
    //         $data['package_id']  = $package_id;
    //         $data['package_price']  = $package->amount;
    //         $data['order_qty']  = 1;
    //         $data['order_amount'] = ($data['package_price'] *  $data['order_qty']);
    //         $data['order_discount'] = 0;
    //         $data['order_currency']  = 'USD';
    //         $data['order_net_amount']  = ( $data['order_amount'] - $data['order_discount'] );
    //         $data['package_obj'] = $package_obj;
    //         $data['order_status'] = "Pending";
    //         $data['payment_status'] = "Pending"; 
    //         $data['payment_type'] = 'paypal';
    //         $data['payment_reference_id'] = 1;
    //         $data['user_order_amount'] = $rate['amount'];
    //         $data['user_order_currency'] = $rate['toCode'];

    //         if($package->type == "Free"){
    //             $data['order_status'] = "Success";
    //             $data['payment_status'] = "Success"; 
    //             $data['payment_type'] = 'free';
    //             $data['payment_reference_id'] = 0;
    //         }

    //         $order = Order::create($data);

    // //            return view('orders.checkout',compact('order','user','package','rate'));
    //         $request['order_id'] =  $order->id; 

    //         return redirect('/paypal/payment')->with('request',$request);
        

    // }

    public function checkout(Request $request)
    {   
        $lang = \App::getLocale();
        $toCode = Language::where('lang_code',$lang)->pluck('currency_code')->first();
        $package = Package::findOrFail($request->package_id); 
        $rate =  ApiCall::convertRate($package->amount , $package->currency ,$toCode);
        $user = [];

            if(\Auth::check()){
                // User data 
                $user_id = \Auth::user()->id;
                $user = Auth::user();
                $user_obj = json_encode($user);
                $data['user_id'] = $user_id;
                $data['user_obj'] = $user_obj;
            }else{
                $data['user_id'] = 0; 
                $data['user_obj'] = 0;
            }
            // package data    
            $package_id = $request->package_id;
            $package = Package::findOrFail($package_id); 
            $package_obj = json_encode($package);

            $data['package_name']  = $package->title;
            $data['package_id']  = $package_id;
            $data['package_price']  = $package->amount;
            $data['order_qty']  = 1;
            $data['order_amount'] = ($data['package_price'] *  $data['order_qty']);
            $data['order_discount'] = 0;
            $data['order_currency']  = 'USD';
            $data['order_net_amount']  = ( $data['order_amount'] - $data['order_discount'] );
            $data['package_obj'] = $package_obj;
            $data['order_status'] = "Pending";
            $data['payment_status'] = "Pending"; 
            $data['payment_type'] = 'paypal';
            $data['payment_reference_id'] = 1;
            $data['user_order_amount'] = $rate['amount'];
            $data['user_order_currency'] = $rate['toCode'];

            if($package->type == "Free"){
                $data['order_status'] = "Success";
                $data['payment_status'] = "Success"; 
                $data['payment_type'] = 'free';
                $data['payment_reference_id'] = 0;
            }

            $order = Order::create($data);

            $request['order_id'] =  $order->id; 

            \Session::put('request_data', $request->all()); 

            return redirect()->route('paypal.payment');
    }

    public function show($id)
    {
        if(Auth::check()){
            $user_id = Auth::user()->id;
        }else{
            $user_id = \Session::get('user_id');
        }
        $lang = \App::getLocale();
        $toCode = Language::where('lang_code',$lang)->pluck('currency_code')->first();

        $order = Order::where('id',$id)->where('user_id',$user_id)->with('user','package')->first();
        
        if($order == ''){

            return redirect()->back();

        }else{
        
            $package_price =  ApiCall::convertRate($order->package_price , $order->order_currency,$toCode);
            $order_amount =  ApiCall::convertRate($order->order_amount , $order->order_currency,$toCode);
            $order_net_amount =  ApiCall::convertRate($order->order_net_amount , $order->order_currency,$toCode);

            return view('orders.show', compact('order','package_price','order_amount','order_net_amount'));
        }
    }


    public function freepackage(Request $request){
        
        $new_user_msg = '' ;
        $msg = '' ; 

        if(Auth::guest()){
            $user_email =  $request->get('email');
            $user = User::where('email',$user_email)->first();
            if(count($user) == 1){ 
                $user_id = $user->id; 
            }else{
                $user = new User;
                $user->email = $request->get('email');
                $password = str_random(8);
                $user->password = bcrypt($password);
                $user->save();
                $user_id = $user->id; 

                $new_user_msg = "Your are Registered with the new account. Please Check your Email." ;
                
                //Notify Admin and User When New user is created
                $user->notify(new NewUser($password));
            }
        }else{
            
            $user = User::find(Auth::user()->id);
            $user_id = $user->id;
        }

        if($user->freeqr == 1){

            $msg =  "You Already got Your Free QrCode. Thankyou !!";

        }else{

            //get package detail
            $package = Package::where('id',$request->package_id)->first();

            //Generate 1 qrCode  
            $qrcode = new Qrcode;
            $qrcode->name = "Free QR Code";
            $hash_token = Qrcode::genHashCode();
            Qrcode::genQrCodeJpg($hash_token);
            $qrcode->hash = $hash_token;
            $qrcode->status = 1;
            $qrcode->user_id = $user_id;
            $qrcode->points = 0 ;
            $qrcode->expiry_date = Carbon::now()->addDays(15);
            $qrcode->save();

            $user->freeqr = 1 ;
            $user->save();

            //Notify User for free qrcode
            $user->notify(new FreeQr($qrcode));
            $msg =  "You get a free QrCode. Please Check your Email. " ; 

        }

        return response()->json(['msg'=> $msg , 'new_user_msg' => $new_user_msg ,'code'=>200]);


    }
}
