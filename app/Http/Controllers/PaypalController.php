<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Notifications\NewUser;
use App\Notifications\AdminPaymentSuccess;
use App\Notifications\AdminPaymentFailed;
use App\Notifications\PaymentSuccess;
use App\Notifications\PaymentError;
use App\Notifications\PaymentCancel;
use Illuminate\Support\Facades\Lang;

// use App\Transaction;
use App\User;
use App\Order;
use App\Payment_table;
use App\Package;
use App\Qrcode;
use App\Log;


use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;

use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

use PayPal\Api\Refund;
use PayPal\Api\RefundRequest;
use PayPal\Api\Sale;

class PaypalController extends Controller
{
 
    private $_api_context;
    private $mail_function;

    public function __construct()
    {
        $mode = config('paypal.mode');
        $settings = $config = config('paypal.settings');
        ($mode == "live") ? $paypal_conf = config('paypal.live') : $paypal_conf = config('paypal.sandbox');

        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($settings);

        
    }

    public function processPayment(Request $request)
    {   
        $request = \Session::get('request_data');
        $request = (object) $request;
        
        $can_procees = 1;
        $order_id = $request->order_id;
        
        if(Auth::guest()){
            $user_email =  $request->email;
            $user = User::where('email',$user_email)->first();
            if(count($user) == 1){ 
                \Session::put('user_id', $user->id);
            }else{
                $user = new User;
                $user->name = $request->first_name;
                $user->email = $request->email;
                $password = str_random(8);
                $user->password = bcrypt($password);
                $user->phone = $request->phone;
                $user->save();
                \Session::put('new_user', $user->id);
                \Session::put('user_id', $user->id);

                //Notify Admin and User When New user is created
                $user->notify(new NewUser($password));
            }
            $user_id = \Session::get('user_id');
            $find_order = Order::find($order_id);
            $find_order->user_id = $user_id;
            $find_order->user_obj = $user;
            $find_order->save();
            
            $order = Order::where("id", $order_id)->where("user_id", $user_id )->first();
        }else{
            $order = Order::where("id", $order_id)->where("user_id", Auth::user()->id)->first();
        }

        if($order && $can_procees){

            $des = "Qr-Code " . $order->package_name . " ";
            
            $payer = new Payer();
            $payer->setPaymentMethod('paypal');

            $item_1 = new Item();
            $item_1->setName($order->package_name)// item name
            ->setCurrency($order->order_currency)
                ->setQuantity(1)
                ->setPrice($order->order_net_amount); // unit price

            // add item to list
            $item_list = new ItemList();
            $item_list->setItems(array($item_1));

            $amount = new Amount();
            $amount->setCurrency($order->order_currency)
                ->setTotal($order->order_net_amount);

            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($item_list)
                ->setDescription($des);

            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(URL('order/paypal/callback/success'))// Specify return URL
            ->setCancelUrl(URL('order/paypal/callback/cancel'));

            $payment = new Payment();
            $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));

            try {
                $payment->create($this->_api_context);
                $payment_request_id = $payment->getId();
                $order->payment_request_id = $payment_request_id;
                session(['paypal_payment_id' => $payment_request_id]);
                $order->payment_request_object = $payment->toJSON();
                $order->first_name = $request->first_name;
                $order->last_name = $request->last_name;
                $order->address = $request->address;
                $order->phone = $request->phone;

                $order->save();
            } catch (\PayPal\Exception\PPConnectionException $ex) {

                $order->order_status = "Failed";
                $order->payment_status = "Failed" ;
                $order->save();
                Session::flash('error', 'Some error occured');
                return redirect('/message');

            }

            foreach ($payment->getLinks() as $link) {
                if ($link->getRel() == 'approval_url') {
                    $redirect_url = $link->getHref();
                    break;
                }
            }

            if (isset($redirect_url)) {
                return redirect($redirect_url);
            } else {
                
                $order->order_status = "Failed";
                $order->payment_status = "Failed" ;
                $order->save();

                Session::flash('error', 'Some error occured');
                return redirect('/message');
            }
        }
        else {
            Session::flash('error', 'Some error occured');
            return redirect('/message');
        }




    }

    public function paypalCallbackSuccess(Request $request)
    {
        
        $payment_id = Session::get('paypal_payment_id');
        
        $order = array();
        if (Session::has('paypal_payment_id')) {
            Session::forget('paypal_payment_id');
            $order = Order::where("payment_request_id", $payment_id)->first();
        } else {
            Session::flash('flash_warning', 'Payment failed !!');
            return redirect("/message");
        }

        if (empty($request->input('PayerID')) || empty($request->input('token'))) {
            Session::flash('error', 'Payment failed !!');
            $order->payment_status = "Failed";
            $order->order_status = "Failed";
            $order->save();

             //Notify Admin
             $admins = User::where('type','admin')->get();
             foreach($admins as $admin){
                 $admin->notify(new AdminPaymentFailed($order));
             }
 
             if(Auth::guest()){
                 $user_id = \Session::get('user_id');
                 $user = User::find($user_id);
                 
                 //Notify User when transaction is failed
                 $user->notify(new PaymentError($order));
             }else{
                 $user = User::find(Auth::user()->id);
                 //Notify User when transaction is failed
                 $user->notify(new PaymentError($order));
             } 

            return redirect("orders/" . $order->id);
        }

        try {
            $payment = Payment::get($payment_id, $this->_api_context);

            $execution = new PaymentExecution();
            $execution->setPayerId($request->input('PayerID'));

            //Execute the payment
            $payment->execute($execution, $this->_api_context);

            $transactions = $payment->getTransactions();
            $related_resources = $transactions[0]->getRelatedResources();
            $sale = $related_resources[0]->getSale();
            $sale_id = $sale->getId();
            
            if($sale_id!=""){ 
                $order->payment_status = "Success";
                $order->order_status = "Success"; 
                $order->save();
               

            }else{
                $status = $sale->getState();
            }
            // Transaction table entry
            //echo '<pre>'; print_r($transactions);exit;
            //print_r();  
            $trans_obj = serialize($transactions);  
            $payments['order_id'] = $order->id;
            $payments['payment_reference_id'] = $payment_id;
            $payments['transaction_id'] = $sale_id;
            $payments['amount'] = $order->order_net_amount;
            $payments['currency'] = $order->order_currency;
            $payments['transaction_object'] = $trans_obj;

            $payments = Payment_table::create($payments);
            

            Session::flash('flash_success', 'Order placed sucessfully !!');

            //Notify Admin
            $admins = User::where('type','admin')->get();
            foreach($admins as $admin){
                $admin->notify(new AdminPaymentSuccess($order));
            }

            if(Auth::guest()){
                $new_user =  \Session::get('new_user');
                $user_id = \Session::get('user_id');
                $user = User::find($user_id);
                if($new_user == $user_id){
                    Session::flash('flash_message', 'Your New Account is Created. Please check Your Email and LOG IN');
                }
                Session::flash('flash_message', 'Please LOG IN and Check your QrCodes'); 
                //Notify User on Successfull Transaction
                $user->notify(new PaymentSuccess($order));
            }else{
                $user = User::find(Auth::user()->id);
                //Notify User on Successfull Transaction
                $user->notify(new PaymentSuccess($order));
            }

            //get package detail
            $package = Package::where('id',$order->package_id)->first();
            /*
            for( $i = 1; $i <= $package->qr_code_no; $i++){
                $qrcode = new Qrcode;
                $qrcode->name = uniqid().'QR-'.$i;
                $qrcode->status = 1;
                $qrcode->user_id = $order->user_id;
                $qrcode->expiry_date = Carbon::now()->addYear($package->expiry_year)->addMonth($package->expiry_months);
                $qrcode->package_id = $package->id;
                $qrcode->save();
            }
            */


            $user_points = User::find($order->user_id);
            $points =  $user_points->points;
            $user_points->points = $points + $package->qr_code_no;
            $user_points->save();

            $log = new Log;
            $log->user_id = $user_points->id;
            $log->type = "Credit";
            $log->points = $package->qr_code_no;
            $log->order_id = $order->id;
            $log->save();


           

        } catch (\PayPal\Exception\PPConnectionException $ex) {
            Session::flash('error','Some error occured !!');

            $order->payment_status = "Failed";
            $order->order_status = "Failed";
            $order->save();

            //Notify Admin
            $admins = User::where('type','admin')->get();
            foreach($admins as $admin){
                $admin->notify(new AdminPaymentFailed($order));
            }

            if(Auth::guest()){
                $user_id = \Session::get('user_id');
                $user = User::find($user_id);
                
                //Notify User when transaction is failed
                $user->notify(new PaymentError($order));
            }else{
                $user = User::find(Auth::user()->id);
                //Notify User when transaction is failed
                $user->notify(new PaymentError($order));
            } 

        }
        return redirect("orders/" . $order->id);
    }

    public function paypalCallbackCancel(Request $request)
    {
        if (Session::has('paypal_payment_id')) {
            $order = Order::where("payment_request_id", Session::get('paypal_payment_id'))->first();
            Session::forget('paypal_payment_id');
            Session::flash('flash_success', 'Order Cancelled');


            $order->payment_status = "Cancelled";
            $order->order_status = "Cancelled";
            $order->save();

            if(Auth::guest()){
                $user_id = \Session::get('user_id');
                $user = User::find($user_id);
                
                //Notify User on Transaction Cancel
                $user->notify(new PaymentCancel($order));
            }else{
                $user = User::find(Auth::user()->id);
                //Notify User on Transaction Cancel
                $user->notify(new PaymentCancel($order));
            }
            
            return redirect("orders/" . $order->id);
        } else {

            Session::flash('flash_warning','Payment Failed');
            return redirect("/message");
        }

    }

    

}
