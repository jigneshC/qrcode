<?php

namespace App\Http\Controllers;

use App\Language;
use App\People;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use Session;
use Illuminate\Support\Facades\Lang;
use App\Log;
use App\Qrcode;

class ProfileController extends Controller
{

    public function index(Request $request)
    {
        $user = Auth::user();

        if($request->has('renew'))
        {
            
            $user->renew = $request->renew ;
            $user->save();
            return json_encode(['code' =>200 ]);
            exit;

        } 

        return view('profile.index', compact('user'));
    }

    public function edit()
    {
        $user = Auth::user();

        return view('profile.edit', compact('user'));
    }

    public function update(Request $request)
    {


        $this->validate($request,[
            'name' => 'required|alpha', 
            //'email' => 'required|unique:users', 
            'phone' => 'required|numeric',
            ]);

        $user = Auth::user();
        $data = $request->except('password');

        if ($request->hasFile('image')) {

                        $file = $request->file('image');

                        $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString() . uniqid());
            
                        $name = $timestamp . '-' . $file->getClientOriginalName();
            
                        $file->move(public_path() . '/images/', $name);
            
                        $data['image'] = $name;
                    }

        $user->update($data);

        Session::flash('success','Your profile updated successfully.');

        flash(__('Your profile updated successfully.'), 'success');

        return redirect()->back();

    }

    public function uploadPhoto(Request $request)
    {
        if ($request->hasFile('image')) {

            $file = $request->file('image');

            $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString() . uniqid());

            $name = $timestamp . '-' . $file->getClientOriginalName();

            $file->move(public_path() . '/images/', $name);

            return $name;
        } else {

            return null;
        }

    }

    public function changePassword()
    {
        return view('profile.changePassword');
    }

    public function updatePassword(Request $request)
    {

        $messages = [
            'current_password.required' => __('Please enter current password'),
            'password_confirmation.same' => __('The confirm password and new password must match.'),
        ];

        $this->validate($request,
            [
                'current_password' => 'required',
                'password' => 'required|min:6|max:255',
                'password_confirmation' => 'required|same:password',
            ], $messages);


        $cur_password = $request->input('current_password');


        $user = Auth::user();

        if (Hash::check($cur_password, $user->password)) {

            $user->password = Hash::make($request->input('password'));
            $user->save();

            Session::flash('success','Password changed successfully.');

            flash('Password changed successfully.', 'success');

            return redirect('/profile');

        } else {
            $error = array('current-password' => __('Please enter correct current password'));

            return redirect()->back()->withErrors($error);
        }

        flash('Something wrong. Please try again latter.', 'error');

        return redirect()->back();


    }

    public function dashboard(){

        $today = date('Y-m-d'); 

        $points_earned = Log::where('user_id',Auth::user()->id)->where('type','Credit')->sum('points');
        
        $points_used = Log::where('user_id',Auth::user()->id)->where('type','Debit')->sum('points');

        $total_qrcode = Qrcode::where('user_id',Auth::user()->id)->where('expiry_date','>=',$today)->count();

        $qrcode_expired = Qrcode::where('user_id',Auth::user()->id)->where('expiry_date','<',$today)->count();

        return view('dashboard',compact('points_earned','points_used','total_qrcode','qrcode_expired'));
    }

}
