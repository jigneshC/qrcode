<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

       //  return $next($request);
        
        if ($request->user()->type == 'admin') {
            return $next($request);
        }

        Session::flash("flash_mesasge","Access Denied");

        return redirect()->to('/');
        
    }
}
