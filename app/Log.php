<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'logs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','type','points','order_id'];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function package(){
        return $this->belongsTo('App\Package','package_id');
    }

    
}
