<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class FreeQr extends Notification
{
    use Queueable;
    public $qrcode;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($qrcode)
    {
        //
        $this->qrcode = $qrcode;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('QR Code')
            ->greeting('Hello, ' . $notifiable->name)
            ->line("You get one Free QrCode")
            ->line('Please Check for your Free Qrcode here')
            ->action('Free QR Code', url('/qrcode/'.$this->qrcode->id))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
