<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PaymentCancel extends Notification
{
    use Queueable;
    public $order_detail;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        //
        $this->order_detail = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('QR Code')
            ->greeting('Hello, ' . $notifiable->name)
            ->line('Your Payment via Paypal is Cancelled.')
            ->line('Order-id : ' . $this->order_detail->id)
            ->line('Package Name : ' . $this->order_detail->package_name)
            ->line('Package Price : ' . $this->order_detail->user_order_amount)
            ->line('Order Quantity : ' . $this->order_detail->order_qty)
            ->line('Order Amount : ' . $this->order_detail->user_order_amount.' '.$this->order_detail->user_order_currency )
            ->line('Net Amount : ' . $this->order_detail->user_order_amount)
            ->line('Payment Status : ' . $this->order_detail->payment_status)
            ->line('Check your order Details and Place your order Again  for Qrcode')
            ->action('Click here', url('/packages'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
