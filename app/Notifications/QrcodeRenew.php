<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class QrcodeRenew extends Notification
{
    use Queueable;
    public $qrcode;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($qrcode)
    {
        //
        $this->qrcode = $qrcode;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->subject('QR Code')
                ->greeting('Hello, ' . $notifiable->name)
                ->line('Your Expired Qr Code - '. $this->qrcode->name .' is Renewed.')
                ->line('Please Check & update your Qrcode')
                ->action('Qrcode', url('/qrcode/'.$this->qrcode->id))
                ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
