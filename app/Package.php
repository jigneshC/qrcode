<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'qr_code_no', 'expiry_year', 'amount','currency','expiry_months','type','ref_id','lang_code' , 'is_feature'
    ];

    public function languages()
    {
        return $this->belongsToMany('App\Language');
    }

    public function scopeLanguage($q, $lang = 'en')
    {

        if ($lang == 'en') {
            //session
            if (session()->has('_lang')) {
                $lang = session()->get('_lang');
            } else {
                $lang = \App::getLocale();
            }
        }

        return $q->where('activities.lang_code', $lang);
    }
}
