<?php

namespace App\Providers;

use App\WebSite;
use Illuminate\Support\ServiceProvider;
use Schema;
use App\Language;

class AppServiceProvider extends ServiceProvider
{

    protected static $_websites = null;

    protected static $_websites_pluck = null;

    protected static $_lang = null;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        //default
        $lang = "en";

        if (is_null(self::$_websites) && \Schema::hasTable('_websites')) {

            self::$_websites = WebSite::active()->get();
            self::$_websites_pluck = WebSite::select(
                \DB::raw("CONCAT(name,' | http://',domain) AS works"), 'domain', 'id')
                ->active()->pluck('domain', 'id')->prepend('Select..', '');
            
        }
        self::$_lang = Language::where('active' ,1)->get();
        // Using view composer to set following variables globally
        view()->composer('*', function ($view) {
            $view->with('_websites', self::$_websites);
            $view->with('_websites_pluck', self::$_websites_pluck);
            $view->with('_lang', self::$_lang);

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
