<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\HasRoles;
use App\Traits\BaseModelTrait;
use DB;

class User extends Authenticatable
{


    //only for user model
    public $is_user = true;

    use Notifiable, HasRoles;


    use SoftDeletes, BaseModelTrait;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', '_website_id', 'api_token','phone','image','points','type','renew'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'deleted_at', 'created_at', 'updated_at', 'created_by', 'updated_by'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */



    /**
     * One User is one person
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */



    /**
     * @param User $user
     * @return string
     */
    public static function genApiKey($unique_string = null)
    {
        if (is_null($unique_string)) {
            $unique_string = time();
        }

        $unique_string . str_random(5) . time();

        return md5(uniqid($unique_string, true));
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lang()
    {
        return $this->belongsTo('App\Language', 'language', 'lang_code');
    }

    public function qrcodes(){
        return $this->hasMany('App\Qrcode');
    }


}
