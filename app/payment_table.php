<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment_table extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payments';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id','payment_reference_id','transaction_id','amount','currency','transaction_object'
    ];
}
