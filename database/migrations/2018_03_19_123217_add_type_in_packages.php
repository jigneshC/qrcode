<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeInPackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packages', function (Blueprint $table) {
            //
            $table->string('type')->nullable();
            $table->string('expiry_months')->default(0);
            $table->integer('ref_id')->default(0);
            $table->string('lang_code')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packages', function (Blueprint $table) {
            //
            $table->dropColumn('type');
            $table->dropcolumn('expiry_months');
            $table->dropColumn('ref_id');
            $table->dropColumn('lang_code');
        });
    }
}
