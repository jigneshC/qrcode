<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\DropdownsType;
use Illuminate\Http\Request;
use Session;

class DropdownsTypesController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.dropdowns');
        $this->middleware('permission:access.dropdown.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.dropdown.create')->only(['create', 'store']);
        $this->middleware('permission:access.dropdown.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $dropdownstypes = DropdownsType::with('created_by')->where('name', 'LIKE', "%$keyword%")
                ->orWhere('active', 'LIKE', "%$keyword%")
                ->orWhere('created_by', 'LIKE', "%$keyword%")
                ->orWhere('updated_by', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $dropdownstypes = DropdownsType::with('created_by')->paginate($perPage);
        }
//return $dropdownstypes;
        return view('admin.dropdowns-types.index', compact('dropdownstypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.dropdowns-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        DropdownsType::create($requestData);

        Session::flash('flash_message', 'DropdownsType added!');

        return redirect('admin/dropdowns-types');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $dropdownstype = DropdownsType::findOrFail($id);

        return view('admin.dropdowns-types.show', compact('dropdownstype'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $dropdownstype = DropdownsType::findOrFail($id);

        return view('admin.dropdowns-types.edit', compact('dropdownstype'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $dropdownstype = DropdownsType::findOrFail($id);
        $dropdownstype->update($requestData);

        Session::flash('flash_message', 'DropdownsType updated!');

        return redirect('admin/dropdowns-types');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        DropdownsType::destroy($id);

        Session::flash('flash_message', 'DropdownsType deleted!');

        return redirect('admin/dropdowns-types');
    }
}
