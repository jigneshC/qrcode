<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Language;
use Illuminate\Http\Request;
use Session;

class LanguagesController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.languages');
        $this->middleware('permission:access.language.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.language.create')->only(['create', 'store']);
        $this->middleware('permission:access.language.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $languages = Language::where('name', 'LIKE', "%$keyword%")
                ->orWhere('lang_code', 'LIKE', "%$keyword%")
                ->activeOrder()->paginate($perPage);
        } else {
            $languages = Language::activeOrder()->paginate($perPage);
        }

        return view('admin.languages.index', compact('languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.languages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {


        $this->validate($request, [
            'name' => 'required',
            'lang_code' => 'required|unique:languages',
            'country' => 'required',
            'country_code' => 'required',
            'currency_code' => 'required',
            'currency_symbol' => 'required',
        ]);
        $requestData = $request->all();

        Language::create($requestData);

        Session::flash('flash_message', 'Language added!');

        return redirect('admin/languages');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $language = Language::find($id);

        if($language == ''){
            return redirect()->back();
        }

        return view('admin.languages.show', compact('language'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $language = Language::findOrFail($id);

        return view('admin.languages.edit', compact('language'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'country' => 'required',
            'country_code' => 'required',
            'currency_code' => 'required',
            'currency_symbol' => 'required',
            'lang_code' => 'required|unique:languages,lang_code,' . $id

        ]);
        $requestData = $request->all();

        $language = Language::findOrFail($id);
        $language->update($requestData);

        Session::flash('flash_message', 'Language updated!');

        return redirect('admin/languages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Language::destroy($id);

        Session::flash('flash_message', 'Language deleted!');

        return redirect('admin/languages');
    }
}
