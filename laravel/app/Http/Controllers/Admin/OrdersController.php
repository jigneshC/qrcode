<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Package;
use Session;
use App\Order;
use Auth;
use App\User;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:access.order');
        $this->middleware('permission:access.orders.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.orders.create')->only(['create', 'store']);
        $this->middleware('permission:access.orders.delete')->only('destroy');
    }

    public function index(Request $request)
    {
        // $keyword = $request->get('search');
        // $perPage = 25;

        // if (!empty($keyword)) {
        //     $orders = Order::where('package_name', 'LIKE', "%$keyword%")
        //     ->orwhere('order_status', 'LIKE', "%$keyword%")
        //     ->where('order_status','!=','Pending')
        //     ->where('user_id','!=',0)
        //         ->paginate($perPage);
        // } else {
        //     $orders = Order::with('user')->where('order_status','!=','Pending')
        //     ->where('user_id','!=',0)->paginate($perPage);
        // }

        return view('admin.orders.index');
    }

    public function datatable(Request $request){

        $orders = Order::with('user')->where('order_status','!=','Pending')
        ->where('user_id','!=',0)->get();
        
        return Datatables::of($orders)->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $package = Package::findOrFail($id);   
        //dd($package);

        return view('admin.orders.create',compact('package'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     /* package data */   
      $package_id = $request->package_id;
      $package = Package::findOrFail($package_id); 
      $package_obj = json_encode($package);

     /* User data */ 
      $user_id = \Auth::user()->id;
      $user = Auth::user();
      $user_obj = json_encode($user);

      $data['package_name']  = $package->title;
      $data['package_id']  = $package_id;
      $data['package_price']  = $package->amount;
      $data['order_qty']  = 1;
      $data['order_amount'] = ($data['package_price'] *  $data['order_qty']);
      $data['order_discount'] = 0;
      $data['order_currency']  = 'USD';
      $data['order_net_amount']  = ( $data['order_amount'] - $data['order_discount'] );
      $data['order_status'] = 'pending';
      $data['payment_status'] = 'pending';
      $data['user_id'] = $user_id;
      $data['user_obj'] = $user_obj;
      $data['package_obj'] = $package_obj;
      $data['payment_type'] = 'paypal';
      $data['payment_reference_id'] = 1;
    
      $order = Order::create($data);

      return view('admin.orders.checkout',compact('order'));
    }

    public function checkout(Request $request)
    {
       
       
        return redirect('paypal');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $order = Order::where('id',$id)->with('user')->first();

        if($order == ''){
            return redirect()->back();
        }
        
        return view('admin.orders.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
