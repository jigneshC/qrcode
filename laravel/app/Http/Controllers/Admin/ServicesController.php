<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Service;
use App\Site;
use Illuminate\Http\Request;
use Session;
use App\User;

class ServicesController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.services');
        $this->middleware('permission:access.service.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.service.create')->only(['create', 'store']);
        $this->middleware('permission:access.service.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $services = Service::with('site')->where('site_id', 'LIKE', "%$keyword%")
                ->orWhere('name', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('manager_id', 'LIKE', "%$keyword%")
                ->orWhere('active', 'LIKE', "%$keyword%")
                ->orWhere('created_by', 'LIKE', "%$keyword%")
                ->orWhere('updated_by', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $services = Service::with('site')->paginate($perPage);
        }

        return view('admin.services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        $sites = Site::pluck('name', 'id')->prepend('Select', '');

        $users = User::pluck('name', 'id')->prepend('Select', '');

        return view('admin.services.create', compact('sites', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $requestData = $request->all();

        Service::create($requestData);

        Session::flash('flash_message', __('Service added!'));

        return redirect('admin/services');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $service = Service::findOrFail($id);

        return view('admin.services.show', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $service = Service::findOrFail($id);

        $sites = Site::pluck('name', 'id')->prepend('Select', '');
        $users = User::pluck('name', 'id')->prepend('Select', '');


        return view('admin.services.edit', compact('service', 'sites', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        $requestData = $request->all();

        $service = Service::findOrFail($id);
        $service->update($requestData);

        Session::flash('flash_message', __('Service updated!'));

        return redirect('admin/services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Service::destroy($id);

        Session::flash('flash_message', __('Service deleted!'));

        return redirect('admin/services');
    }
}
