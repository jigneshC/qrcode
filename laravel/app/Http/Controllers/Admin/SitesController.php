<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Site;
use Illuminate\Http\Request;
use Session;
use App\Country;
use App\Company;
use App\DropdownsType;

class SitesController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:access.sites');
        $this->middleware('permission:access.site.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.site.create')->only(['create', 'store']);
        $this->middleware('permission:access.site.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $sites = Site::where('name', 'LIKE', "%$keyword%")
                ->orWhere('address', 'LIKE', "%$keyword%")
                ->orWhere('city', 'LIKE', "%$keyword%")
                ->orWhere('postcode', 'LIKE', "%$keyword%")
                ->orWhere('country', 'LIKE', "%$keyword%")
                ->orWhere('phone_number_1', 'LIKE', "%$keyword%")
                ->orWhere('phone_number_2', 'LIKE', "%$keyword%")
                ->orWhere('phone_type_1', 'LIKE', "%$keyword%")
                ->orWhere('phone_type_2', 'LIKE', "%$keyword%")
                ->orWhere('site_type', 'LIKE', "%$keyword%")
                ->orWhere('logo', 'LIKE', "%$keyword%")
                ->orWhere('access_conditions', 'LIKE', "%$keyword%")
                ->orWhere('default_email', 'LIKE', "%$keyword%")
                ->orWhere('guard_presence', 'LIKE', "%$keyword%")
                ->orWhere('digicode', 'LIKE', "%$keyword%")
                ->orWhere('keybox_presence', 'LIKE', "%$keyword%")
                ->orWhere('keybox_code', 'LIKE', "%$keyword%")
                ->orWhere('keybox_place', 'LIKE', "%$keyword%")
                ->orWhere('keybox_issue', 'LIKE', "%$keyword%")
                ->orWhere('known_issue', 'LIKE', "%$keyword%")
                ->orWhere('company_id', 'LIKE', "%$keyword%")
                ->orWhere('instructions_tasks_file', 'LIKE', "%$keyword%")
                ->orWhere('order_file', 'LIKE', "%$keyword%")
                ->orWhere('procedures_file', 'LIKE', "%$keyword%")
                ->orWhere('sitemaps_file', 'LIKE', "%$keyword%")
                ->orWhere('active', 'LIKE', "%$keyword%")
                ->orWhere('created_by', 'LIKE', "%$keyword%")
                ->orWhere('updated_by', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $sites = Site::paginate($perPage);
        }

        return view('admin.sites.index', compact('sites'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {


        $countries = Country::pluck('name', 'code')->prepend('Select', '');

//        $business = DropdownsType::whereName('business_type')->with('values')->first();
//
//        $businessTypes = $business->values()->pluck('name', 'id')->prepend('Select', '');


        $phone = DropdownsType::whereName('phone_type')->with('values')->first();


        $phoneTypes = $phone->values()->pluck('name', 'parent_id')->prepend('Select', '');


        $site_ = DropdownsType::whereName('site_type')->with('values')->first();

        $siteTypes = $site_->values()->pluck('name', 'parent_id')->prepend('Select', '');


        $companies = Company::pluck('name', 'id')->prepend('Select', '');

        return view('admin.sites.create', compact('countries', 'phoneTypes', 'companies', 'siteTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

//        return $request->file();

        $this->validate($request, [

            'company_id' => 'required',
            'name' => 'required',
            "instructions_tasks_file" => 'mimes:pdf',
            "order_file" => 'mimes:pdf',
            "procedures_file" => 'mimes:pdf',
            "sitemaps_file" => 'mimes:pdf',
        ]);

//        mimes:jpeg,bmp,png


        $requestData = $request->all();

        $name = $this->uploadLogo($request);

        if (!is_null($name)) {
            $requestData['logo'] = $name;
        }


        $instruction_tasks_file = $this->_uploadFile('instructions_tasks_file', $request);
        if (!is_null($instruction_tasks_file)) {
            $requestData['instructions_tasks_file'] = $instruction_tasks_file;
        }

        $order_file = $this->_uploadFile('order_file', $request);
        if (!is_null($order_file)) {
            $requestData['order_file'] = $order_file;
        }

        $procedures_file = $this->_uploadFile('procedures_file', $request);
        if (!is_null($procedures_file)) {
            $requestData['procedures_file'] = $procedures_file;
        }

        $sitemaps_file = $this->_uploadFile('sitemaps_file', $request);
        if (!is_null($sitemaps_file)) {
            $requestData['sitemaps_file'] = $sitemaps_file;
        }

        Site::create($requestData);

        Session::flash('flash_message', 'Site added!');

        return redirect('admin/sites');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $site = Site::findOrFail($id);

        return view('admin.sites.show', compact('site'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $site = Site::findOrFail($id);

        $countries = Country::pluck('name', 'code')->prepend('Select', '');


        $phone = DropdownsType::whereName('phone_type')->with('values')->first();

        $phoneTypes = $phone->values()->pluck('name', 'parent_id')->prepend('Select', '');

        $companies = Company::pluck('name', 'id')->prepend('Select', '');

        $site_ = DropdownsType::whereName('site_type')->with('values')->first();

        $siteTypes = $site_->values()->pluck('name', 'parent_id')->prepend('Select', '');


        return view('admin.sites.edit', compact('site', 'countries', 'phoneTypes', 'companies', 'siteTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [

            'company_id' => 'required',
            'name' => 'required',
            "instructions_tasks_file" => 'mimes:pdf',
            "order_file" => 'mimes:pdf',
            "procedures_file" => 'mimes:pdf',
            "sitemaps_file" => 'mimes:pdf',
        ]);
        $requestData = $request->all();

        $name = $this->uploadLogo($request);

        if (!is_null($name)) {
            $requestData['logo'] = $name;
        }

        $instruction_tasks_file = $this->_uploadFile('instructions_tasks_file', $request);
        if (!is_null($instruction_tasks_file)) {
            $requestData['instructions_tasks_file'] = $instruction_tasks_file;
        }

        $order_file = $this->_uploadFile('order_file', $request);
        if (!is_null($order_file)) {
            $requestData['order_file'] = $order_file;
        }

        $procedures_file = $this->_uploadFile('procedures_file', $request);
        if (!is_null($procedures_file)) {
            $requestData['procedures_file'] = $procedures_file;
        }

        $sitemaps_file = $this->_uploadFile('sitemaps_file', $request);
        if (!is_null($sitemaps_file)) {
            $requestData['sitemaps_file'] = $sitemaps_file;
        }


        $site = Site::findOrFail($id);
        $site->update($requestData);

        Session::flash('flash_message', 'Site updated!');

        return redirect('admin/sites');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Site::destroy($id);

        Session::flash('flash_message', __('Site deleted!'));

        return redirect('admin/sites');
    }


    /**
     * @param Request $request
     * @return null|string
     */
    public function uploadLogo(Request $request)
    {
        if ($request->hasFile('logo')) {

//            dd($request->file('image'));
            $file = $request->file('logo');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString() . uniqid());

            $name = $timestamp . '-' . $file->getClientOriginalName();

//            dd($name);
//            $image->filePath = $name;

            $file->move(public_path() . '/uploads/', $name);

            return $name;
        } else {

            return null;
        }

    }


    public function _uploadFile($filename, Request $request)
    {
        if ($request->hasFile($filename)) {

//            dd($request->file('image'));
            $file = $request->file($filename);
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', \Carbon\Carbon::now()->toDateTimeString() . uniqid());


//            $_ext = $file->getClientOriginalExtension();
//
//            if (strtolower($_ext) != 'pdf') {
//                return null;
//            }


            $name = $timestamp . '-' . $file->getClientOriginalName();

//            dd($name);
//            $image->filePath = $name;

            $file->move(public_path() . '/uploads/files/', $name);

            return $name;
        } else {

            return null;
        }

    }

}
