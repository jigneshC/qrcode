<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Session;
use App\Log;

class UsersController extends Controller

{

    public function __construct()
    {
        $this->middleware('permission:access.users');
        $this->middleware('permission:access.user.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.user.create')->only(['create', 'store']);
        $this->middleware('permission:access.user.delete')->only('destroy');
    }


    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        // $keyword = $request->get('search');
        // $perPage = 15;

        // if (!empty($keyword)) {
        //     $users = User::with('roles','qrcodes')->where('name', 'LIKE', "%$keyword%")->orWhere('email', 'LIKE', "%$keyword%")
        //         ->paginate($perPage);
        // } else {
        //     $users = User::with('roles','qrcodes')->paginate($perPage);
        // }
        
        return view('admin.users.index');
    }

    public function datatable(Request $request){

        $users = User::with('roles','qrcodes')->get();

        return Datatables::of($users)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $roles = Role::select('id', 'name', 'label')->lower()->get();
        $roles = $roles->pluck('label', 'name');
        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'email' => 'required|unique:users', 'password' => 'required', 'roles' => 'required']);

        $data = $request->except('password');
        $data['password'] = bcrypt($request->password);


        $data['_website_id'] = 0;

        $user = User::create($data);

        if ($user) {
            $user->api_token = User::genApiKey($user->id);
            $user->save();
        }

        foreach ($request->roles as $role) {
            $user->assignRole($role);
        }

        Session::flash('flash_message', __('User added!'));

        return redirect('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        $user = User::Find($id);
        
        if($user == ''){
            return redirect()->back();
        }

        $points_earned = Log::where('user_id',$user->id)->where('type','Credit')->sum('points');
        
        $points_used = Log::where('user_id',$user->id)->where('type','Debit')->sum('points');


        

        return view('admin.users.show', compact('user','points_earned','points_used'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
        $roles = Role::select('id', 'name', 'label')->lower()->get();
        $roles = $roles->pluck('label', 'name');

        $user = User::with('roles')->findOrFail($id);
        $user_roles = [];
        foreach ($user->roles as $role) {
            $user_roles[] = $role->name;
        }
        return view('admin.users.edit', compact('user', 'roles', 'user_roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', 'email' => 'required|unique:users,email,' . $id, 'roles' => 'required']);

        $data = $request->except('password');
        if ($request->has('password')) {
            $data['password'] = bcrypt($request->password);
        }

        $user = User::findOrFail($id);
        $user->update($data);


        if ($user) {
            $user->api_token = User::genApiKey($user->id);
            $user->save();
        }

        $user->roles()->detach();
        foreach ($request->roles as $role) {
            $user->assignRole($role);
        }

        Session::flash('flash_message', __('User updated!'));

        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request ,$id)
    {
        User::destroy($id);

        if($request->ajax()){

            $message = "User deleted!";

            return response()->json(['messages' => $message],200);

        }else{

            Session::flash('flash_message', __('User deleted!'));

            return redirect('admin/users');
        } 

        
    }
}
