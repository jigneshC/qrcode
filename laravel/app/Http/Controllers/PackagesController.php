<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Package;
use Session;
use App\Language;
use Auth;
use App\Logic\ApiCall;

class PackagesController extends Controller
{

    public function index(Request $request)
    {
        $lang = \App::getLocale();
        $toCode = Language::where('lang_code',$lang)->pluck('currency_code')->first();

        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $pack = Package::where('title', 'LIKE', "%$keyword%")
                ->orWhere('qr_code_no', 'LIKE', "%$keyword%")    
                ->orWhere('expiry_year', 'LIKE', "%$keyword%")  
                ->orWhere('amount', 'LIKE', "%$keyword%")  
                ->orWhere('currency', 'LIKE', "%$keyword%")
                ->get();
                
        } else {
            $pack = Package::where('lang_code',$lang)->get();
        }

        $packages = [];
        foreach ($pack as $i => $package){
            $amount = $package->amount ;
            $formCode = $package->currency ;
            $rate=  ApiCall::convertRate($amount , $formCode,$toCode);
            $packages[$i]['id'] =  $package->id ;
            $packages[$i]['amount'] =  $rate['amount'] ;
            $packages[$i]['currency'] =  $rate['toCode'] ;
            $packages[$i]['title'] = $package->title ;
            $packages[$i]['description'] = $package->description;
            $packages[$i]['qr_code_no'] = $package->qr_code_no;
            $packages[$i]['expiry_year'] = $package->expiry_year;
            $packages[$i]['expiry_months'] = $package->expiry_months;
            $packages[$i]['type'] = $package->type;

        }

        $free = Package::where('type','Free')->where('lang_code',$lang)->latest()->first();

        return view('packages.index', compact('packages','free'));
    }

    public function show($id)
    {
        $package = Package::findOrFail($id);

        return view('packages.show', compact('package'));
    }

}
