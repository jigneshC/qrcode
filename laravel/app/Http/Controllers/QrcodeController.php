<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Qrcode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\Setting;
use App\User;
use App\Log;
use Carbon\Carbon;

use Auth;
use Milon\Barcode\DNS2D;
use Image;

class QrcodeController extends Controller
{

    public function getQrcode($code = null)
    {
        $qrcode = array();
        if(!$code){
            return view('qrcode', compact('qrcode'));
        }
        $qrcode = Qrcode::where("hash",$code)->where('status',1)->first();

        if($qrcode && $qrcode->redirectlink !=""){
          //  dd('fsfdf');
           // return Redirect::away($qrcode->redirectlink);
            // if(strpos($qrcode->redirectlink, 'http://') == 0 || strpos($qrcode->redirectlink, 'https://') == 0) {
            //    dd($qrcode->redirectlink);
            //     return Redirect::to($qrcode->redirectlink); 
            // }else{
            //     return Redirect::to('//'.$qrcode->redirectlink);
            // }


            $check_pos_http =  strpos($qrcode->redirectlink, 'http://');
            $check_pos_https =  strpos($qrcode->redirectlink, 'https://');
            $check_pos_http = (string)$check_pos_http;
            $check_pos_https = (string)$check_pos_https;
  
                  if( $check_pos_http == "0" ||  $check_pos_https == "0" ) {
                      
                      return Redirect::to($qrcode->redirectlink); 
                      
                  }else{
                     
                      return Redirect::to('//'.$qrcode->redirectlink);
                     
                  }
        }
           
        
        else{
            return view('qrcode', compact('qrcode'));
        }


    }

    public function index(Request $request)
    {   
        $user_id = \Auth::user()->id;
        $keyword = $request->get('search');
        $perPage = 10;
        $today_date = date('Y-m-d');

        if (!empty($keyword)) {
            $qrcode = Qrcode::where('name', 'LIKE', "%$keyword%")
                ->where('user_id',$user_id)
                ->latest()
				->paginate($perPage);
        } else {
            $qrcode = Qrcode::where('user_id',$user_id)
                    ->latest()
                    ->paginate($perPage);
        }

        return view('qrcode.index', compact('qrcode','today_date'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {   
        $today_date = date('Y-m-d');

        $user_id = \Auth::user()->id;
        
        $qrcode = Qrcode::where('id',$id)->where('user_id',$user_id)->first();

        if($qrcode == ''){
            return redirect()->back();
        }

        return view('qrcode.show', compact('qrcode','today_date'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $qrcode = Qrcode::findOrFail($id);

        return view('qrcode.edit', compact('qrcode'));
    }

    public function update($id, Request $request)
    {
        
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();

        if($request->has('redirectlink')){
         //   dd(strpos($request->redirectlink, 'http://'));
        $url =  $this->remove_http($request->redirectlink);
       // dd($url);
            // if(strpos($request->redirectlink, 'http://') !== 0 ) {
            //     $redirect_link =  'http://' . $request->redirectlink;
            //     $requestData['redirectlink'] = $redirect_link ;
            // }else{
            //     $requestData['redirectlink'] = $request->redirectlink;
            // }

            $hash_token = Qrcode::genHashCode($url);
            $requestData['redirectlink'] = $url;

        }else{
            $hash_token = Qrcode::genHashCode();
        }
        
        // $hash_token = Qrcode::genHashCode($request->redirectlink);
        // Qrcode::genQrCodeJpg($hash_token);
         $requestData['hash'] = $hash_token;
         
        
        $qrcode = Qrcode::findOrFail($id);
        $qrcode->update($requestData);
        $qrcode->genQrCodeJpg($hash_token);

        Session::flash('flash_message', 'Qrcode updated!');

        return redirect('/qrcode');
    }

    function remove_http($url) {
        $disallowed = array('http://', 'https://');
        foreach($disallowed as $d) {
           if(strpos($url, $d) === 0) {
              return str_replace($d, '', $url);
           }
        }
        return $url;
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $qrcode = Qrcode::findOrFail($id);

        Qrcode::destroy($id);

        $path = public_path().'/uploads/qrcode/' . $qrcode->hash.".jpg";
        \File::delete($path);

        Session::flash('flash_message', 'Qrcode deleted!');

        return redirect('/qrcode');
    }

    public function generateqr(Request $request){

        $settings  = Setting ::all();

        $user = User::find(Auth::user()->id);

        $points_earned = Log::where('user_id',$user->id)->where('type','Credit')->sum('points');
        
        $points_used = Log::where('user_id',$user->id)->where('type','Debit')->sum('points');

        if(!empty($request->setting)){
            
            $duration = $request->setting;
            $setting = Setting::where('duration',$duration)->first();

            if($user->points < $setting->points ){
                Session::flash('flash_message', "You Don't have Enough Points");
                return redirect()->back();
            }else{
            //Generate 1 qrCode  
                $qrcode = new Qrcode;
                $qrcode->name = $request->name;
                
                if($request->has('redirectlink')){
                    
                    if(strpos($request->redirectlink, 'http://') !== 0 || strpos($request->redirectlink, 'https://') !== 0) {
                        $redirect_link =  'http://' . $request->redirectlink;
                       
                    }else{
                        $redirect_link = $request->redirectlink; 
                    }

                    $hash_token = Qrcode::genHashCode($redirect_link);

                    $qrcode->redirectlink = $redirect_link ;
                }else{
                    $hash_token = Qrcode::genHashCode();
                }
                
                Qrcode::genQrCodeJpg($hash_token);
                $qrcode->hash = $hash_token;
                $qrcode->status = $request->status;
                $qrcode->user_id = $user->id;
                if($setting->duration == "Lifetime"){

                    $qrcode->expiry_date = Carbon::now()->addYear(150);  
                    
                }else{
                    $qrcode->expiry_date = Carbon::now()->addYear($setting->years)->addMonth($setting->months)->addDays($setting->days);
                }
               
                $qrcode->package_id = 0;
                $qrcode->points = $setting->points;
                $qrcode->years = $setting->years;
                $qrcode->months = $setting->months;
                $qrcode->days = $setting->days;
                $qrcode->save();

                $log = new Log;
                $log->user_id = $user->id;
                $log->type = "Debit";
                $log->points = $setting->points;
                $log->save();

                $points = $user->points;
                $user->points = $points - $setting->points;
                $user->save();

                Session::flash('flash_message', "Your QR-CODE Generated Successfully.");
                return redirect('/qrcode/'. $qrcode->id);


            }
        }
        return view('qrcode.generate',compact('settings','points_earned','points_used'));


    }

}
