<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Language;


class LanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   

        $browser_lang = "en";
        
        if($_SERVER && isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])){
            $browser_lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        }

        // $browser_lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

        $default_lang = Language::where('active',1)->where('lang_code','LIKE',"%$browser_lang%")->first();
   
        if ($request->get('lang')) {
            
            $lang = $request->get('lang');
            $request->session()->replace(['_lang' => $lang]);
        
        } else if ($request->session()->has('_lang')) {

            $lang = $request->session()->get('_lang');
           
        } else if($default_lang){
            
            $lang = $default_lang->lang_code ;
                  
        }
         else {

            if (Auth::check()) {
                $lang = Auth::user()->language;
               
            } else {
                $lang = 'en';
               
            }

        }
       
        \App::setLocale($lang);

//        if ($request->is('language/*') or $request->is('language')) {
//            return $next($request);
//        }
//
//        if ($request->session()->has('_lang')) {
//            $lang = $request->session()->get('_lang');
//            \App::setLocale($lang);
//        }
        return $next($request);
    }
}
