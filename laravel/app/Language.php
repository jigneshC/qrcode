<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = "languages";

    protected $fillable = [
        'lang_code', 'name', 'description', 'country', 'country_code','currency_code','currency_symbol','active'
    ];


    public function user()
    {
        return $this->hasMany('App\User', 'lang_code', 'language');
    }


//    public function categories()
//    {
//        return $this->hasMany('App\Category');
//    }

    public function scopeActive($q)
    {
        return $q->where('active', true);
    }

    public function scopeActiveOrder($q)
    {
        return $q->orderBy('active', 'desc');
    }

}
