<?php
namespace App\Logic;
use Auth;

class ApiCall
{

    public static function convertRate($amount, $fromCode, $toCode) {
       
       
        $amount = $amount ;
        $fromCode = $fromCode;
        $toCode = $toCode;
        $unit = 1;

        $data = "";
        $url = config('site_setting.api_url').'?access_key='.config('site_setting.api_access_key').'&format=1&source='.$fromCode.'&currencies='.$toCode;
        $ch = curl_init ($url); // your URL to send array data
        curl_setopt ($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // Your array field
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec ($ch);

        $result = json_decode($result);

        if($result->success == null){

            if($fromCode == 'USD' && $toCode == 'INR'){
                $unit = 65.4;
            }
            $convert = $amount * $unit;
            $convert = number_format((float)$convert, 2, '.', '');
    
            return [ 'amount' => $convert , 'fromCode' => $fromCode , 'toCode' => $toCode  ]; 
            exit;

        }
        
        
        $unit = $result->quotes;

        $convertor = 0;
        foreach($unit as $convertor_unit){
            $convertor = $convertor_unit;
        }

        $convert = $amount * $convertor;
        $convert = number_format((float)$convert, 2, '.', '');

        return [ 'amount' => $convert , 'fromCode' => $fromCode , 'toCode' => $toCode  ];    
    }
    

}