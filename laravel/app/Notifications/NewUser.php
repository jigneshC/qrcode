<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewUser extends Notification
{
    use Queueable;

    public $user_pass; 

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($pass)
    {
        //
        $this->user_pass = $pass;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->subject('QR Code')
                ->greeting('Hello, ' . $notifiable->name)
                ->line('Your profile has been created in Qr Code.')
                ->line('With Email-id : ' . $notifiable->email)
                ->line('And Password : ' .$this->user_pass)
                ->line('Login with the given Password and Complete Your Profile')
                ->action('Login', url('/'))
                ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
