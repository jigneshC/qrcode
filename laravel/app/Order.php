<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'package_name', 'package_id', 'package_price', 'order_qty', 'order_amount','order_discount',
        'order_currency','order_net_amount','order_status','payment_status','user_id','user_obj',
        'package_obj','payment_type','payment_reference_id','first_name','last_name','address','phone',
        'user_order_amount','user_order_currency'
    ];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function package(){
        return $this->belongsTo('App\Package','package_id');
    }
}
