<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Milon\Barcode\DNS2D;
use Image;
use phpDocumentor\Reflection\Types\Null_;

class Qrcode extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'qrcodes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */


    protected $fillable = ['name', 'hash', 'redirectlink', 'status','user_id','expiry_date','pacakage_id','points','years','months','days'];

    protected $appends = array('qrcode_url');

    public function getQrcodeUrlAttribute()
    {
        $path = public_path().'/uploads/qrcode/' . $this->hash.".jpg";

        if($this->hash != "" && file_exists($path)){
            return asset("uploads/qrcode")."/".$this->hash.".jpg";
        }else{
            return null;
        }

    }

    public static function genHashCode($unique_string = null)
    {

        if (is_null($unique_string)) {
            $unique_string = time();
        }

        $unique_string . str_random(5) . time();

        return md5(uniqid($unique_string, true));
    }
    public static function genQrCodeJpg($hash_token){

        $qrBase64 = DNS2D::getBarcodePNG(URL('/')."/".$hash_token, "QRCODE",10,10);//DNS2D::getBarcodePNG($hash_token, "QRCODE");

        $png_url = $hash_token.".jpg";
        $path = public_path().'/uploads/qrcode/' . $png_url;

        Image::make($qrBase64)->save($path);

    }
    
    public function user(){
        return $this->belongsTo('App\User');
    }
}
