<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebSite extends Model
{

    use SoftDeletes;
    protected $table = "_websites";

    /**
     * @var array
     */
    protected $fillable = [
        'domain', 'name', 'status', 'description'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'master' => 'boolean',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'user_website', 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function peoples()
    {
        return $this->hasMany('App\People');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    /**
     * @param $query
     * @param $bool
     * @return mixed
     */
    public function scopeMaster($query, $bool)
    {
        return $query->where('master', $bool);
    }
}
