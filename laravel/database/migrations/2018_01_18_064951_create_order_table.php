<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function(Blueprint $table) {
            $table->increments('id');
            $table->string('package_name');
            $table->integer('package_id');
            $table->integer('package_price');
            $table->integer('order_qty')->default('1');
            $table->integer('order_amount');  
            $table->integer('order_discount')->nullable(); 
            $table->string('order_currency'); 
            $table->integer('order_net_amount'); 
            $table->enum('order_status', ['pending', 'success','fail','cancelled']);	
            $table->enum('payment_status', ['pending', 'success','fail','cancelled']);	
            $table->integer('user_id'); 
            $table->longText('user_obj'); 
            $table->longText('package_obj'); 
            $table->enum('payment_type', ['paypal']);	
            $table->text('payment_reference_id'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
