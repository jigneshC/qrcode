<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInQrcode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('qrcodes', function (Blueprint $table) {
            //
            $table->integer('user_id')->default(0);
            $table->date('expiry_date')->nullable();
            $table->integer('package_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qrcodes', function (Blueprint $table) {
            //
            $table->dropColumn('user_id');
            $table->dropColumn('expiry_date');
            $table->dropColumn('package_id'); 
        });
    }
}
