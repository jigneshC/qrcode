<?php
return[
    // language dropdown
    'language' => 'Language',
    
    //buttons
    'create_a_qr_code_for_free' => 'Create a QR Code for Free',
    'get_started' => 'Get Started',
    'login' => 'Log In',
    'signup' => 'Sign Up',
    'welcome' => 'Welcome',
    'logout' => 'Logout',
    'buy_now' => 'Buy Now',
    'view_all_packages' => 'View all Packages',
    'messages' => 'Messages',


    //Content
    'content' =>  [

        'top' => [
            'h1' => "Launch your",
            'h2' => "QR Code Generator",
            'p1' => "The instant way to target",
            'p2' => "customers on  the go",
        ],
        
        
        'section_marketing' => [
                'heading' => "Your marketing just got easier with updatable QR codes",
                'p' => [
                    "From posters, T-shirts, mugs, flyers, brochures and catalogues to business cards, QR codes are everywhere. That's because the results speak for themselves: millions of scans and customer conversions worldwide. QR codes are fast, fun and effective.",
                    "Customers only need a smartphone to get instant access to all your digital content. For savvy businesses, QR codes have becomethe go-to marketing tool to help them spread the word and make an impact on a younger demographic.",
                    "But what if you want to constantly update your digital content? With most QR code services, you'll need to create and print a whole new QR code.",
                    "That's why we created thedynamic QR Code Library: It'sa renewable QR code generator that lets you change the destination link of your QR code as many times as you need. Whether you're switching up your URL, video, image or contact details, simply log into an online portal(with API for QR code) and update your QR code details. No fuss, fast results.",                    
                ]

        ],

        'section_professional' => [
            'heading' => "A professional QR Code Generator",
            'tab_1' => [
                'li' => "Create Unique QR Codes",
                'p' => "You Can Create New Qr Codes That Link To Your Website, Contact Form, Video Or Other Digital Media In Just Minutes. Using Just A Smartphone, Anyone Anywhere Can Then Scan Your Printed Qr Code, Interact With Your Business And Move Further Along The Sales Funnel."
            ],
            'tab_2' => [
                'li' => "Update QR Codes When You Need",
                'p' => "Online Campaigns Can Change In An Instant. With A Renewable Qr Code, You Can Easily Update And Manage Your Destination Link Without Re-Printing Your Qr Code. Simply Log Onto An Online Portal (We Offer An Api For Qr Codes) And Change The Linked Url Or File As Many Times As You Need. It Gives You Greater Flexibility And Speed."
            ], 
            'tab_3' => [
                'li' => "Achieve High-Quality Results",
                'p' => "Design A Sleek And Eye-Catching Campaign With Our High-Resolution 32-Bit And 64-Bit Qr Codes. We Offer Downloadable Print Formatsin Jpeg, Png, Eps And Svg For Stunning Results.You Can Create The Perfect First Impression With Professional Qualityqr Code Graphics. "
            ],
            'tab_4' => [
                'li' => "Share Your Account With Others",
                'p' => "Working In A Team? Add Other Users To Theqr Code Libraryportal With Our Account-Sharing Features. Choose The Level Of Access For Each User, Such As Viewing Rights Or Full Administrative Status. Our Flexible Qr Code Generator Helps You Organise And Streamline Your Campaign."
            ],
            'tab_5' => [
                'li' => "Get User Help And Support",
                'p' => "Get help managing your QR Code Library. For any questions, guidance or troubleshooting, contact our Customer Support by email and we'll get back to you as soon as possible. We work togive you the best QR Code Generator experience."
            ]

        
            ],

            'section_plan' => [
                'heading_2' => "Plans and Pricing",
                'heading_3' => "Simple, transparent pricing. Always know what you'll pay.",
                'free_trial' => "Free Trial",
                'most_popular' => "Most Popular",
                'business' => "Business",
                'per_point' => "Per Point",
                'points_earn' => "Points You can Earn",
                'per_account_user' => "Qr Code per Account Users",
                'scans' => "Number of Scans",
                'formats' => "JPEG, PNG, EPS and SVG formats",
                'customer_support' => "Customer Support",
                'api' => "API for QR Code",
                'tracking' => "QR Code Tracking",

            ],

            'faq' => [
                'heading' => "Frequently Asked Questions",
                'q1' => "What is a QR code?",
                'ans1' => "A QR code (Quick Response Code) is a code made up of black-and-white pixel patterns. With the ability to encode up to several hundred characters, QR codes are much more powerful than traditional supermarket barcodes.",
                "ans1_a" => "In the past decade, QR codes have exploded in popularity due to the rise of smartphones—users can simply point their phone cameras to a QR code and, without typing in a URL, be taken straight to a company's website, contact form, video or other digital media. Marketers in particular have seen the potential of QR codes in promoting events, products and services.",

                'q2' => "Where can I put my QR code?",
                'ans2' => "After creating your QR code in a QR code generator, you can print out your code on physical objects such as posters, brochures, and catalogues as well as mugs, T-shirts, badges, pencil cases and other merchandise. You can also email QR codes directly to customers",

                'q3' => "How do I create a renewable QR code?",
                'ans3' => "Simply log into the QR Code Library, choose the digital media that links to your QR code then download and print out your updatable QR code. It takes just a minute. Then follow the same steps if you need to update the link associated with your renewable QR code.",

                'q4' => "What makes the QR Code Library different to other QR Code Generators?",
                'ans4' => "The QR Code Library offers renewable QR codes. Unlike traditional QR code generators, you don't have to keep re-printing your code when your destination link changes. Instead, you can update your QR code digital media link by logging into the online portal. Our professional service, offering API for QR code, manages your codes in a flexible and convenient way.",

                'q5' => "How do I best use QR codes on my marketing materials?",
                'ans5' => "Engage your customers by letting them know the benefits of scanning the code. For example, you can put a short text next to the code: ‘Scan to get your 15% discount'. Also make sure to size your QR code at least two centimetres wide and display your graphic in the best quality resolution possible. That way your customers can successfully scan at their first attempt.",

                'q6' => "Can I give other people accesstomy QR Code Library account?",
                'ans6' => "Yes. We offer multiple user access to your QR code generator. You can give them different levels of access such as limited viewing rights or full administrative power.Best of all, our professional service manages your entire QR code collection using API for QR Code", 

            ]


    ]

    
];