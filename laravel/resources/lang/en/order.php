<?php

return [

    // Show
    'back' => 'Back',
    'order' => 'Order',
    'order_id' => 'Order ID',
    'user_name' => 'User Name',
    'user_email' => 'User Email',
    'user_phone' => 'User Phone',
    'item_details' => 'Item details',
    'package_name' => 'Package Name',
    'package_price' => 'Package Price',
    'number_of_qrcodes' => 'Number Of Qrcodes',
    'order_details' => 'Order Details',
    'order_quantity' => 'Order Quantity',
    'order_amount' => 'Order Amount',
    'order_discount' => 'Order Discount',
    'order_net_amount' => 'Order Net Amount',
    'order_status' => 'Order Status',
    'payment_details' => 'Payment Details',
    'payment_type' => 'Payment Type',
    'payment_date' => 'Payment Date',
    'payment_reference_id' => 'Payment Reference ID',
    'payment_request_id' => 'Payment Request Id',
    'payment_status' => 'Payment Status',
    'user_details' => 'User Details',
    
    //Index
    'id' => 'ID',
    'order_placed_by' => 'Order Placed By',
    'no_of_qrcodes' => 'Number of QrCodes',
    'package_title' => 'Package Title',
    'currency' => 'Currency',
    'actions' => 'Actions',
    'no_orders_found' => 'No Orders Found',
    'view' => 'View',
    'orders' => 'Orders',
    'points' => 'Points',
    'total_no_of_points' => 'Total No. of Points',
    'points_you_can_earn' => 'Points You Can Earn',
    'search' => 'Search',

    //Checkout
    'checkout_qrcode' => 'Checkout - QR COde',
    'checkout' => 'Checkout',
    'billing_details' => 'Billing Details',
    'first_name' => 'First Name *',
    'last_name' => 'Last Name',
    'email' => 'Email *',
    'address' => 'Address',
    'phone' => 'Phone *',
    'your_order' => 'Your Order',
    'product' => 'Product',
    'product_name' => 'Product Name',
    'validity' => 'Validity',
    'grand_total' => 'Grand Total',
    'place_order' => 'Place Order',
    'total' => 'Total',

    //form
    'buy_now' => 'Buy Now',

    'send_me_my_free_qrcode' => 'Send me My Free QrCode',


];