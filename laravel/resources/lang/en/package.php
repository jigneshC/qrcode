<?php

return [

    //show-list
    'package' => 'Package',
    'back' => 'Back',
    'id' => 'Id',
    'title' => 'Title',
    'description' => 'Description',
    'qrcode_no' => 'Number of Qr Codes',
    'expiry_year' => 'Expiry Year',
    'currency' => 'Currency',
    'created_on' => 'Created On',
    'actions' => 'Actions',
    

    //Index
    'plan_and_pricing_qrcode' => 'Plan And Pricing - QR Code',
    'buy_now' => 'Buy Now',
    'number_of_renewable_qrcodes' => 'Number of Renewable QR Codes',
    'number_of_account_users' => 'Number of Account Users',
    'qr_code_validity' => 'Qr Code Validity',
    'total_points' => 'Total Points',
    'create_a_qr_code_for_free' => "Create a QR code for FREE",
    
    //content
    'content' =>  [
        'text_line_1' => "Simple, transparent pricing.",
        'text_line_2' => "Always know what you'll pay.",
        
        'section_competition' => [
                'heading' => "Stay Ahead of the Competition",
                'li' => [
                    "Reach more customersin public spaces",
                    "Connect with a younger, tech-savvy demographic",
                    "Offer instant access to your digital media ",
                    "Generate more click-throughs to your website",
                    "Move people further along the sales funnel",
                    "Create your QR codes in just minutes ",
                    "Easy, interactive and cost-effective marketing strategy ",
                    "Updatable, lifetime codes never expire",
                    "Download, print or directly email QR codes",
                ]

                ],

        'section_question' => [
            'heading' => "What's API for QR Code?",
            'p' => "Whether you need to manage inventory or simply track a parcel, we offer access to an API for QR codes with bulk orders of over 50 codes. Our professional tool helps your marketing campaign stay organised. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porttitor tempor lorem sit amet sollicitudin. Fusce scelerisque, arcu vel maximus consequat. Fusce scelerisque, arcu vel maximus consequat.",
            'li' => [
                "Order more than 50 renewable QR codes.",
                "Manage your QR codes via our API.",
                "View full product information linked to your purchased QR codes.",
                ]
            ]
        
        ]
    




];