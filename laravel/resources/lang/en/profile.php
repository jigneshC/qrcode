<?php

return [

    //index
    'my_profile' => 'My Profile',
    'edit_profile' => 'Edit Profile',
    'change_password' => 'Change Password',
    'name' => 'Name',
    'email' => 'Email',
    'phone' => 'Phone',
    'photo' => 'Photo',
    'joined' => 'Joined',

    //edit
    'back' => 'Back',
    'name_*' => 'Name *',
    'email_*' => 'Email *',
    'phone_*' => 'Phone *',
    'update_profile' => 'Update Profile',

    //changePassword
    'confirm_password' => 'Confirm Password',
    'new_password' => 'New Password',
    'current_password' => 'Current Password'
    





];