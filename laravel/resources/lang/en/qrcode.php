<?php

return [
    
    
    'edit_qr_code' => "Edit QR Code",
    'id' => "ID",
    'back' => "Back",
    'name' => "Name",
    'redirect_link' => "Redirect Link",
    'hash_code' => "Hash Code",
    'status' => "Status",
    'create' => 'Create',
    'generate_qrcode' => "Generate - QRCode",
    'duration' => "Duration",
    'generate' => "Generate",
    'qrcode' => "Qr-Code",
    'qrcode_list' => "Qrcode List",
    'search' => "Search",
    'edit' => "Edit",
    'delete' => "Delete",
    'view' => "View",
    'expired' => "Expired",
    'active' => "Active",
    'inactive' => "Inactive",
    'no_qr_codes_found' => "No Qr Codes Found",
    'actions' => "Actions",
    'view_qr_code' => "View Qr Code",
    'expiry_date' => "Expiry Date",
    'qrcode_expired' => "Your Qr Code is Expired",
    'created_on' => "Created On",




];
?>