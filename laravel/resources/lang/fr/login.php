<?php
return[

    'login' => "S'identifier",
    'signup' => "S'inscrire",
    'email_address' => 'Adresse email',
    'password' => 'Mot de passe',
    'remember_me' => 'Souviens-toi de moi',
    'sign_in' => 'Se connecter',
    'forgot_your_password' => 'Mot de passe oublié ?',
    'new' => 'Nouveau ?',

    //Register
    'register' => 'Créez un compte',
    'name' => 'prénom',
    'email' => 'Email',
    'phone' => 'Téléphone',
    'confirm_password' => 'Confirmez le mot de passe',
    'already_register' => 'Déjà inscrit?',

    //access denied
    'access_denied' => 'Accès refusé',
    'dont_have_permission' => "Vous n'êtes pas autorisé à accéder à cette page.",

    //emails
    'reset_password' => 'Réinitialiser le mot de passe',
    'send_password_reset_link' => 'Envoyer le mot de passe',

    //reset




    
];