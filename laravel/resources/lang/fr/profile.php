<?php

return [

    //index
    'my_profile' => 'Mon Profil',
    'edit_profile' => 'Editer mon profil',
    'change_password' => 'Changer mon mot de passe',
    'name' => 'Nom',
    'email' => 'Email',
    'phone' => 'Téléphone',
    'photo' => 'Photo',
    'joined' => 'Membre depuis',

    //edit
    'back' => 'Retour',
    'name_*' => 'Nom *',
    'email_*' => 'Email *',
    'phone_*' => 'Téléphone *',
    'update_profile' => 'Mettre à jour mon profil',

    //changePassword
    'confirm_password' => 'Confirmez le mot de passe',
    'new_password' => 'Nouveau mot de passe',
    'current_password' => 'Mot de passe actuel'
    
];