<?php

return [
    
    
    'edit_qr_code' => "Editer le QR Code",
    'id' => "ID",
    'back' => "Retour",
    'name' => "Nom",
    'redirect_link' => "Lien de redirection",
    'hash_code' => "Hash Code",
    'status' => "Statut",
    'create' => 'Créer',
    'generate_qrcode' => "Générer - QRCode",
    'duration' => "Durée",
    'generate' => "Générer",
    'qrcode' => "QRCode",
    'qrcode_list' => "Liste de QRCode",
    'search' => "Chercher",
    'edit' => "Editer",
    'delete' => "Supprimer",
    'view' => "Voir",
    'expired' => "Expiré",
    'active' => "Actifs",
    'inactive' => "Inactifs",
    'no_qr_codes_found' => "Trouvé aucun QRCode",
    'actions' => "Actions",
    'view_qr_code' => "Voir le QRCode",
    'expiry_date' => "Date d'expiration",
    'qrcode_expired' => "Votre QRCode est expiré",
    'created_on' => "Créé le",




];
?>