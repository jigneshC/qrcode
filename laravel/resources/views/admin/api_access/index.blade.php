@extends('layouts.backend')

@section('title','Api Access')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        Api Access
                    </div>

                </div>
                <div class="box-content ">

                    <a href="{{ url('/admin') }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a>

                    <br><br>
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                            <tr>
                                <th>AccessKey</th>
                                <td>
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="key_text" disabled>
                                        </div>

                                        <button type="button" class="btn btn-default" id="key_btn">Show Access Key
                                        </button>

                                        <button type="button" class="btn btn-success" id="regen_btn"
                                                style="display: none" data-user_id="{!! Auth::user()->id !!}">Regenerate
                                            Key
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>Document</th>
                                <td>
                                    @markdown
                                    #APIs

                                    ##qrcode
                                    > **{site}/api/v1.0/qrcode/get-qrcode/{hash_code}?api_token=Your api token**

                                    - method : GET
                                    - request : null
                                    - response
                                    - messages
                                    - data
                                    - status(true|false)

                                    > **{site}/api/v1.0/qrcode/create-qrcode?api_token=Your api token**

                                    - method : POST
                                    - request :
                                    - name(string|required),
                                    - redirectlink(string),
                                    - status(['0','1']|required) // 0 for inactive , 1 for active
                                    - response
                                    - messages
                                    - data
                                    - status(true|false)

                                    > **{site}/api/v1.0/qrcode/edit-qrcode/{hash_code}?api_token=Your api token**

                                    - method : PUT
                                    - request :
                                    - name(string|required),
                                    - redirectlink(string),
                                    - status(['0','1']) // 0 for inactive , 1 for active
                                    - response
                                    - messages
                                    - data
                                    - status(true|false)


                                    @endmarkdown
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="passModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Enter Your Password</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger _error" style="display: none"></div>
                    <form class="form-horizontal" id="AccessForm" data-toggle="validator">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Email:</label>
                            <div class="col-sm-10">
                                <div class="form-control" disabled="">{{Auth::user()->email}}</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">Password:</label>
                            <div class="col-sm-10">
                                <input type="password" name="password" class="form-control" id="pwd"
                                       placeholder="Enter password" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Show Access Key</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


@endsection

@push('js')
<script>
    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).val()).select();
        document.execCommand("copy");
        $temp.remove();
    }


    $(document).ready(function () {

        var access = false;
        var btn = $('#key_btn');
        var input = $('#key_text');
        var passModal = $('#passModal');

        var AccessForm = $('#AccessForm');
        var _error = $('._error');

        var regenBtn = $('#regen_btn');

        regenBtn.hide();//for double sure

        _error.hide();

        //show modal
        btn.click(function (e) {
            e.preventDefault();

            if (access == true) {
                copyToClipboard(input);
                alert('Access key Copied');

                btn.text('Copied');
//                btn.prop('disabled', true);
                return;
            }

            passModal.modal('show');
        });


        regenBtn.click(function (e) {
            e.preventDefault();
            if (access == true) {
//                alert('regen btn clicked');
                var user_id = $(this).data('user_id');
                regenToken(user_id);
            }

        });


        /**
         *
         * @param user_id
         */
        var regenToken = function (user_id) {

            var _url = "{!! url('admin/api-access/regen') !!}";
            $.post(_url, {},
                function (data, status) {

                    if (data.status == true) {
                        input.prop('disabled', false);
                        input.val(data.api_token);
                        btn.text('Copy');
                        alert('Access key regenerated.')
                    } else {
                        alert('Something wrong!, try again.');
                        input.val('');
                        input.prop('disabled', true);
                    }
                    console.log(status);
                });

        }


        //access form submit
        AccessForm.submit(function (e) {
            e.preventDefault();

            var data = $(this).serializeArray();

            if (data['password'] == '') {
                _error.text('Please, Enter your password.').show();
            } else {
                _error.hide();
            }

            var url = "{!! url('admin/api-access') !!}";
            $.post(url, data,
                function (data, status) {

                    if (data.status == true) {
                        //enable
                        input.prop('disabled', false);
                        input.val(data.api_token);
                        btn.text('Copy');
                        access = true;
                        passModal.modal('hide');
                        _error.hide();

                        regenBtn.show();

                    } else {
                        //desable
                        _error.text('Your password is wrong.').show();
                        input.val('');
                        input.prop('disabled', true);
                        btn.text('Show Access Key');
                        access = false;
                    }
//                    console.log(data);
                    console.log(status);
//                alert("Data: " + data + "\nStatus: " + status);
                });

        });

    });

</script>
@endpush