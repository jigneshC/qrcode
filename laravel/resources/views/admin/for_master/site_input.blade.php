@if(_MASTER)

    <?php
    $_id = isset($website_id) ? $website_id : null;

    $_sel = null;
    if (is_null($_id)) {
        $_sel = _WEBSITE_ID;
    }

    ?>


    <div class="form-group {{ $errors->has('_website_id') ? 'has-error' : ''}}">
        {!! Form::label('_website_id', 'Website', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('_website_id',$_websites_pluck, $_sel, ['class' => 'form-control','required'=>'required']) !!}
            {!! $errors->first('_website_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

@endif