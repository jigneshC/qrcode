@extends('layouts.backend')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        Orders
                    </div>

                </div>
                <div class="box-content ">
{{--                   
                    {!! Form::open(['method' => 'GET', 'url' => '/admin/orders', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                    <div class="input-group" style="max-width: 300px">
                        <input type="text" class="form-control" name="search" placeholder="Search..." value="{!! request()->get('search') !!}">
                        <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                    </div>
                    {!! Form::close() !!} --}}

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-borderless" id="orders-table" >
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>User Name </th>
                                <th>Package Name</th>
                                <th> Points </th>
                                <th>Order Amount</th>
                                <th>Order Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            {{-- <tbody>

                            @foreach($orders as $item)
                            /* $package_detail = json_decode($item->package_obj) ;
                            $user_detail = json_decode($item->user_obj) ;  */ 
                            
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $user_detail->name }}</td>
                                    <td>{{ $package_detail->title }}</td>
                                    <td>{{ $package_detail->qr_code_no }}</td>
                                    <td>{{ $item->user_order_amount }} {{ $item->order_currency }}</td>
                                    <td>{{ $item->order_status }}</td>
                                    <td>
                                        
                                        <a href="{{ url('/admin/orders/' . $item->id) }}" title="View Orders">
                                            <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                   aria-hidden="true"></i> View
                                            </button>
                                        </a>
                                        @if(Auth::user()->can('access.orders.edit'))
                                        <a href="{{ url('/admin/orders/' . $item->id . '/edit') }}" title="Edit Orders">
                                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                      aria-hidden="true"></i> Edit
                                            </button>
                                        </a>
                                        @endif
                                        @if(Auth::user()->can('access.orders.delete'))
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/orders', $item->id],
                                            'style' => 'display:inline'
                                        ]) !!}


                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs',
                                                'title' => 'Delete Order',
                                                'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        @endif
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody> --}}
                        </table>
                        {{-- <div class="pagination-wrapper"> {!! $orders->appends(['search' => Request::get('search')])->render() !!} </div> --}}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection


@push('js')

    <script>

        var url ="{{ url('admin/orders-data') }}";
        var edit_url = "{{ url('admin/orders') }}";

        //Permissions
        var edit_order ="{{ Auth::user()->can('access.order.edit') }}";
        var delete_order ="{{ Auth::user()->can('access.order.delete') }}";
    
        
        datatable = $('#orders-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]], 
            ajax: {
                url:url,
                type:"get",
                data: function (d)  {
                   
                }
            },
                columns: [
                    { data: 'id', name: 'id',  "orderable":true,searchable:true },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var user_json = decodeHtml(o.user_obj);
                            var user_detail = JSON.parse(user_json);
                            return user_detail['name'];//user_name ;
                               
                        }
                    
                    
                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {

                            var package_json = decodeHtml(o.package_obj);
                            var package_detail = JSON.parse(package_json);
                            return package_detail['title'];//package_title ;
                        }
                    
                    
                    },
                   
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            
                            var package_json = decodeHtml(o.package_obj);
                            var package_detail = JSON.parse(package_json);
                            return package_detail['qr_code_no'];//points ;
                        }
                    
                    
                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            
                            return o.user_order_amount+" "+o.order_currency ;
                        }
                    
                    
                    },
                   
                    { data: 'order_status', name: 'order_status',  "orderable":true,searchable:true },
                   
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var v="" ;
                            
                            v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i>View</button></a>&nbsp;";
                            
                            return v;
                        }
                    
                    
                    }
                ]
        });

function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}
    </Script>
    
@endpush
