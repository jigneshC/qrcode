@extends('layouts.backend')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        Packages
                    </div>

                </div>
                <div class="box-content ">
                    
                    @if(Auth::user()->can('access.packages.create'))
                    <a href="{{ url('/admin/packages/create') }}" class="btn btn-success btn-sm" title="Add New Packages">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
                    @endif

                    {{-- {!! Form::open(['method' => 'GET', 'url' => '/admin/packages', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                    <div class="input-group" style="max-width: 300px">
                        <input type="text" class="form-control" name="search" placeholder="Search..." value="{!! request()->get('search') !!}">
                        <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                    </div>
                    {!! Form::close() !!} --}}

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-borderless" id="packages-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Package Name</th>
                                <th>Points</th>
                                <th>Type</th>
                                <th>Price</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            {{-- <tbody>
                            @foreach($packages as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->qr_code_no }}</td>
                                    <td>{{ $item->type }}</td>
                                    <td>{{ $item->amount }} {{ $item->currency }}</td>
                                    <td>
                                        <!--
                                        @if(Auth::user()->can('access.orders.edit'))
                                        <a href="{{ url('/admin/orders/create/'.$item->id) }}" title="Make order">
                                            <button class="btn btn-primary btn-xs"><i class="fa fa-cart"
                                                                                        aria-hidden="true"></i> Make Order
                                            </button>
                                        </a>
                                        @endif
                                    -->   

                                        <a href="{{ url('/admin/packages/' . $item->id) }}" title="View Packages">
                                            <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                   aria-hidden="true"></i> View
                                            </button>
                                        </a>
                                        @if(Auth::user()->can('access.packages.edit'))
                                        <a href="{{ url('/admin/packages/' . $item->id . '/edit') }}" title="Edit Packages">
                                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                      aria-hidden="true"></i> Edit
                                            </button>
                                        </a>
                                        @endif
                                        @if(Auth::user()->can('access.packages.delete'))
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/packages', $item->id],
                                            'style' => 'display:inline'
                                        ]) !!}


                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs',
                                                'title' => 'Delete Package',
                                                'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        @endif
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody> --}}
                        </table>
                        {{-- <div class="pagination-wrapper"> {!! $packages->appends(['search' => Request::get('search')])->render() !!} </div> --}}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection



@push('js')

    <script>

        var url ="{{ url('admin/packages-data') }}";
        var edit_url = "{{ url('admin/packages') }}";
    
        
        datatable = $('#packages-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]], 
            ajax: {
                url:url,
                type:"get",
                data: function (d)  {
                   
                }
            },
                columns: [
                    { data: 'id', name: 'id',  "orderable":true,searchable:true },
                    { data: 'title', name: 'title', "orderable":true, searchable:true },
                    { data: 'qr_code_no', name: 'qr_code_no', "orderable":true, searchable:true },
                    { data: 'type', name: 'type', "orderable":true, searchable:true },
                    { 
                        "data": null,
                        "name" : 'amount',
                        "searchable": true,
                        "orderable": true,
                        "render": function (o) {
                            if(o.type == 'Free')
                                return o.amount+" USD";
                            else
                                return o.amount+" "+o.currency;                        
                        }
                    
                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var v="" ; var e=""; var d=""; 
                            
                            v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i>View</button></a>&nbsp;";

                            
                            e= "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" class='edit-item' ><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>Edit</button></a>&nbsp;";
                            
                               
                            
                            d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" ><button class='btn btn-danger btn-xs'><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a>&nbsp;";
                            

                            
                            return v+e+d;
                        }
                    
                    
                    }
                ]
        });

        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            url = "{{url('admin/packages')}}/" + id;
            var r = confirm("Are you sure you want to Delete Package ?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(data.messages);
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            }
        });

    </Script>
    
@endpush
