@extends('layouts.backend')

@section('content')
        <div class="row">
            <div class="col-md-12">
             <div class="box bordered-box blue-border">
                                          <div class="box-header blue-background">
                                                              <div class="title">
                                                                  <i class="icon-circle-blank"></i>
                                                               Package #{{ $package->title }}
                                                              </div>

                                           </div>
                                           <div class="box-content ">

                        <a href="{{ url('/admin/packages') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        @if(Auth::user()->can('access.packages.edit'))
                        <a href="{{ url('/admin/packages/' . $package->id . '/edit') }}" title="Edit Qrcode"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                       @endif
                       @if(Auth::user()->can('access.packages.delete'))
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/packages', $package->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Package',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        @endif    
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $package->id }}</td>
                                    </tr>
                                    <tr><th> Title </th><td> {{ $package->title }} </td></tr>
                                    <tr><th> Description </th><td> {{ $package->description }} </td></tr>
                                    <tr><th> Points</th><td> {{ $package->qr_code_no }} </td></tr>
                                   {{--   <tr><td> {!!  DNS2D::getBarcodeHTML($qrcode->hash, "QRCODE")  !!} </td></tr> --}}
                                    <tr><th> Type </th><td> {{ $package->type }} </td></tr>
                                    <tr><th> Price </th><td>{{ $package->amount }} {{ $package->currency }} </td></tr>
                                    <tr><th> Created On </th><td>  {{ $package->created_at }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
