@extends('layouts.backend')

@section('title',__('Edit Profile'))

@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{__('Edit Profile')}}</div>
                <div class="panel-body">

                    <a href="{{ route('profile.index') }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                            Back
                        </button>

                    </a>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($user,[
                        'method' => 'PATCH',
                        'class' => 'form-horizontal',
                        'files'=>true
                    ]) !!}

                    {{--                    @include('admin.for_master.site_input')--}}


                    <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
                        {!! Form::label('name', 'Name: (*)', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
                        {!! Form::label('email', 'Email: ', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            {{--<p style="border: 1px solid #d0d0d0; padding: 5px">{{$user->email}}</p>--}}
                            {!! Form::email('email', isset($user->email)?$user->email:old('email'), ['class' => 'form-control', 'required' => 'required','disabled'=>true]) !!}
                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : ''}}">
                            {!! Form::label('phone', 'Phone: (*) ', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {{--<p style="border: 1px solid #d0d0d0; padding: 5px">{{$user->email}}</p>--}}
                                {!! Form::text('phone', isset($user->phone)?$user->phone:old('phone'), ['class' => 'form-control', 'required' => 'required']) !!}
                                {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    
                        <div class="form-group">
                            @if($user->image)
                                <div class="col-md-6 col-md-offset-4">
                                    <img src="{!! asset('/images/'.$user->image) !!}" alt="" width="150px">
                                </div>
                            @endif
                        </div>
                        
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                            @if($user->image)
                                {!! Form::label('image', trans('people.label.change_photo'), ['class' => 'col-md-4 control-label']) !!}
                            @else
                                {!! Form::label('image', trans('people.label.photo'), ['class' => 'col-md-4 control-label']) !!}
                            @endif
                            <div class="col-md-6">
                                {!! Form::file('image',  ['class' => 'form-control']) !!}
                                {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                    


                    @if($user->people)

                        @include('admin.profile.peopleForm')


                    @endif
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-4">
                            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update Profile', ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}


                </div>
            </div>
        </div>
    </div>
@endsection