@extends('layouts.backend')


@section('title','My Profile')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">My Profile</div>
                <div class="panel-body">
                    <div class="row">


                        <div class="col-md-6">

                            <a href="{{ route('profile.edit') }}" class="btn btn-success btn-sm"
                               title="Edit Profile">
                                <i class="fa fa-edit" aria-hidden="true"></i> Edit Profile
                            </a>
                            <a href="{{ route('profile.password') }}" class="btn btn-info btn-sm"
                               title="Change Password">
                                <i class="fa fa-lock" aria-hidden="true"></i> Change Password
                            </a>
                        </div>

                        <div class="col-md-6">
                        </div>
                    </div>
                    <hr>

                    <div class="table-responsive">
                       
                        <table class="table table-borderless">

                            <tr>
                                <td class="col-md-2">Name</td>
                                <td>{{$user->name}}</td>
                            </tr>

                            <tr>
                                <td>Email</td>
                                <td>{{$user->email}}</td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td>{{$user->phone}}</td>
                            </tr>

                            <tr>
                                <td>Joined</td>
                                <td>{{$user->created_at->diffForHumans()}}</td>
                            </tr>


                        </table>
                    </div>


                    @if($user->people)
                        <h2>Person details</h2>
                        <div class="table-responsive">
                            <table class="table table-borderless">

                                <tr>
                                    <td class="col-md-2">First Name</td>
                                    <td>{{$user->people->first_name}}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2">Last Name</td>
                                    <td>{{$user->people->last_name}}</td>
                                </tr>

                                <tr>
                                    <td>Phone number 1</td>
                                    <td>{{$user->people->phone_number_1}}</td>
                                </tr>

                                <tr>
                                    <td>Phone type 1</td>
                                    <td>{{$user->people->phone_type_1}}</td>
                                </tr>


                                <tr>
                                    <td>Phone number 2</td>
                                    <td>{{$user->people->phone_number_2 or null}}</td>
                                </tr>

                                <tr>
                                    <td>Phone type 2</td>
                                    <td>{{$user->people->phone_type_2 or null}}</td>
                                </tr>


                                <tr>
                                    <td>photo</td>
                                    <td>
                                        @if($user->photo)
                                            <img src="{!! asset('uploads/'.$user->photo) !!}" alt="" width="150">
                                        @endif
                                    </td>
                                </tr>


                            </table>
                        </div>

                    @endif

                    {{--<pre>--}}
                    {{--{!! json_encode(Auth::user(),JSON_PRETTY_PRINT) !!}--}}
                    {{--</pre>--}}

                </div>
            </div>
        </div>
    </div>
@endsection