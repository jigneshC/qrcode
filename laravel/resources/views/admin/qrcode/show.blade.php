@extends('layouts.backend')
@section('title','Show QR Code')
@section('content')
        <div class="row">
            <div class="col-md-12">
             <div class="box bordered-box blue-border">
                                          <div class="box-header blue-background">
                                                              <div class="title">
                                                                  <i class="icon-circle-blank"></i>
                                                               QR Code {{ $qrcode->id }}
                                                              </div>

                                           </div>
                                           <div class="box-content ">

                        <a href="{{ url('/admin/qrcode') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/qrcode/' . $qrcode->id . '/edit') }}" title="Edit Qrcode"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/qrcode', $qrcode->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Qrcode',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $qrcode->id }}</td>
                                    </tr>
                                    <tr><th> Name </th><td> {{ $qrcode->name }} </td></tr><tr><th> Hash </th><td> {{ $qrcode->hash }} </td></tr><tr><th> Redirectlink </th><td> {{ $qrcode->redirectlink }} </td></tr>
                                   {{--   <tr><td> {!!  DNS2D::getBarcodeHTML($qrcode->hash, "QRCODE")  !!} </td></tr> --}}
                                    <tr><th> QR Code </th><td> <img src="{!! asset('uploads/qrcode') !!}/{{ $qrcode->hash }}.jpg" alt="barcode"  height="300" /> </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
