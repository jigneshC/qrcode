{{--
<div class="form-group {{ $errors->has('duration') ? 'has-error' : ''}}">
    {!! Form::label('duration', 'Duration', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('duration',\config('site_setting.duration'),null, ['class' => 'form-control']) !!}
        {!! $errors->first('duration', '<p class="help-block">:message</p>') !!}
    </div>
</div>
--}}
<div class="form-group {{ $errors->has('years') ? 'has-error' : ''}}">
        {!! Form::label('years', 'Years :', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::number('years', null, ['class' => 'form-control', 'id' => 'yeasr','Placeholder'=>'Enter number of Years','min'=>'0', 'max'=>'99' ,'step'=>'1']) !!}
            {!! $errors->first('years', '<p class="help-block">:message</p>') !!}
        </div>
</div>

<div class="form-group {{ $errors->has('months') ? 'has-error' : ''}}">
            {!! Form::label('months', 'Months :', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::number('months', null, ['class' => 'form-control','id' => 'months','Placeholder'=>'Enter Number of Months','min'=>'0', 'max'=>'12' ,'step'=>'1']) !!}
                {!! $errors->first('months', '<p class="help-block">:message</p>') !!}
            </div>
</div>
<div class="form-group {{ $errors->has('days') ? 'has-error' : ''}}">
        {!! Form::label('days', 'Days :', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::number('days', null, ['class' => 'form-control','id' => 'days','Placeholder'=>'Enter Number of Days','min'=>'0', 'max'=>'31' ,'step'=>'1']) !!}
            {!! $errors->first('days', '<p class="help-block">:message</p>') !!}
        </div>
</div>

<div class="form-group {{ $errors->has('points') ? 'has-error' : ''}}">
    {!! Form::label('points', 'Points :', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('points', null, ['class' => 'form-control','min'=>'1','step'=>'any']) !!}
        {!! $errors->first('points', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
