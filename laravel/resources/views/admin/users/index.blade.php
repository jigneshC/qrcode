@extends('layouts.backend')

@section('title','Users')
@section('pageTitle','Users')




@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Users</div>
                <div class="panel-body">


                    <div class="row">
                        <div class="col-md-6">
                            @if(Auth::user()->can('access.user.create'))
                                <a href="{{ url('/admin/users/create') }}" class="btn btn-success btn-sm"
                                   title="Add New User">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                                </a>

                            @endif
                        </div>

                        {{-- <div class="col-md-6">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/users', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}


                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..."
                                       value="{!! request()->get('search') !!}">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            {!! Form::close() !!}

                        </div> --}}
                    </div>


                    <div class="table-responsive">
                        <table class="table table-borderless" id="users-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Type</th>
                                <th>Points Remaining</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            {{-- <tbody>
                            @foreach($users as $item)

                                
                                 $role = join(' + ', $item->roles()->pluck('label')->toArray());
                                ?>


                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td><a href="{{ url('/admin/users', $item->id) }}">{{ $item->name }}</a></td>
                                    <td>{{ $item->email }}</td>
                                    <!-- <td>{{ $role }}</td> -->
                                    <td>{{ str_replace("_"," ",ucfirst($item->type) ) }}</td>  
                                    <td>{{ $item->points }}</td>
                                    <!--
                                    <td>
                                            <a href="{{ url('/admin/qrcode') }}?uid={{$item->id}}" title="Qrcodes">
                                                <button class="btn btn-warning btn-sm"> {{count($item->qrcodes)}}
                                                </button>
                                            </a> 
                                    </td>
                                    -->
                                    <td>

                                        <a href="{{ url('/admin/users/' . $item->id) }}" title="View User">
                                            <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                   aria-hidden="true"></i> View
                                            </button>
                                        </a>

                                        @if(Auth::user()->can('access.user.edit'))

                                            <a href="{{ url('/admin/users/' . $item->id . '/edit') }}"
                                               title="Edit User">
                                                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                          aria-hidden="true"></i> Edit
                                                </button>
                                            </a>
                                        @endif
                                             
                                        @if(Auth::user()->can('access.user.delete'))
                                            @if($item->id != 1) 
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/admin/users', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-danger btn-xs',
                                                    'title' => 'Delete User',
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                            {!! Form::close() !!}
                                            @endif
                                        @endif

                                    </td>
                                </tr>
                            @endforeach
                            </tbody> --}}
                        </table>
                        {{-- <div class="pagination"> {!! $users->appends(['search' => Request::get('search')])->render() !!} </div> --}}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

    <script>

        var url ="{{ url('admin/users-data') }}";
        var edit_url = "{{ url('admin/users') }}";
        var qr_url = "{{ url('admin/qrcode') }}";

        //Permissions
        var edit_user ="{{ Auth::user()->can('access.user.edit') }}";
        var delete_user ="{{ Auth::user()->can('access.user.delete') }}";
        
    
        
        datatable = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]], 
            ajax: {
                url:url,
                type:"get",
                data: function (d)  {
                   
                }
            },
                columns: [
                    { data: 'id', name: 'id',  "orderable":true,searchable:true },
                    { data: 'name', name: 'name',  "orderable":true,searchable:true },
                    { data: 'email', name: 'email', "orderable":true, searchable:true },
                  
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "defaultContent": "User",
                        "render": function (o) {
                           
                            var type = (o.type).replace(/_/, "");
                            return type.charAt(0).toUpperCase() + type.slice(1);
                        }
                    
                    
                    },
                   
                    { data: 'points', name: 'points', "orderable":true, searchable:true },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var v="" ; var e=""; var d="";  var qrcodes="";
                            
                            v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i>View</button></a>&nbsp;";

                            if(edit_user){
                                e= "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" class='edit-item' ><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>Edit</button></a>&nbsp;";
                            }
                               
                            if(delete_user && o.id != 1){
                                d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" ><button class='btn btn-danger btn-xs'><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a>&nbsp;";
                            }
                            
                            qrcodes = "<a href='"+qr_url+"?uid="+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-xs'><i class='fa fa-qrcode' aria-hidden='true'></i> "+ o.qrcodes.length +"</button></a>&nbsp;";

                            
                            return v+e+d+qrcodes;
                        }
                    
                    
                    }
                ]
        });

        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            url = "{{url('admin/users')}}/" + id;
            var r = confirm("Are you sure you want to delete this User ?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(data.messages)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            }
        });

    </Script>
    
@endpush