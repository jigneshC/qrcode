@extends('layouts.backend')


@section('title',__('login.access_denied'))


@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Oops !</div>

                <div class="panel-body">
                    @lang('login.dont_have_permission')
                </div>
            </div>
        </div>
    </div>
@endsection

