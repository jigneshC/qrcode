@extends('layouts.app')

@section('title',__('login.login'))

@section('content')

<div class="card">
        <h3 class="text-center">@lang('login.login')</h3>
        <form class='validate-form' role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}


                <div class='form-group{{ $errors->has('email') ? ' has-error' : '' }}'>
                    <div class='controls with-icon-over-input'>
                        <label>@lang('login.email_address')</label>
                        <input value="" placeholder="E-mail" class="form-control" data-rule-required="true"
                               name="email" type="text" value="{{ old('email') }}"/>
                        <i class='icon-user text-muted'></i>
                    </div>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                    @endif
                </div>


                <div class='form-group{{ $errors->has('password') ? ' has-error' : '' }}'>
                    <div class='controls with-icon-over-input'>
                            <label>@lang('login.password')</label>
                        <input value="" placeholder="Password" class="form-control" data-rule-required="true"
                               name="password" type="password"/>
                        <i class='icon-lock text-muted'></i>
                    </div>
                    @if ($errors->has('password'))
                        <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                    @endif
                </div>
                <div class='checkbox-1'>
                    
                    <input id='remember_me' name='remember_me' type='checkbox' style=" height: 20px; width: 20px;" value='1' {{ old('remember') ? 'checked' : '' }}><span style="padding-left: 15px;">@lang('login.remember_me')</span>
                               
                </div>
                <button class='btn btn-block' type="submit">@lang('login.sign_in')</button>
            </form>
            <div class='text-center'>
                <hr class='hr-normal'>
                <a href='{{ route('password.request') }}'>@lang('login.forgot_your_password')</a><br>
                
                        <i class='icon-user'></i>
                        @lang('login.new')
                    <a href='{!! route('register') !!}'>
                        <strong>@lang('login.signup')</strong>
                    </a>
            </div>

        
    </div>


{{--
    <div class='login-container'>
        <div class='container'>
            <div class='row'>

                <div class='col-sm-4 col-sm-offset-4'>
                    <h1 class='text-center title'>Log in</h1>
                    <form class='validate-form' role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}


                        <div class='form-group{{ $errors->has('email') ? ' has-error' : '' }}'>
                            <div class='controls with-icon-over-input'>
                                <input value="" placeholder="E-mail" class="form-control" data-rule-required="true"
                                       name="email" type="text" value="{{ old('email') }}"/>
                                <i class='icon-user text-muted'></i>
                            </div>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>


                        <div class='form-group{{ $errors->has('password') ? ' has-error' : '' }}'>
                            <div class='controls with-icon-over-input'>
                                <input value="" placeholder="Password" class="form-control" data-rule-required="true"
                                       name="password" type="password"/>
                                <i class='icon-lock text-muted'></i>
                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class='checkbox'>
                            <label for='remember_me'>
                                <input id='remember_me' name='remember_me' type='checkbox'
                                       value='1' {{ old('remember') ? 'checked' : '' }}>
                                Remember me
                            </label>
                        </div>
                        <button class='btn btn-block' type="submit">Sign in</button>
                    </form>
                    <div class='text-center'>
                        <hr class='hr-normal'>
                        <a href='{{ route('password.request') }}'>Forgot your password?</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class='login-container-footer'>
        <div class='container'>
            <div class='row'>
                <div class='col-sm-12'>
                    <div class='text-center'>
                        <a href='{!! route('register') !!}'>
                            <i class='icon-user'></i>
                            New?
                            <strong>Sign up</strong>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
--}}

@endsection



{{--<div class="container">--}}
{{--<div class="row">--}}
{{--<div class="col-md-8 col-md-offset-2">--}}
{{--<div class="panel panel-default">--}}
{{--<div class="panel-heading">Login</div>--}}
{{--<div class="panel-body">--}}
{{--<form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">--}}
{{--{{ csrf_field() }}--}}

{{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
{{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

{{--<div class="col-md-6">--}}
{{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>--}}

{{--@if ($errors->has('email'))--}}
{{--<span class="help-block">--}}
{{--<strong>{{ $errors->first('email') }}</strong>--}}
{{--</span>--}}
{{--@endif--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
{{--<label for="password" class="col-md-4 control-label">Password</label>--}}

{{--<div class="col-md-6">--}}
{{--<input id="password" type="password" class="form-control" name="password" required>--}}

{{--@if ($errors->has('password'))--}}
{{--<span class="help-block">--}}
{{--<strong>{{ $errors->first('password') }}</strong>--}}
{{--</span>--}}
{{--@endif--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="form-group">--}}
{{--<div class="col-md-6 col-md-offset-4">--}}
{{--<div class="checkbox">--}}
{{--<label>--}}
{{--<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me--}}
{{--</label>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="form-group">--}}
{{--<div class="col-md-8 col-md-offset-4">--}}
{{--<button type="submit" class="btn btn-primary">--}}
{{--Login--}}
{{--</button>--}}

{{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
{{--Forgot Your Password?--}}
{{--</a>--}}
{{--</div>--}}
{{--</div>--}}
{{--</form>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}