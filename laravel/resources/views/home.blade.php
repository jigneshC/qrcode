{{--
@extends('layouts.backend')


@section('title','Dashboard')


@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{__('Dashboard')}}</div>
                
                <div class="panel-body">
                    @if(\Auth::check())
                    {{__('You are logged in!')}}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
--}}

@extends('layouts.main')

@section('title','QRCODES')

@section('content')

<header>
    <div class="header-div clearfix">
    <div class="header-top-div">
    <div class="container">
        <div class="clearfix">
        <div class="logo-div">
            <a href="#"><img src="{{asset('assets/images/logo.png')}}" alt=""></a>
        </div>
        <div class="top-link-div">
            @if(Auth::guest())
                <a href="{{route('register')}}" class="signup-link">@lang('home.signup')</a>
                <a href="{{route('login')}}" class="login-link">@lang('home.login')</a>
            @else
                @if(Auth::user()->type == 'user')
                <a href="{{url('/dashboard')}}" class="login-link-1">@lang('home.welcome') {{Auth::user()->name}}</a>
                @else
                <a href="{{url('/admin')}}" class="login-link">@lang('home.welcome') {{Auth::user()->name}}</a>
                @endif
                <a href="{{ url('/logout') }}" class="login-link"
                onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                 <i class='icon-signout'></i>
                 @lang('home.logout') 
             </a>

             <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                   style="display: none;">
                 {{ csrf_field() }}
             </form>
                
            @endif
                <a href="#" class="dropdown-toggle signup-link" data-toggle="dropdown">
                    <i class="fa fa-globe space-5"></i><span class="space-5-1">@lang('home.language')</span>
                </a>
                <div class="dropdown-menu dropdown-menu-large small-dropdown row animated fadeInUp">
                    <li class="col-sm-12">
                        <h4 class="dropdown_header">@lang('home.language')</h4>
                        <ul class="categories-links">
                             @foreach($_lang as $lang)
                            <li>
                                <a href="{{request()->fullUrlWithQuery(['lang'=> $lang->lang_code])}}" class="color-black">
                                    @if($lang->lang_code == 'en')
                                    <img src="{{asset('assets/images/en.png')}}" class="img-padd10"  />
                                    @elseif($lang->lang_code == 'fr')
                                    <img src="{{asset('assets/images/fr.png')}}" class="img-padd10" />
                                    @else
                                    @endif
                                    {{$lang->name}} </a>
                            </li>
                            @endforeach


                        </ul>
                    </li>
                </div>
            
        </div>
        </div>
     </div> 
     </div><!-- end of header-top-div -->  

     @include("modal.freeqr")

      
     <div class="container"> 
        <div class="banner-caption-div clearfix">
            <h1>@lang('home.content.top.h1')</h1>
            <h2>@lang('home.content.top.h2')</h2>
            
            <p>@lang('home.content.top.p1')<br>@lang('home.content.top.p2')</p>
            
            <div class="banner-btn-div clearfix">
                <a href="{{url('/packages')}}" class="getstarted-link">@lang('home.get_started')</a>
                <a href='#' class='open_model_for_freeqr free-link' data-packageid='{{$free->id}}'>@lang('home.create_a_qr_code_for_free')</a>
            </div>  
  
        </div><!-- end of banner caption div -->
        
    </div>
    
    <div class="hero-img"></div>
<div class="phy-here-img-1"><img src="{{asset('assets/images/hero-qr-code-1.png')}}" alt="" class="img-responsive"></div>
    </div><!-- end of header-div -->
    
    
    
    <div class="hero-banner-div"></div>
</header>


<div class="content-area clearfix">
    
    	<div class="intro-div clearfix">
         <div class="phy-here-img-2"><img src="{{asset('assets/images/hero-qr-code-2.png')}}" alt="" class="img-responsive"></div>
    	<div class="qrcode"></div>
        <div class="container">
        <div class="row">
            	<div class="col-md-7 col-sm-7">
                    <h2>@lang('home.content.section_marketing.heading')</h2>
                    <?php $p_content = Lang::get('home.content.section_marketing.p'); ?>
                    @foreach( $p_content as $p_contents)
                    <p>{{$p_contents}}</p>
                    @endforeach

                    
                    <div class="btn-link-div clearfix">
                        {{-- 
                            {!! Form::open(['url' => '/orders', 'class' => 'form', 'files' => true]) !!}
                            <input type="hidden" name="package_id" value="{{ $free->id }}">
                             {!! Form::submit(trans('home.create_a_qr_code_for_free'),['class'=>'home_page_free_qr btn btn-info'] ) !!}
                            {!! Form::close() !!}
                         --}}
                        <a href='#' class='open_model_for_freeqr free-link' data-packageid='{{$free->id}}'>@lang('home.create_a_qr_code_for_free')</a>
                    </div>

                </div>
       </div><!-- end of row -->
       </div><!-- end of container -->
       </div><!-- end of intro div -->
            
            <div class="qr-generator-div clearfix">
            <div class="container">
            <div class="row">
            	<h2 class="block-title text-center">@lang('home.content.section_professional.heading')</h2>
                
                <div class="qr-tab-div clearfix">
                    <div class="col-md-4 col-sm-6  col-mb-2">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-right">
                            <li class="active" data-toggle="tooltip" data-placement="top" title="Create Unique QR Codes"><a href="#tab-1" data-toggle="tab" ><span class="tab-1-icon"></span><span class="txt"> @lang('home.content.section_professional.tab_1.li') </span></a></li>
                            <li data-toggle="tooltip" data-placement="top" title="Update QR Codes When You Need"><a href="#tab-2" data-toggle="tab"><span class="tab-2-icon"></span><span class="txt">@lang('home.content.section_professional.tab_2.li') </span></a></li>
                            <li data-toggle="tooltip" data-placement="top" title="Achieve High-Quality Results"><a href="#tab-3" data-toggle="tab"><span class="tab-3-icon"></span><span class="txt">@lang('home.content.section_professional.tab_3.li')</span></a></li>
                            {{-- <li data-toggle="tooltip" data-placement="top" title="Share Your Account With Others"><a href="#tab-4" data-toggle="tab"><span class="tab-4-icon"></span><span class="txt">@lang('home.content.section_professional.tab_4.li')</span></a></li> --}}
                            <li data-toggle="tooltip" data-placement="top" title="Get User Help And Support"><a href="#tab-5" data-toggle="tab"><span class="tab-5-icon"></span><span class="txt">@lang('home.content.section_professional.tab_5.li')</span></a></li>                            
                        </ul>
                    </div>
                    
                
                	<div class="col-md-8 col-sm-6  col-mb-1">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-1">
                            	<div class="qr-tab-content clearfix">
                                	<p> @lang('home.content.section_professional.tab_1.p')</p>
                                </div>
                            </div><!-- end of tab -->
                            
                            <div class="tab-pane" id="tab-2">
                            	<div class="qr-tab-content clearfix">
                                	<p>@lang('home.content.section_professional.tab_2.p')</p>
                                </div>
                            </div><!-- end of tab -->
                            
                            <div class="tab-pane" id="tab-3">
                            	<div class="qr-tab-content clearfix">
                                	<p>@lang('home.content.section_professional.tab_3.p')</p>
                                </div>
                            </div><!-- end of tab -->
                            
                            {{-- <div class="tab-pane" id="tab-4">
                            	<div class="qr-tab-content clearfix">
                                	<p>@lang('home.content.section_professional.tab_4.p')</p>
                                </div>
                            </div><!-- end of tab --> --}}
                            
                            <div class="tab-pane" id="tab-5">
                            	<div class="qr-tab-content clearfix">
                                	<p>@lang('home.content.section_professional.tab_5.p')</p>
                                </div>
                            </div><!-- end of tab -->
                            
                        </div> <!-- Tab panes -->
                    </div>

                </div><!-- end of qr-tab-div -->
                
                <div class="btn-link-div text-center clearfix">
                    {{--
                        {!! Form::open(['url' => '/orders', 'class' => 'form', 'files' => true]) !!}
                        <input type="hidden" name="package_id" value="{{ $free->id }}">
                         {!! Form::submit(trans('home.create_a_qr_code_for_free'),['class'=>'home_page_free_qr btn btn-info'] ) !!}
                        {!! Form::close() !!}
                    --}}
                        <a href='#' class='open_model_for_freeqr free-link' data-packageid='{{$free->id}}'>@lang('home.create_a_qr_code_for_free')</a>
                    
                    </div>
            
            </div><!-- end of qr-generator-div -->
            
            </div><!-- end of row -->
            </div><!-- end of container -->
      
       

            <div class="two-block-div clearfix">
        
        <div class="container">
        <div class="row">
        
            <div class="col-md-6 col-sm-6">
                <div class="two-block-box clearfix">
                    <h2>@lang('package.content.section_competition.heading')</h2>
                    <ul class="chkmark-ul-white">
                        <?php $li_content = Lang::get('package.content.section_competition.li'); ?>
                        @foreach( $li_content as $li_contents)
                        <li>{{$li_contents}}</li>
                        @endforeach

                    </ul>
                    <div class="white-btn-link-div clearfix">
                        <a href="#" class='open_model_for_freeqr' data-packageid='{{$free->id}}' >@lang('package.create_a_qr_code_for_free')</a>
                    </div>
                    
                </div>
            
            </div><!-- end of col -->
            
            <div class="col-md-6 col-sm-6">
                <div class="two-block-box-2 clearfix">
                    <h2>@lang('package.content.section_question.heading')</h2>
                    <p>@lang('package.content.section_question.p')</p>
                    <ol class="white-ol">
                            <?php $li_content = Lang::get('package.content.section_question.li'); ?>
                            @foreach( $li_content as $li_contents)
                            <li>{{$li_contents}}</li>
                            @endforeach

                    </ol>
                    
                    <div class="white-btn-link-div clearfix">
                        <a href="#" class='open_model_for_freeqr free-link' data-packageid='{{$free->id}}'>@lang('package.create_a_qr_code_for_free') </a>
                    </div>

                </div>
            
            </div><!-- end of col -->
        
            </div><!-- end of row -->
            </div><!-- end of container -->
        
        </div><!-- end of two-block-div -->            
           
            
            
            
            <div class="price-plan-div clearfix">
            <div class="container">
            <div class="row">
                
                	<h2 class="block-title text-center">@lang('home.content.section_plan.heading_2')</h2>
                	<h3 class="sub-block-title text-center">@lang('home.content.section_plan.heading_3')</h3>
                
                	<div class="price-plan-block price-plan-block-1">
                    	<div class="top-block clearfix">
                        	<h2>@lang('home.content.section_plan.free_trial')</h2>
                            <h3>{{$free->title}}</h3>
                        </div>
                        <div class="btm-block clearfix">
                        	<h4>{{$free->type}}</h4>
                            
                            
                            <div class="block-txt clearfix">
                            	{{-- <dd class="clearfix"><span class="txt-1">{{$free->qr_code_no}}</span><span class="txt-2">@lang('home.content.section_plan.points_earn')</span></dd>
                                <dd class="clearfix"><dd class="clearfix"><span class="txt-1">1</span><span class="txt-2">@lang('home.content.section_plan.per_account_user')</span></dd> --}}
                                <dd class="clearfix"><span class="txt-1">Unlimited</span><span class="txt-2">@lang('home.content.section_plan.scans')</span></dd>
                                {{--
                                <dd class="clearfix"><span class="txt-1">24 hrs.</span><span class="txt-2">QR Code Validity</span></dd>
                                --}}
                                {{-- <dd class="clearfix"><span class="txt-1"><img src="{{asset('assets/images/clr-checkmark.png')}}" alt=""></span><span class="txt-2">@lang('home.content.section_plan.formats')</span></dd> --}}
                                <dd class="clearfix"><span class="txt-1">-</span><span class="txt-2">@lang('home.content.section_plan.customer_support')</span></dd>
                                <dd class="clearfix"><span class="txt-1">-</span><span class="txt-2">@lang('home.content.section_plan.api')</span></dd>
                               <dd class="clearfix"><span class="txt-1">-</span><span class="txt-2">@lang('home.content.section_plan.tracking')</span></dd>
                            </div><!-- end block-txt -->
      
                        </div><!-- end of btm bblock -->
                        
                        <div class="btm-buy-now clearfix">
                                <a href='#' class='open_model_for_freeqr ' data-packageid='{{$free->id}}'>@lang('home.buy_now')</a>
                        	
                        </div>
                    
                    </div><!-- end of price-plan-block -->
                    
                    <div class="price-plan-block price-plan-block-2">
                    	<div class="top-block clearfix">
                        	<h2>@lang('home.content.section_plan.most_popular')</h2>
                            <h3>{{$popular->title}}</h3>
                        </div>
                        <div class="btm-block clearfix">
                        	<h4>{{ number_format((float)($popular_rate['amount']/$popular->qr_code_no), 2, '.', '')}} <span> {{$popular_rate['toCode']}}</span></h4>
                            
                            
                            <div class="block-txt clearfix">
                            	{{-- <dd class="clearfix"><span class="txt-1">{{$popular->qr_code_no}}</span><span class="txt-2">@lang('home.content.section_plan.points_earn')</span></dd>
                                <dd class="clearfix"><span class="txt-1">Unlimited</span><span class="txt-2">@lang('home.content.section_plan.per_account_user')</span></dd> --}}
                                <dd class="clearfix"><span class="txt-1">Unlimited</span><span class="txt-2">@lang('home.content.section_plan.scans')</span></dd>
                                <dd class="clearfix">
                                {{-- <dd class="clearfix"><span class="txt-1"><img src="{{asset('assets/images/clr-checkmark.png')}}" alt=""></span><span class="txt-2">@lang('home.content.section_plan.formats')</span></dd> --}}
                                <dd class="clearfix"><span class="txt-1">-</span><span class="txt-2"> @lang('home.content.section_plan.customer_support')</span></dd>
                                <dd class="clearfix"><span class="txt-1">-</span><span class="txt-2">@lang('home.content.section_plan.api')</span></dd>
                               <dd class="clearfix"><span class="txt-1">-</span><span class="txt-2"> @lang('home.content.section_plan.tracking')</span></dd>
                            </div><!-- end block-txt -->
      
                        </div><!-- end of btm bblock -->
                        
                        <div class="btm-buy-now clearfix">
                                {{-- {!! Form::open(['url' => '/orders', 'class' => 'form', 'files' => true]) !!}
                                <input type="hidden" name="package_id" value="{{ $popular->id }}">
                                 {!! Form::submit( trans('package.buy_now'),['class' => 'btn btn-default btn-1']) !!}
                                {!! Form::close() !!} --}}
                                <a href="{{ url('/orders/create/'.$popular->id) }}">@lang('home.buy_now')</a>
                        </div>
                    
                    </div><!-- end of price-plan-block -->
                    
                    <div class="price-plan-block price-plan-block-1">
                    	<div class="top-block clearfix">
                        	<h2>@lang('home.content.section_plan.business')</h2>
                            <h3>{{$pricing->title}}</h3>
                        </div>
                        <div class="btm-block clearfix">
                        	<h4>{{ number_format((float)($pricing_rate['amount']/$pricing->qr_code_no), 2, '.', '')}} <span> {{$pricing_rate['toCode']}}</span></h4>
                            
                            
                            <div class="block-txt clearfix">
                            	{{-- <dd class="clearfix"><span class="txt-1">{{$pricing->qr_code_no}}</span><span class="txt-2">@lang('home.content.section_plan.points_earn')</span></dd>
                                <dd class="clearfix"><span class="txt-1">Unlimited</span><span class="txt-2">@lang('home.content.section_plan.per_account_user')</span></dd> --}}
                                <dd class="clearfix"><span class="txt-1">Unlimited</span><span class="txt-2">@lang('home.content.section_plan.scans')</span></dd>
                                <dd class="clearfix">
                                {{-- <dd class="clearfix"><span class="txt-1"><img src="{{asset('assets/images/clr-checkmark.png')}}" alt=""></span><span class="txt-2">@lang('home.content.section_plan.formats')</span></dd> --}}
                                <dd class="clearfix"><span class="txt-1"><img src="{{asset('assets/images/clr-checkmark.png')}}" alt=""></span><span class="txt-2"> @lang('home.content.section_plan.customer_support')</span></dd>
                                <dd class="clearfix"><span class="txt-1"><img src="{{asset('assets/images/clr-checkmark.png')}}" alt=""></span><span class="txt-2">@lang('home.content.section_plan.api')</span></dd>
                               <dd class="clearfix"><span class="txt-1"><img src="{{asset('assets/images/clr-checkmark.png')}}" alt=""></span><span class="txt-2"> @lang('home.content.section_plan.tracking')</span></dd>
                            </div><!-- end block-txt -->
      
                        </div><!-- end of btm bblock -->
                        
                        <div class="btm-buy-now clearfix">
                                {{-- {!! Form::open(['url' => '/orders', 'class' => 'form', 'files' => true]) !!}
                                <input type="hidden" name="package_id" value="{{ $pricing->id }}">
                                 {!! Form::submit( trans('package.buy_now'),['class' => 'btn btn-default btn-2']) !!}
                                {!! Form::close() !!} --}}
                                <a href="{{ url('/orders/create/'.$pricing->id) }}">@lang('home.buy_now')</a>
                        </div>
                    
                    </div><!-- end of price-plan-block -->
                    
                     </div><!-- end of row -->
                        </div><!-- end of container -->
                        
                        <div class="padd-custom text-center">
                    <a href='{{url('/packages')}}' class='btn btn-info' >@lang('home.view_all_packages')</a>
                        </div>
            
                </div><!-- end of price-plan-div -->


               
                
                
                 <div class="container">
                 <div class="row">
                 
                     <div class="faq-div clearfix">
                     	
                        <h2 class="block-title text-center">@lang('home.content.faq.heading')</h2>
                        
                        <div class="faq-accordian-div clearfix">
                        	
                            <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                     <h4 class="panel-title">
                                        <a class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion" href="#panel1"><i class="more-less icon-minus"></i><span class="accordion-txt">@lang('home.content.faq.q1')</span></a>
                                    </h4>
                        
                                </div>
                                <div id="panel1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                    	 <div class="faq-txt">
                                              	<p>@lang('home.content.faq.ans1')</p>
                                                <p>@lang('home.content.faq.ans1_a')</p>

                                              </div>
                                    </div>
                                </div>
                            </div><!-- end of panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                     <h4 class="panel-title">
                                        <a class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion" href="#panel2"><i class="more-less icon-plus"></i><span class="accordion-txt">@lang('home.content.faq.q2')</span></a>
                                    </h4>
                        
                                </div>
                                <div id="panel2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                    
                                    	<div class="faq-txt">
                                              	<p>@lang('home.content.faq.ans2')</p>

                                              </div>
                                    </div>
                                </div>
                            </div><!-- end of panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                     <h4 class="panel-title">
                                        <a class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion" href="#panel3"><i class="more-less icon-plus"></i><span class="accordion-txt">@lang('home.content.faq.q3')</span></a>
                                    </h4>
                        
                                </div>
                                <div id="panel3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                    	<div class="faq-txt">
                                              	<p>@lang('home.content.faq.ans3')</p>

                                              </div>
                                    </div>
                                </div>
                            </div><!-- end of panel -->
                            
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                     <h4 class="panel-title">
                                        <a class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion" href="#panel4"><i class="more-less icon-plus"></i><span class="accordion-txt">@lang('home.content.faq.q4')</span></a>
                                    </h4>                        
                                </div>
                                <div id="panel4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                    	<div class="faq-txt">
                                              	<p>@lang('home.content.faq.ans4')</p>
                                         </div>
                                    </div>
                                </div>
                            </div><!-- end of panel -->
                            
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                     <h4 class="panel-title clearfix">
                                        <a class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion" href="#panel5"><i class="more-less icon-plus"></i><span class="accordion-txt">@lang('home.content.faq.q5')</span></a>
                                    </h4>                        
                                </div>
                                <div id="panel5" class="panel-collapse collapse">
                                    <div class="panel-body">
                                    	<div class="faq-txt">
                                              	<p>@lang('home.content.faq.ans5')</p>
                                         </div>
                                    </div>
                                </div>
                            </div><!-- end of panel -->
                            
                             {{-- <div class="panel panel-default">
                                <div class="panel-heading">
                                     <h4 class="panel-title clearfix">
                                        <a class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#accordion" href="#panel6"><i class="more-less icon-plus"></i><span class="accordion-txt">@lang('home.content.faq.q6')</span></a>
                                    </h4>                        
                                </div>
                                <div id="panel6" class="panel-collapse collapse">
                                    <div class="panel-body">
                                    	<div class="faq-txt">
                                              	<p>@lang('home.content.faq.ans6')</p>
                                         </div>
                                    </div>
                                </div>
                            </div><!-- end of panel --> --}}
                            
                            
                        </div><!-- end of panel group -->
                        
                        
                        <div class="btn-link-div text-center clearfix">
                            {{--
                                {!! Form::open(['url' => '/orders', 'class' => 'form', 'files' => true]) !!}
                                <input type="hidden" name="package_id" value="{{ $free->id }}">
                                 {!! Form::submit(trans('home.create_a_qr_code_for_free'),['class'=>'home_page_free_qr btn btn-info'] ) !!}
                                {!! Form::close() !!}
                                --}}
                            
                    	<a href='#' class='open_model_for_freeqr ' data-packageid='{{$free->id}}'>@lang('home.create_a_qr_code_for_free')</a>
                    </div>
                            
                            
                        </div><!-- end of faq-accordian-div -->
                     
                     </div><!-- end of faq-div -->
                
                </div><!-- end of row -->
                </div><!-- end of container -->
    	
    </div><!-- end of content-area -->

@endsection
