{{--
<!DOCTYPE html>
<html>
<head>
    <title>@yield('title','Home') | {{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta content='text/html;charset=utf-8' http-equiv='content-type'>
    <meta content='Flat administration template for Twitter Bootstrap. Twitter Bootstrap 3 template with Ruby on Rails support.'
          name='description'>
    <link href='{!! asset('assets/images/meta_icons/favicon.ico') !!}' rel='shortcut icon' type='image/x-icon'>
    <link href='{!! asset('assets/images/meta_icons/apple-touch-icon.png') !!}' rel='apple-touch-icon-precomposed'>
    <link href='{!! asset('assets/images/meta_icons/apple-touch-icon-57x57.png') !!}' rel='apple-touch-icon-precomposed'
          sizes='57x57'>
    <link href='{!! asset('assets/images/meta_icons/apple-touch-icon-72x72.png') !!}' rel='apple-touch-icon-precomposed'
          sizes='72x72'>
    <link href='{!! asset('assets/images/meta_icons/apple-touch-icon-114x114.png') !!}'
          rel='apple-touch-icon-precomposed' sizes='114x114'>
    <link href='{!! asset('assets/images/meta_icons/apple-touch-icon-144x144.png') !!}'
          rel='apple-touch-icon-precomposed' sizes='144x144'>
    <!-- / START - page related stylesheets [optional] -->

    <!-- / END - page related stylesheets [optional] -->
    <!-- / bootstrap [required] -->
    <link href="{!! asset('assets/stylesheets/bootstrap/bootstrap.css') !!}" media="all" rel="stylesheet"
          type="text/css"/>
    <!-- / theme file [required] -->
    <link href="{!! asset('assets/stylesheets/light-theme.css') !!}" media="all" id="color-settings-body-color"
          rel="stylesheet" type="text/css"/>
    <!-- / coloring file [optional] (if you are going to use custom contrast color) -->
    <link href="{!! asset('assets/stylesheets/theme-colors.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <!-- / demo file [not required!] -->
    <link href="{!! asset('assets/stylesheets/demo.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <!--[if lt IE 9]>
    <script src="{!! asset('assets/javascripts/ie/html5shiv.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('assets/javascripts/ie/respond.min.js') !!}" type="text/javascript"></script>
    <![endif]-->
</head>
<body class='contrast-red login contrast-background'>
<div class='middle-container'>
    <div class='middle-row'>
        <div class='middle-wrapper'>
            <div class='login-container-header'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <div class='text-center'>
                                QR CODE
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @yield('content')
        </div>
    </div>
</div>
<!-- / jquery [required] -->
<script src="{!! asset('assets/javascripts/jquery/jquery.min.js') !!}" type="text/javascript"></script>
<!-- / jquery mobile (for touch events) -->
<script src="{!! asset('assets/javascripts/jquery/jquery.mobile.custom.min.js') !!}" type="text/javascript"></script>
<!-- / jquery migrate (for compatibility with new jquery) [required] -->
<script src="{!! asset('assets/javascripts/jquery/jquery-migrate.min.js') !!}" type="text/javascript"></script>
<!-- / jquery ui -->
<script src="{!! asset('assets/javascripts/jquery/jquery-ui.min.js') !!}" type="text/javascript"></script>
<!-- / jQuery UI Touch Punch -->
<script src="{!! asset('assets/javascripts/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js') !!}"
        type="text/javascript"></script>
<!-- / bootstrap [required] -->
<script src="{!! asset('assets/javascripts/bootstrap/bootstrap.js') !!}" type="text/javascript"></script>
<!-- / modernizr -->
<script src="{!! asset('assets/javascripts/plugins/modernizr/modernizr.min.js') !!}" type="text/javascript"></script>
<!-- / retina -->
<script src="{!! asset('assets/javascripts/plugins/retina/retina.js') !!}" type="text/javascript"></script>
<!-- / theme file [required] -->
<script src="{!! asset('assets/javascripts/theme.js') !!}" type="text/javascript"></script>
<!-- / demo file [not required!] -->
<script src="{!! asset('assets/javascripts/demo.js') !!}" type="text/javascript"></script>
<!-- / START - page related files and scripts [optional] -->
<!-- / START - page related files and scripts [optional] -->
<script src="{!! asset('assets/javascripts/plugins/validate/jquery.validate.min.js') !!}"
        type="text/javascript"></script>
<script src="{!! asset('assets/javascripts/plugins/validate/additional-methods.js') !!}"
        type="text/javascript"></script>
<!-- / END - page related files and scripts [optional] -->
</body>
</html>
--}}



<!DOCTYPE html>
<html lang="en">

  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title')</title>
  <link href="{{asset('assets/images/favicon.png')}}" rel="icon" type="image" />
  <link href="{{asset('assets/images/favicon.ico')}}" rel='icon' type='image/ico'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{asset('assets/css/jquery-ui.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
  <link rel="canonical" href="login.html">
</head>
<body class="login-layout login">
    <main class="page-content" aria-label="Content" id="main-content">
		<div class="wrapper">
			<header class="main-header">
				<img src="{{asset('assets/images/logo.png')}}" alt="citrusbug">
            </header>
            
            @yield('content')
            {{--
			<div class="card">
				<h3 class="text-center">LOGIN</h3>
				<form action="" class="form">
					<div>
						<label for="">E-Mail Address</label>
						<input type="email">
					</div>
					
					<div>
						<label for="">Password</label>
						<input type="password">
					</div>

					<!-- <div class="checkbox padd_0 margin-left--15">
						<input type="checkbox" id="rememberme">
						<label for="rememberme">Remember Me</label>

						<a href="#" class="forgot">Forgot your password?</a>
					</div> -->

					<div class="buttons">
						<button class="button-login" href="dashboard.html">Login</button>
					</div>
				</form>
            </div>
            --}}
		</div>
    </main>
    <footer class="site-footer">
	</footer>
<!-- js -->
<script type="text/javascript" src="{{asset('assets/js/jquery-1.12.4.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/jquery-ui.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<!-- /js -->
</body>
</html>
