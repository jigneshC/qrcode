{{--
<!DOCTYPE html>
<html>
<head>
    <title>@yield('title','Home') | {{ config('app.name') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta content='text/html;charset=utf-8' http-equiv='content-type'>
    <meta content='Flat administration template for Twitter Bootstrap. Twitter Bootstrap 3 template with Ruby on Rails support.'
          name='description'>


    <!-- / END - page related stylesheets [optional] -->
    <!-- / bootstrap [required] -->
    <link href="{!! asset('assets/stylesheets/bootstrap/bootstrap.css') !!}" media="all" rel="stylesheet"
          type="text/css"/>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <!-- / theme file [required] -->

    <style>
        a:hover {
            text-decoration: none;
        }
    </style>

    @yield('headExtra')

    <!--[if lt IE 9]>
    <script src="{!! asset('assets/javascripts/ie/html5shiv.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('assets/javascripts/ie/respond.min.js') !!}" type="text/javascript"></script>
    <![endif]-->
</head>
<body class='contrast-red without-footer'>

<div id='wrapper'>
    <div id='main-nav-bg'></div>



    <section id='content'>
        <div class='container'>

            <div class='row' id='content-wrapper'>
                <div class='col-xs-12'>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-header">
                                <h1 class="pull-left">
                                    @yield('title','<i class="icon-tint"></i>
                                    <span>Home</span>')

                                </h1>

                            </div>
                        </div>
                    </div>

                    @yield('content')


                </div>
            </div>

        </div>
    </section>
</div>
<!-- / jquery [required] -->
<script src="{!! asset('assets/javascripts/jquery/jquery.min.js') !!}" type="text/javascript"></script>



@yield('footerExtra')
@stack('js')

<!-- / START - page related files and scripts [optional] -->

<!-- / END - page related files and scripts [optional] -->
</body>
</html>
--}}

{{-- 
<!DOCTYPE html>
<html lang="en">

  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title')</title>
  <link href="{{asset('assets/images/favicon.png')}}" rel="icon" type="image" />
  <link href="{{asset('assets/images/favicon.ico')}}" rel='icon' type='image/ico'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{asset('assets/css/jquery-ui.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
  <link rel="canonical" href="url('/dashboard')">
</head>

  
</head>

  <body class="planner-contributor active-sm-navigation">
    
<nav class="navigation" id="main-navigation">
		<div class="user">
			<a href="#" id="user-pic" title="View user settings">
				@if(\Auth::check() && Auth::user()->image != null)
					<img src="{{asset('/images/'.Auth::user()->image)}}" alt="">
				@else
					<img src="{{asset('assets/images/user-icon.png')}}" alt="">
			    @endif
			</a>
			<p class="visible-desktop"><a href="#" id="user-name" title="View user settings">
				@if(Auth::check())
				{{Auth::user()->name}}
				@endif</a></p>

			<ul class="settings visible-desktop" id="navigation-user-settings">
				<li>Settings
					<ul>
						<li><a href="personal-settings.html">Your Settings</a></li>
					</ul>
				</li>
				<li><a href="#"> <i class="fa fa-sign-out"></i>Logout</a></li>
			</ul>
		</div><!-- .user -->

		<ul class="menu">
			<div class="treeview">
				<ul>
					<li>
						<a href="{{url('/home')}}" class="active">Home</a>
					</li>
					@if(Auth::check())
					<li>
						<a href="#">My Profile</a>
                		<ul>
							<li><a href="{{url('/profile')}}">View Profile</a></li>
                			<li><a href="{{url('/profile/edit')}}">Edit Profile</a></li>
                			<li><a href="{{url('/profile/change-password')}}">Change Password</a></li>
						</ul>
					</li>
					<li>
						<a href="#">My Orders</a>
                		<ul>
                			<li><a href="{{url('/orders')}}">Orders List</a></li>
						</ul>
					</li>
					<li>
						<a href="#">My Qrcodes</a>
						<ul>
                			<li><a href="{{url('/generateqr')}}">Generate</a></li>
						</ul>
                		<ul>
                			<li><a href="{{url('/qrcode')}}">Qrcodes</a></li>
						</ul>
					</li>
                    <li>
                       <a href="{{url('/packages')}}">Purchase Packages</a>
						
					</li>
					<li>
					<a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

                 	<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    	{{ csrf_field() }}
					</form>
					</li>
					@else
						<li>
							<a href="{{url('/packages')}}">Purchase Packages</a>
						</li>
					@endif
					
				</ul>
			</div>
			
		</ul><!-- /.menu -->
	<!-- <img class="logo visible-desktop" src="/assets/images/SimpleChurchTools-logo.svg" alt=""> -->
</nav>
    <main class="page-content" aria-label="Content" id="main-content">
      <div class="wrapper_page">
        
          <header class="main-header">
	<div class="title-actions">
		<h1>@yield('title')</h1>
	</div><!-- .title-actions -->
	@include('partials.page_notification')
</header>
        @yield('content')
        
<!--      
<div class="card-main">
	
	<div class="input-search">
		<input class="search" type="search" placeholder="Search">
	</div>
	
	<table class="prayers-list">
		<tr class="active">
			<td class="checkbox">
				<input type="checkbox">
			</td>
			<td class="title">
				<a href="#">Ryan Hayden</a>
			</td>
			<td>
				<span>Willing to Do:</span>
				Speaker
				Soloist
			</td>
			<td>
				<span>Email:</span>
				pastorryanhayden@gmail.com
			</td>
			<td class="actions">
				<div class="desktop">
					<a href="#"><i class="fa fa-archive"></i></a>
					<a href="#"><i class="fa fa-pencil"></i></a>
				</div>
				<div class="mobile">
					<button class="button-orange">Archive</button>
					<button class="button-green">View</button>
				</div>
			</td>
		</tr>
	</table>
	<div class="pagination">
		<ul>
			<li><button> <i class="fa fa-chevron-left"></i> </button></li>
			<li><button class="active">1</button></li>
			<li><button>2</button></li>
			<li><button>3</button></li>
			<li><button>4</button></li>
			<li><button> <i class="fa fa-chevron-right"></i> </button></li>
		</ul>
		<div class="perPage">
			<label for="perPage">Show </label>
			<select name="" id="perPage">
				<option value="">5</option>
				<option value="">10</option>
				<option value="">20</option>
				<option value="">35</option>
			</select>
		</div>
	</div>
</div> <!-- /.card-main -->
	  </div>
    </main>
    --!>
<footer class="site-footer"></footer>
<!-- js -->
<script type="text/javascript" src="{{asset('assets/js/jquery-1.12.4.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/jquery-ui.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/treeview.js')}}"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<!--<script type="text/javascript" src="{{asset('assets/js/custom.js')}}"></script>-->
<!-- /js -->
<script>
		jQuery("#form_id").validate({
			
				rules: { 
					redirectlink: {
						 valid_link : true,
					},
			
				},
				messages: {
					redirectlink: {
						valid_link: "Please Enter Valid Redirect Link",
					},

			
				},
				submitHandler: function(form) {
					form.submit();
				}
			});

			jQuery.validator.addMethod(
		
		"valid_link",
		function(){
			var re = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
            var url = jQuery("#redirectlink").val();
            if (re.test(url))  
				return true;
			else
				return false;
		}
	);

	/*
	Valid Urls
	"http://www.example.com"
	"https://www.example.com"
	"www.example.com"
	*/

</script>


  </body>

</html>
 --}}


 <!DOCTYPE html>
 <html lang="en">
 <head>
	 <meta charset="utf-8">
	 <meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
	 <title>Dashboard - QRCode</title>
 
	 {{-- <link href="{{asset('frontend/assets/images/favicon.png')}}" rel="icon" type="image" />
	 <link href="{{asset('frontend/assets/images/favicon.ico')}}" rel="icon" type="image/ico"/> --}}
	 <link href="{{ asset('/favicon-qr.ico')}}" rel="icon" type="image"  />
	 
 
	 <!-- Global stylesheets -->
	 <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css"/>
	 <link href="{{asset('frontend/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css"/>
	 <link href="{{asset('frontend/assets/css/minified/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
	 <link href="{{asset('frontend/assets/css/minified/core.min.css')}}" rel="stylesheet" type="text/css"/>
	 <link href="{{asset('frontend/assets/css/minified/components.min.css')}}" rel="stylesheet" type="text/css"/>
	 <link href="{{asset('frontend/assets/css/minified/colors.min.css')}}" rel="stylesheet" type="text/css"/>
	 <link href="{{asset('frontend/assets/css/style.css')}}" rel="stylesheet" type="text/css"/>
	 <!-- /global stylesheets -->
 
	 <!-- Core JS files -->
	 <script type="text/javascript" src="{{asset('frontend/assets/js/plugins/loaders/pace.min.js')}}"></script>
	 <script type="text/javascript" src="{{asset('frontend/assets/js/core/libraries/jquery.min.js')}}"></script>
	 <script type="text/javascript" src="{{asset('frontend/assets/js/core/libraries/bootstrap.min.js')}}"></script>
	 <script type="text/javascript" src="{{asset('frontend/assets/js/plugins/loaders/blockui.min.js')}}"></script>
	 <!-- /core JS files -->
 
	 <!-- Theme JS files -->
	 <script type="text/javascript" src="{{asset('frontend/assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
	 <script type="text/javascript" src="{{asset('frontend/assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
	 <script type="text/javascript" src="{{asset('frontend/assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
	 <script type="text/javascript" src="{{asset('frontend/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
	 <script type="text/javascript" src="{{asset('frontend/assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	 <!--<script type="text/javascript" src="assets/js/plugins/ui/moment/moment.min.js"></script>
	 <script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>-->
 
	 <script type="text/javascript" src="{{asset('frontend/assets/js/core/app.js')}}"></script>
	 <!--<script type="text/javascript" src="assets/js/pages/dashboard.js"></script>-->
	 <!-- /theme JS files -->
 </head>
 
 <body>
 
	 <!-- Main navbar -->
	 <div class="navbar navbar-default header-highlight">
		 <div class="navbar-header">
			 <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('frontend/assets/images/logo-1.png')}}" alt="logo" title="QRcode"></a>
 
			 <ul class="nav navbar-nav visible-xs-block">
				 <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				 <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			 </ul>
		 </div>
 
		 <div class="navbar-collapse collapse" id="navbar-mobile">
			 <ul class="nav navbar-nav">
				 <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
				 </li>
			 </ul>
 
			 <ul class="nav navbar-nav navbar-right">
				 
			 
				 <li class="dropdown dropdown-user">
					 <a class="dropdown-toggle" data-toggle="dropdown">
						 <i class="fa fa-user"></i>
						 <span>@if(Auth::check()){{Auth::user()->name}}@endif</span>
						 <i class="caret"></i>
					 </a>
 
					 <ul class="dropdown-menu dropdown-menu-right">
						 <li><a href="{{url('/profile')}}"><i class="icon-user-plus"></i> My profile</a></li>
						 <li><a href="{{url('/packages')}}"><i class="fa fa-shopping-cart"></i>Purchase Packages</a></li>
						 
						 <li class="divider"></li>
						 
						 <li>
							<a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="icon-switch2"></i>Logout</a>
			
							<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
							</form>
						</li>
					 </ul>
				 </li>
			 </ul>
		 </div>
	 </div>
	 <!-- /main navbar -->
 
	 
	 <!-- Page container -->
	 <div class="page-container">
 
		 <!-- Page content -->
		 <div class="page-content">
 
			 <!-- Main sidebar -->
			 <div class="sidebar sidebar-main">
				 <div class="sidebar-content">
					 <div class="sidebar-user">
						 <div class="category-content">
							 <div class="media">
								 <div class="media-body">
									 <span class="media-heading text-semibold user-name text-center font-qrcode">
									@if(Auth::check())	 Welcome   {{ Auth::user()->name}} @endif</span>
								 </div>
							 </div>
						 </div>
					 </div>
 
					 <!-- Main navigation -->
					 <div class="sidebar-category sidebar-category-visible">
						 <div class="category-content no-padding">
							 <ul class="navigation navigation-main navigation-accordion">
 
								 <!-- Main -->
								 <!--<li class="navigation-header"><span>User Name</span> <i class="icon-menu" title="Main pages"></i></li>-->
								 <li class="active"><a href="{{url('/home')}}"><i class="icon-home4"></i><span>Home</span></a></li>
								 @if(Auth::check())
								 <li>
									 <a href=""><i class="fa fa-user"></i><span>My Profile</span></a>
									 <ul>
										 <li><a href="{{url('/profile/edit')}}"><i class="fa fa-edit sidebar-nav-icon"></i>Edit Profile</a></li>
										 <li><a href="{{url('/profile/change-password')}}"><i class="fa fa-lock sidebar-nav-icon"></i>Change Password</a></li>
									 </ul>
								 </li>
								 
								 <li>
									 <a><i class="fa fa-list sidebar-nav-icon"></i><span> My Orders</span></a>
									 <ul>
										 <li><a href="{{url('/orders')}}"><i class="fa fa-list-alt sidebar-nav-icon"></i>Orders List</a></li>
									 </ul>
								 </li>
								 <li>
									 <a href="#"><i class="fa fa-qrcode sidebar-nav-icon"></i><span>My Qrcodes</span></a>
									 <ul>
										 <li><a href="{{url('/generateqr')}}"><i class="fa fa-wrench sidebar-nav-icon"></i>Generate</a></li>
										 <li><a href="{{url('/qrcode')}}"><i class="fa fa-qrcode sidebar-nav-icon"></i>Qrcodes</a></li>
									 </ul>
								 </li>
								 @endif
								 <li>
									 <a href="{{url('/packages')}}"><i class="fa fa-shopping-cart sidebar-nav-icon"></i><span>Purchase Packages</span></a>
								 </li>
 
								 <!-- /main -->
							 </ul>
						 </div>
					 </div>
					 <!-- /main navigation -->
 
				 </div>
			 </div>
			 <!-- /main sidebar -->

			 	<!-- Main content -->
				 <div class="content-wrapper">
						
							<section class="top-section">
								<div class="page-header">
									<div class="page-header-content">
										<div class="row">
											<div class="col-lg-4 col-md-4 col-sm-12 clearfix">
												<div class="page-title">
													<h4><span class="text-semibold font-head">
														@if(Auth::check())
														<a href="{{url('/dashboard')}}">Dashboard</a> @if(Route::currentRouteName() != null ) / @endif
														@endif 
														@yield('title')
													</h4>
												</div>
											</div>
											<div class="col-lg-8 col-md-8 col-sm-12 clearfix">
												
											</div>
										</div>
									</div>			
								</div><!-- /page header -->
			
							</section>
		
		
						<!-- Content area -->
						<div class="content">

			 @include('partials.page_notification')

			 @yield('content')

			   <!-- Footer -->
			   <div class="footer text-muted">
					&copy; <a href="#" target="_blank"> QR Code Generator 2018 </a>
				</div>
				<!-- /footer -->

			</div>
			<!-- /content area -->

		</div>
		<!-- /main content -->
 

		 <!-- /page content -->
	 </div>
 
	 </div>
	 
	 <!-- /page container -->

	 
 <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
 
 <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

 @stack('js')
 <script>
	jQuery(".qr_form").validate({
		
			rules: { 
				redirectlink: {
					 valid_link : true,
				},
		
			},
			messages: {
				redirectlink: {
					valid_link: "Please Enter Valid Redirect Link",
				},

		
			},
			submitHandler: function(form) {
				form.submit();
			}
		});

		jQuery.validator.addMethod(
	
	"valid_link",
	function(){
		var re = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
		var url = jQuery("#redirectlink").val();
	
		if(url == null || url == ''){
			return true;
		}else{
			if (re.test(url)) 
				return true;
			else
				 return false;
			
		}
		
	}
);

/*
Valid Urls


"http://www.example.com"
"https://www.example.com"
"www.example.com"
*/

</script>

 </body>
 </html>
 