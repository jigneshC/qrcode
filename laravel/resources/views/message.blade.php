@extends('layouts.main')

@section('title','Messages - QR Code')

@section('content')

<header>
    <div class="header-div inside-header-div clearfix">
    <div class="header-top-div">
    <div class="container">
        <div class="clearfix">
        <div class="logo-div">
            <a href="{{url('/home')}}"><img src="{{asset('assets/images/logo.png')}}" alt=""></a>
        </div>
        <div class="top-link-div">
            @if(Auth::guest())
                <a href="{{route('register')}}" class="signup-link">@lang('home.signup')</a>
                <a href="{{route('login')}}" class="login-link">@lang('home.login')</a>
            @else
                @if(Auth::user()->type == 'user')
                <a href="{{url('/dashboard')}}" class="login-link">@lang('home.welcome') {{Auth::user()->name}}</a>
                <a href="{{ url('/logout') }}" class="login-link"
                onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                 <i class='icon-signout'></i>
                 @lang('home.logout')
             </a>

             <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                   style="display: none;">
                 {{ csrf_field() }}
             </form>
                @else
                <a href="{{url('/admin')}}" class="login-link">@lang('home.welcome') {{Auth::user()->name}}</a>
                <a href="{{ url('/logout') }}" class="login-link"
                onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                 <i class='icon-signout'></i>
                 @lang('home.logout')
             </a>
                @endif
            @endif
            <a href="#" class="dropdown-toggle signup-link" data-toggle="dropdown">
                <i class="fa fa-globe space-5"></i><span class="space-5-1">@lang('home.language')</span>
            </a>
            <div class="dropdown-menu dropdown-menu-large small-dropdown row animated fadeInUp">
                <li class="col-sm-12">
                    <h4 class="dropdown_header">@lang('home.language')</h4>
                    <ul class="categories-links">
                         @foreach($_lang as $lang)
                        <li>
                            <a href="{{request()->fullUrlWithQuery(['lang'=> $lang->lang_code])}}">
                                    @if($lang->lang_code == 'en')
                                    <img src="{{asset('assets/images/en.png')}}"  />
                                    @elseif($lang->lang_code == 'fr')
                                    <img src="{{asset('assets/images/fr.png')}}"  />
                                    @else
                                    @endif
                                {{$lang->name}} </a>
                        </li>
                        @endforeach


                    </ul>
                </li>
            </div>
        </div>
        </div>
     </div> 
     </div><!-- end of header-top-div -->  
     
     <!--div class="StripeBackground">
        <div class="stripe s0"></div>
        <div class="stripe s2 pattern"></div>
      </div>
      <div class="StripeBackground accelerated">
<div class="stripe s1"></div>
<div class="stripe s3"></div>
<div class="stripe s4"></div>
</div-->
      </div><!-- end of header-div -->
</header>

<div class="content-area middle-content-area clearfix">
    <h2 class="blk-title">@lang('home.messages')</span></h2>
        <div class="container">
        	<div class="row">
                @include('partials.page_notification')
       		</div>
        </div>
    </div>


@endsection