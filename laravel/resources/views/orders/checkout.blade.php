@extends('layouts.main')

@section('title',__('order.checkout_qrcode'))

@section('content')


<header>
        <div class="header-div inside-header-div clearfix">
        <div class="header-top-div">
        <div class="container">
            <div class="clearfix">
            <div class="logo-div">
                <a href="{{url('/home')}}"><img src="{{asset('assets/images/logo.png')}}" alt=""></a>
            </div>
            <div class="top-link-div">
                @if(Auth::guest())
                    <a href="{{route('register')}}" class="signup-link">@lang('home.signup')</a>
                    <a href="{{route('login')}}" class="login-link">@lang('home.login')</a>
                @else
                    @if(Auth::user()->type == 'user')
                    <a href="{{url('/dashboard')}}" class="login-link">@lang('home.welcome') {{Auth::user()->name}}</a>
                    <a href="{{ url('/logout') }}" class="login-link"
                    onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">
                     <i class='icon-signout'></i>
                     @lang('home.logout')
                 </a>
    
                 <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                       style="display: none;">
                     {{ csrf_field() }}
                 </form>
                    @else
                    <a href="{{url('/admin')}}" class="login-link">@lang('home.welcome') {{Auth::user()->name}}</a>
                    <a href="{{ url('/logout') }}" class="login-link"
                    onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">
                     <i class='icon-signout'></i>
                     @lang('home.logout')
                 </a>
                    @endif
                @endif
                <a href="#" class="dropdown-toggle signup-link" data-toggle="dropdown">
                    <i class="fa fa-globe space-5"></i><span class="space-5-1">@lang('home.language')</span>
                </a>
                <div class="dropdown-menu dropdown-menu-large small-dropdown row animated fadeInUp">
                    <li class="col-sm-12">
                        <h4 class="dropdown_header">@lang('home.language')</h4>
                        <ul class="categories-links">
                             @foreach($_lang as $lang)
                            <li>
                                <a href="{{request()->fullUrlWithQuery(['lang'=> $lang->lang_code])}}">
                                        @if($lang->lang_code == 'en')
                                        <img src="{{asset('assets/images/en.png')}}"  />
                                        @elseif($lang->lang_code == 'fr')
                                        <img src="{{asset('assets/images/fr.png')}}"  />
                                        @else
                                        @endif
                                    {{$lang->name}} </a>
                            </li>
                            @endforeach


                        </ul>
                    </li>
                </div>
            </div>
            </div>
         </div> 
         </div><!-- end of header-top-div -->  
        </div><!-- end of header-div -->
    </header>


<div class="content-area middle-content-area clearfix">
     <div class="checkout-page-container clearfix">
            
                <h2 class="blk-title">@lang('order.checkout')</span></h2>
                
                
                <div class="container">
                <div class="row">
                
                    <div class="checkout-page-blk-div clearfix">
                    
                    <div class="col-md-6 col-sm-6">
                    <div class="row1">
                        
                        <h4>@lang('order.billing_details')</h4>

                        @if($package->type == "free")
                            {!! Form::open(['url' => '/freepackage','class' => 'form-horizontal','id'=>'form_id','files' => true]) !!}
                        @else
                            {{-- {!! Form::open(['url' => '/paypal/payment/', 'class' => 'form-horizontal', 'id'=> 'form_id'  , 'files' => true]) !!} --}}
                            {!! Form::open(['url' => '/orders/checkout', 'class' => 'form-horizontal', 'id'=> 'form_id'  , 'files' => true]) !!}
                        @endif
                        {{-- <input type="hidden" name="order_id" value="{{ $order->id }}"> --}}
                        <input type="hidden" name="package_id" value="{{ $package->id }}">

                        <div class="billding-form-div clearfix">
                            <div class="form-group clearfix">

                                <div class="col-md-6 col-sm-6">
                                        {!! Form::label('first_name', trans('order.first_name') ) !!}
                                        {!! Form::text('first_name',  (isset($user->name)) ? $user->name : null, ['class' => 'form-control']) !!}
                                        {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
                                </div><!-- end of col -->

                                <div class="col-md-6 col-sm-6">
                                        {!! Form::label('last_name', trans('order.last_name')) !!}
                                        {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
                                </div><!-- end of col -->

                            </div><!-- end of form group -->


                            <div class="form-group clearfix">

                                <div class="col-md-12 col-sm-12">
                                    {!! Form::label('email', trans('order.email')) !!}
                                    {!! Form::text('email', (isset($user->email)) ? $user->email : null, ['class' => 'form-control','id'=>'email']) !!}
                                </div><!-- end of col -->

                            </div><!-- end of form group -->

                            <div class="form-group clearfix">

                                <div class="col-md-12 col-sm-12">
                                        {!! Form::label('address',trans('order.address')) !!}
                                        {!! Form::text('address', null, ['class' => 'form-control']) !!}

                                </div><!-- end of col -->

                            </div><!-- end of form group -->


                            <div class="form-group clearfix">

                                <div class="col-md-12 col-sm-12">
                                        {!! Form::label('phone', trans('order.phone')) !!}
                                        {!! Form::text('phone', (isset($user->phone)) ? $user->phone : null, ['class' => 'form-control', 'id' => 'phone']) !!}
                                </div><!-- end of col -->

                            </div><!-- end of form group -->


                            
                        </div><!-- end of billding-form-div -->
                    
                    
                    </div><!-- end of row -->
                    </div><!-- end of col -->
                    
                    <div class="col-md-6 col-sm-6">
                    <div class="row1">
                    
                        <h4 class="mmt20">@lang('order.your_order')</h4>
                        <div class="checkout-page-tbl clearfix">
                        <div class="table-responsive">
                            <table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <th>@lang('order.product')</th>
                                <th align="right" class="text-right">@lang('order.total')</th>
                            </tr>
                            <tr>
                                <td>@lang('order.product_name')<span class="txt"></span></td>
                                <td align="right"><span class="total-price">{{$package->title}}</span></td>
                            </tr>
                            <tr>
                                    <td>@lang('order.points_you_can_earn')<span class="txt"></span></td>
                                    <td align="right"><span class="total-price">{{$package->qr_code_no}}</span></td>
                            </tr>

                             <tr>
                             <tr>
                                <td><span class="grand-total">@lang('order.grand_total')</span></td>
                                <td align="right"><span class="total-price">{{$rate['amount']}} {{$rate['toCode']}} </span></td>
                            </tr>
                            </table>
    
                        
                        </div>
                        </div><!-- end nof checkout-page-tbl -->
                    
                    </div><!-- end of row -->
                    </div><!-- end of col -->
                        
                    
                    </div><!-- end nof checkout-page-blk-div -->
                    
                    <div class="btn-order-div clearfix">
                            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('order.place_order'), ['class' => 'btn-order']) !!}

                    </div>
                    {!! Form::close() !!}
                
                </div><!-- end of row -->
                </div><!-- end of container -->              
            </div><!-- end of checkout-page-container -->   
        </div><!-- end of content-area -->



@endsection

@push('js')
<script>

$("#form_id").validate({

    rules: { 
        first_name: {
             required: true,
             valid_name : true,
        },
        email: {
            required: true,
            valid_email : true,
        },
        phone: {
            required: true,
            valid_phone : true,
        },

    },
    messages: {
        first_name: {
            required: "First Name is required",
            valid_name: "Please Enter Valid Name",
        },
        email: {
            required: "Email is required",
            valid_email: "Please Enter Valid Email Address",
        },
        phone: {
            required: "Phone number is required",
            valid_phone: "Please Enter Valid Phone Number",
        },

    },
    submitHandler: function(form) {
        form.submit();
    }
});
$.validator.addMethod(
    "valid_email", 
    function() {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var email = $('#email').val();
        if(reg.test(email) == true)
            return true;
        else
            return false;   
        
    }
);
$.validator.addMethod(

    "valid_phone",
    function(){
        var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        var phone = $('#phone').val();
        if((phone.match(phoneno)))
            return true;
        else
            return false;
    }
);
$.validator.addMethod(
    
    "valid_name",
    function(){
        var alphaExp = /^[a-zA-Z]+$/;
        var first_name = $('#first_name').val();
        if((first_name.match(alphaExp)))
            return true;
        else
            return false;
    }
);


</script>
@endpush


