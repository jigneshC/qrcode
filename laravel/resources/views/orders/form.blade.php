<div class="form-group">
        <div class="col-md-offset-4 col-md-4">
            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('order.buy_now'), ['class' => 'btn btn-primary']) !!}
        </div>
</div>