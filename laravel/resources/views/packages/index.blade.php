@extends('layouts.main')

@section('title',__('package.plan_and_pricing_qrcode'))

@section('content')

<header>
    <div class="header-div inside-header-div clearfix">
    <div class="header-top-div">
    <div class="container">
        <div class="clearfix">
        <div class="logo-div">
            <a href="{{url('/home')}}"><img src="{{asset('assets/images/logo.png')}}" alt=""></a>
        </div>
        <div class="top-link-div">
            @if(Auth::guest())
                <a href="{{route('register')}}" class="signup-link">@lang('home.signup')</a>
                <a href="{{route('login')}}" class="login-link">@lang('home.login')</a>
            @else
                @if(Auth::user()->type == 'user')
                <a href="{{url('/dashboard')}}" class="login-link">@lang('home.welcome') {{Auth::user()->name}}</a>
                <a href="{{ url('/logout') }}" class="login-link"
                onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                 <i class='icon-signout'></i>
                 @lang('home.logout')
             </a>

             <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                   style="display: none;">
                 {{ csrf_field() }}
             </form>
                @else
                <a href="{{url('/admin')}}" class="login-link">@lang('home.welcome') {{Auth::user()->name}}</a>
                @endif
            @endif
            <a href="#" class="dropdown-toggle signup-link" data-toggle="dropdown">
                <i class="fa fa-globe space-5"></i><span class="space-5-1">@lang('home.language')</span>
                
            </a>
            <div class="dropdown-menu dropdown-menu-large small-dropdown row animated fadeInUp">
                <li class="col-sm-12">
                    <h4 class="dropdown_header">@lang('home.language')</h4>
                    <ul class="categories-links">
                         @foreach($_lang as $lang)
                        <li>
                            <a href="{{request()->fullUrlWithQuery(['lang'=> $lang->lang_code])}}">
                                @if($lang->lang_code == 'en')
                                <img src="{{asset('assets/images/en.png')}}" class="img-padd10"  />
                                @elseif($lang->lang_code == 'fr')
                                <img src="{{asset('assets/images/fr.png')}}" class="img-padd10" />
                                @else
                                @endif
                                {{$lang->name}} </a>
                        </li>
                        @endforeach


                    </ul>
                </li>
            </div>
        </div>
        </div>
     </div> 
     </div><!-- end of header-top-div -->  
     
     <!--div class="StripeBackground">
        <div class="stripe s0"></div>
        <div class="stripe s2 pattern"></div>
      </div>
      <div class="StripeBackground accelerated">
<div class="stripe s1"></div>
<div class="stripe s3"></div>
<div class="stripe s4"></div>
</div-->
      </div><!-- end of header-div -->
</header>

@include("modal.freeqr")

<div class="content-area middle-content-area clearfix">

    <div class="plan-pricing-container clearfix">
    
        <h2 class="blk-title">@lang('package.content.text_line_1')<span>@lang('package.content.text_line_2')</span></h2>

        <div class="plan-pricing-blk-div clearfix">

                @include("modal.freeqr")
        
        <div class="container">
            <div class="row">
                
                @foreach($packages as $item)
                <div class="col-md-4 col-sm-4">
                    <div class="plan-pricing-row-blk clearfix">
                        <div class="top-block clearfix">
                            <h2>{{$item['title']}}</h2>
                            <h3></h3>
                        </div>
                        <div class="btm-block clearfix">
                            <h4>
                                @if($item['type'] == "Pricing")
                                    {{$item['amount']}} {{$item['currency']}}
                                @elseif($item['type'] == "Free")
                                    {{$item['type']}}
                                @endif
                            </h4>
                            <h5>{!!$item['description']!!}</h5>
                            
                            <div class="block-txt clearfix">
                                <dd class="clearfix"><span class="txt-1">{{$item['qr_code_no']}}</span>
                                    <span class="txt-2">@lang('package.total_points')</span>
                                </dd>
                                <dd class="clearfix"><dd class="clearfix"><span class="txt-1">Unlimited</span>
                                    <span class="txt-2">@lang('package.number_of_account_users')</span></dd>
                                <dd class="clearfix">
                                    {{--
                                    <span class="txt-1">
                                    @if($item['type'] == "Free")
                                        24 hrs.
                                    @else
                                        @if($item['expiry_year'] == 0 && $item['expiry_months'] == 0)
                                            No Validity
                                        @elseif($item['expiry_year'] == 0)
                                            {{ $item['expiry_months'] }} months
                                        @elseif($item['expiry_months'] == 0)
                                            {{ $item['expiry_year'] }} 
                                            @if($item['expiry_year'] <= 1) 
                                                Year 
                                            @else 
                                                Years 
                                            @endif 
                                        @else
                                            {{$item['expiry_year']}} 
                                            @if($item['expiry_year'] <= 1) 
                                                Year 
                                            @else 
                                                Years 
                                            @endif  
                                            & {{$item['expiry_months']}} months
                                        @endif 
                                    @endif


                                </span><span class="txt-2">@lang('package.qr_code_validity')</span>
                                --}}
                                <dd class="clearfix"><span class="txt-2"></span></dd>
                            </dd>
                            </div><!-- end block-txt -->
      
                        </div><!-- end of btm bblock -->
                        
                        <div class="btm-buy-now clearfix">
                            @if($item['type']  == 'Free')
                                <a href='#' class='open_model_for_freeqr free-link' data-packageid='{{$item['id']}}'>@lang('package.buy_now')</a>
                            @else
                            <a href="{{ url('/orders/create/'.$item['id']) }}">@lang('package.buy_now')</a>
                                {{-- {!! Form::open(['url' => '/orders/create/1', 'class' => 'form', 'files' => true]) !!}
                                <input type="hidden" name="package_id" value="{{ $item['id'] }}">
                                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('package.buy_now'),['class' => 'btn btn-default']) !!}
                                {!! Form::close() !!} --}}
                            @endif
                               
                            
                        </div>
                     </div><!-- end nof plan-pricing-row-blk -->
                
                </div><!-- end of col -->
                @endforeach
                
            </div>
        </div>  


        </div><!-- end nof plan-pricing-blk-div -->
        
    </div><!-- end of plan-pricing-container -->
   
    
    
     <div class="two-block-div clearfix">
        
        <div class="container">
        <div class="row">
        
            <div class="col-md-6 col-sm-6">
                <div class="two-block-box clearfix">
                    <h2>@lang('package.content.section_competition.heading')</h2>
                    <ul class="chkmark-ul-white">
                        <?php $li_content = Lang::get('package.content.section_competition.li'); ?>
                        @foreach( $li_content as $li_contents)
                        <li>{{$li_contents}}</li>
                        @endforeach

                    </ul>
                    <div class="white-btn-link-div clearfix">
                        <a href="#" class='open_model_for_freeqr' data-packageid='{{$free->id}}' >@lang('package.create_a_qr_code_for_free')</a>
                    </div>
                    
                </div>
            
            </div><!-- end of col -->
            
            <div class="col-md-6 col-sm-6">
                <div class="two-block-box-2 clearfix">
                    <h2>@lang('package.content.section_question.heading')</h2>
                    <p>@lang('package.content.section_question.p')</p>
                    <ol class="white-ol">
                            <?php $li_content = Lang::get('package.content.section_question.li'); ?>
                            @foreach( $li_content as $li_contents)
                            <li>{{$li_contents}}</li>
                            @endforeach

                    </ol>
                    
                    <div class="white-btn-link-div clearfix">
                        <a href="#" class='open_model_for_freeqr' data-packageid='{{$free->id}}'>@lang('package.create_a_qr_code_for_free') </a>
                    </div>

                </div>
            
            </div><!-- end of col -->
        
            </div><!-- end of row -->
            </div><!-- end of container -->
        
        </div><!-- end of two-block-div -->
   
    
    
</div><!-- end of content-area -->



@endsection
