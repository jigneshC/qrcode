<li class='dropdown medium only-icon widget'>
    <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
        <i class='icon-rss'></i>
        <div class='label'>5</div>
    </a>
    <ul class='dropdown-menu'>
        <li>
            <a href='#'>
                <div class='widget-body'>
                    <div class='pull-left icon'>
                        <i class='icon-user text-success'></i>
                    </div>
                    <div class='pull-left text'>
                        John Doe signed up
                        <small class='text-muted'>just now</small>
                    </div>
                </div>
            </a>
        </li>
        <li class='divider'></li>
        <li>
            <a href='#'>
                <div class='widget-body'>
                    <div class='pull-left icon'>
                        <i class='icon-inbox text-error'></i>
                    </div>
                    <div class='pull-left text'>
                        New Order #002
                        <small class='text-muted'>3 minutes ago</small>
                    </div>
                </div>
            </a>
        </li>
        <li class='divider'></li>
        <li>
            <a href='#'>
                <div class='widget-body'>
                    <div class='pull-left icon'>
                        <i class='icon-comment text-warning'></i>
                    </div>
                    <div class='pull-left text'>
                        America Leannon commented Flatty with veeery long text.
                        <small class='text-muted'>1 hour ago</small>
                    </div>
                </div>
            </a>
        </li>
        <li class='divider'></li>
        <li>
            <a href='#'>
                <div class='widget-body'>
                    <div class='pull-left icon'>
                        <i class='icon-user text-success'></i>
                    </div>
                    <div class='pull-left text'>
                        Jane Doe signed up
                        <small class='text-muted'>last week</small>
                    </div>
                </div>
            </a>
        </li>
        <li class='divider'></li>
        <li>
            <a href='#'>
                <div class='widget-body'>
                    <div class='pull-left icon'>
                        <i class='icon-inbox text-error'></i>
                    </div>
                    <div class='pull-left text'>
                        New Order #001
                        <small class='text-muted'>1 year ago</small>
                    </div>
                </div>
            </a>
        </li>
        <li class='widget-footer'>
            <a href='#'>All notifications</a>
        </li>
    </ul>
</li>