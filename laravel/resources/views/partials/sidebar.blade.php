<nav id='main-nav'>
    <div class='navigation'>
        <div class='search'>
            <form action='search_results.html' method='get'>
                <div class='search-wrapper'>
                    <input value="" class="search-query form-control" placeholder="Search..." autocomplete="off"
                           name="q" type="text"/>
                    <button class='btn btn-link icon-search' name='button' type='submit'></button>
                </div>
            </form>
        </div>
        <ul class='nav nav-stacked'>


            <?php
            $active = function ($part) {
                return str_contains(Request::url(), $part);
            };
            ?>
            <?php //echo '<pre>';print_r($laravelAdminMenus->menus);exit;?>
            @foreach($laravelAdminMenus->menus as $section)
           
                @if($section->items)

                    {{--@if($section->section == 'Modules')--}}
                    @foreach($section->items as $menu)
                    @if(\Auth::check())
                        @if(isset($menu->permission) && Auth::user()->can($menu->permission) )

                            <li class="{{ ($active($menu->url) ? 'active' : '') }}">
                                <a href='{{ url($menu->url) }}'>
                                    @if(isset($menu->icon) && $menu->icon != '')
                                        <i class='{{$menu->icon}} fa-fw fa-2x text-primary'></i>
                                    @else
                                        <i class='fa fa-circle fa-fw fa-2x'></i>
                                    @endif
                                    <span style="font-size: 20px">{{ $menu->title }}</span>
                                </a>
                            </li>

                        @endif
                    @endif
                    @endforeach
                    {{--@else--}}
                    {{--<li class=''>--}}
                    {{--<a class="dropdown-collapse" href="#"><i class='icon-circle-blank'></i>--}}
                    {{--<span> {{ $section->section }}</span>--}}
                    {{--<i class='icon-angle-down angle-down'></i>--}}
                    {{--</a>--}}
                    {{--<ul class='nav nav-stacked'>--}}


                    {{--@foreach($section->items as $menu)--}}

                    {{--@if(isset($menu->permission) && Auth::user()->can($menu->permission))--}}

                    {{--<li class="{{ ($active($menu->url) ? 'active' : '') }}">--}}

                    {{--<a href='{{ url($menu->url) }}'>--}}
                    {{--<i class='icon-circle-blank'></i>--}}
                    {{--<span>{{ $menu->title }}</span>--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--@endif--}}
                    {{--@endforeach--}}

                    {{--</ul>--}}
                    {{--</li>--}}
                    {{--@endif--}}
                @endif
            @endforeach

            {{--@foreach($laravelAdminMenus->menus as $section)--}}
            {{--@if($section->items)--}}
            {{--<li class=''>--}}
            {{--<a class="dropdown-collapse" href="#"><i class='icon-circle-blank'></i>--}}
            {{--<span> {{ $section->section }}</span>--}}
            {{--<i class='icon-angle-down angle-down'></i>--}}
            {{--</a>--}}
            {{--<ul class='nav nav-stacked'>--}}
            {{--@foreach($section->items as $menu)--}}
            {{--<li class=''>--}}
            {{--<a href='{{ url($menu->url) }}'>--}}
            {{--<i class='icon-circle-blank'></i>--}}
            {{--<span>{{ $menu->title }}</span>--}}
            {{--</a>--}}
            {{--</li>--}}
            {{--@endforeach--}}

            {{--</ul>--}}
            {{--</li>--}}
            {{--@endif--}}
            {{--@endforeach--}}


        </ul>
    </div>
</nav>