<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', trans('qrcode.name'), ['class' => 'col-md-4 control-label']) !!}
    
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        <p class=""></p>
    
</div>

<div class="form-group {{ $errors->has('redirectlink') ? 'has-error' : ''}}">
    {!! Form::label('redirectlink',trans('qrcode.redirect_link'), ['class' => 'col-md-4 control-label']) !!}

        {!! Form::text('redirectlink', null, ['class' => 'form-control','id' => 'redirectlink']) !!}
        {!! $errors->first('redirectlink', '<p class="help-block">:message</p> ') !!}
        <p class="redirect"></p>

</div>

@if(isset($action) && $action=="edit")
<div class="form-group">
    {!! Form::label('Hash Code', trans('qrcode.hash_code'), ['class' => 'col-md-4 control-label']) !!}
  
        {!! Form::text('hash', null, ['class' => 'form-control','disabled'=>'disabled']) !!}
        <p class=""></p>
    
</div>
@endif

<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status',trans('qrcode.status'), ['class' => 'col-md-4 control-label']) !!}
   
        {!! Form::select('status',['1' => 'Active','0'=>'Inactive'], null, ['class' => 'form-control']) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
        <p class=""></p>
    
</div>


<div class="text-right">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('qrcode.create'), ['class' => 'btn btn-1 pull-left']) !!}
</div>



