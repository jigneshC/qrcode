@extends('layouts.frontend')

@section('title',__('qrcode.generate_qrcode'))

@section('content')

<!-- Form horizontal -->
<div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">@lang('qrcode.generate_qrcode')</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            
            <div class="col-md-12 clearfix">
                {{-- <div class="col-md-3">Total Points Earned : {{$points_earned}} </div>
                <div class="col-md-3">Points Remaining : {{Auth::user()->points}}</div>
                <div class="col-md-3">Total Points Used : {{$points_used}}</div> --}}
            </div>
            
            <div class="col-md-6 col-sm-12 clearfix">
                    <form method="post" action="{{url('/generateqr')}}"  class="qr_form">
                        {{ csrf_field() }}
                
                        <div class="form-group">
                            {!! Form::label('name', trans('qrcode.name').' :', ['class' => 'col-md-4 control-label']) !!}
                            
                                {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                <p class=""></p>
                           
                        </div>
                        <div class="form-group">
                            {!! Form::label('redirectlink', trans('qrcode.redirect_link').' :', ['class' => 'col-md-4 control-label']) !!}
                            
                                {!! Form::text('redirectlink', null, ['class' => 'form-control','id' => 'redirectlink']) !!}
                                <p class="redirect"></p>
                            
                        </div>
                
                        <div class="form-group">
                            {!! Form::label('status', trans('qrcode.status').' :',['class' => 'col-md-4 control-label']) !!}
                           
                                {!! Form::select('status',['1' => 'Active','0'=>'Inactive'], null, ['class' => 'form-control']) !!}
                                <p class=""></p>
                            
                        </div>
                
                                
                            <div class="form-group">
                                {!! Form::label('duration', trans('qrcode.duration') .' :', ['class' => 'col-md-4 control-label']) !!}
                                
                                    <select name="setting" class="form-control">
                                        @foreach($settings as $setting)
                                            <option value="{{$setting->duration}}">{{$setting->duration}} - points ({{$setting->points}})</option>
                                        @endforeach
                                    </select>
                                    
                                
                            </div>
                            
                
                            <div class="text-right">
                                    
                            {!! Form::submit( trans('qrcode.generate'),['class' => 'btn btn-1 pull-left']) !!}
                            
                            </div>
                
                       </form>
            </div>
           
        </div>
</div>
        

@endsection
