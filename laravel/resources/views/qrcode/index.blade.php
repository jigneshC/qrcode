@extends('layouts.frontend') 

@section('title',__('qrcode.qrcode'))
 
@section('content') 

 <!-- order-list -->
 <div class="panel panel-flat">
        
            <div class="panel-heading">
                <h5 class="panel-title order">@lang('qrcode.qrcode_list')</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body-1 clearfix">
                <div class="col-lg-12 pull-right padding-10">
                    <div class="input-group">
                        {!! Form::open(['method' => 'GET', 'url' => '/qrcode', 'class' => 'navbar-form navbar-right','role' => 'search']) !!}
                            <div class="input-group" style="max-width: 300px">
                                <input type="text" class="form-control searchbtn" name="search" placeholder=@lang('qrcode.search')>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        {!! Form::close() !!}
                       
                </div>
            </div>
        </div>


        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr class="bg-blue">
                        <th>@lang('qrcode.id')</th>
                        <th>@lang('qrcode.name')</th>
                        <th>@lang('qrcode.hash_code')</th>
                        <th>@lang('qrcode.redirect_link')</th>
                        <th>@lang('qrcode.status')</th>
                        <th class="text-center">@lang('qrcode.actions')</th>
                    </tr>
                </thead>
                <tbody>
                        @if(count($qrcode) == 0)
                        <tr><td colspan=5>No Qr Codes Found</td></tr>
                        @endif
                    @foreach($qrcode as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td><a target="_blank" href="{{ url('/'.$item->hash) }}">{{ $item->name }}</a></td>
                        <td>{{ $item->hash }}</td>
                        <td>{{ $item->redirectlink }}</td>
                        <td>@if( $item->status == 1 ) @lang('qrcode.active') @else @lang('qrcode.inactive') @endif</td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="{{ url('/qrcode/' . $item->id) }}" title=@lang('qrcode.view')  class="btn btn-blue"><i class="fa fa-eye"></i>@lang('qrcode.view')</a>    
                                </li>
                                <li class="dropdown">
                                    {!! Form::open([ 'method'=>'DELETE',
                                    'url' => ['/qrcode', $item->id], 'style' => 'display:inline' ]) !!} 
                                    {!! Form::button('<i class="fa fa-trash" ></i> Delete', array( 'type' => 'submit', 'class' => 'btn btn-blue', 'title' => 'Delete Qrcode', 'onclick'=>'return confirm("Confirm delete?")' )) !!} 
                                    {!!Form::close() !!} 
                                </li>
                                
                                <li class="dropdown">
                                    @if($item->expiry_date < $today_date )
                                        <a class="btn btn-red">@lang('qrcode.expired')</a>
                                    @endif
                                    @if($item->expiry_date >= $today_date )
                                        <a href="{{ url('/qrcode/' . $item->id . '/edit') }}" title=@lang('qrcode.edit')  class="btn btn-blue"><i class="fa fa-edit"></i>@lang('qrcode.edit')</a>   
                                    @endif 
                                </li>
                               
                            </ul>
                           

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
        <div class="panel-body-2 clearfix">
            <div class="col-lg-12 pull-right padding-10">
                <div class="col-lg-12 bg-white-custom-p">
                    <div class="pagination-div">
                       {!! $qrcode->appends(['search' => Request::get('search')])->render() !!} 
                    </div>

                </div>

            </div>
        </div>


</div>
    <!-- panel -->


@endsection