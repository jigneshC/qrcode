<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});



Route::group(['prefix' => 'v1.0',  'namespace' => 'Api'], function () {

   // Route::get('/get-qrcodes', 'QrcodeController@getQrcodes');
//    Route::get('/get-qrcode/{qrcode_id}', 'QrcodeController@getQrcode');

    Route::post('/create-qrcode', 'QrcodeController@postQrcode');
    Route::put('/edit-qrcode/{qrcode_id}', 'QrcodeController@putQrcode');

    Route::delete('/delete-qrcode/{qrcode_id}', 'QrcodeController@deleteQrcode');

    Route::post('/create-user', 'UsersController@register');

    Route::post('/generate-qr','UsersController@generateQrcode');

    Route::post('/get-qrcode','UsersController@getQrcode');

});

