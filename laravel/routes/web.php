<?php

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/', 'HomeController@index');

//Packages
Route::resource('/packages', 'PackagesController');

//Orders
Route::get('/orders/create/{id}','OrdersController@create');
Route::post('/orders/checkout','OrdersController@checkout');
Route::resource('/orders', 'OrdersController');
Route::post('/freepackage', 'OrdersController@freepackage');




//Message
Route::get('/message', function(){
    return view('message');
});


Route::group(['middleware' => ['auth']], function () {

    //My Profile
    Route::get('/profile', 'ProfileController@index')->name('profile.index');
    Route::get('/profile/edit', 'ProfileController@edit')->name('profile.edit');
    Route::patch('/profile/edit', 'ProfileController@update');
    Route::get('/dashboard', 'ProfileController@dashboard');

    // Change Password
    Route::get('/profile/change-password', 'ProfileController@changePassword')->name('profile.password');
    Route::patch('/profile/change-password', 'ProfileController@updatePassword');
    
    // QR CODE
    Route::resource('qrcode', 'QrcodeController');
    Route::get('generateqr', 'QrcodeController@generateqr');
    Route::post('generateqr', 'QrcodeController@generateqr');

});

Route::group(['middleware' => ['auth', 'admin']], function () {

    Route::group(['prefix' => 'admin'], function () {

        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/', 'Admin\AdminController@index');

        //Roles & Permissions
        Route::get('/give-role-permissions', 'Admin\AdminController@getGiveRolePermissions');
        Route::post('/give-role-permissions', 'Admin\AdminController@postGiveRolePermissions');
        Route::resource('/roles', 'Admin\RolesController');
        Route::resource('/permissions', 'Admin\PermissionsController');

        //Users
        Route::resource('/users', 'Admin\UsersController');
        Route::get('/users-data/', 'Admin\UsersController@datatable');

        //Packages
        Route::resource('/packages', 'Admin\PackagesController');
        Route::get('/packages-data/', 'Admin\PackagesController@datatable');
        
        // Orders
        Route::get('/orders/create/{id}','Admin\OrdersController@create');
        Route::post('/orders/checkout','Admin\OrdersController@checkout');
        Route::resource('/orders', 'Admin\OrdersController');
        Route::get('/orders-data/', 'Admin\OrdersController@datatable');
        
        //Generator
        Route::get('/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
        Route::post('/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

        // Admin profile
        Route::get('/profile', 'Admin\ProfileController@index')->name('profile.index');
        Route::get('/profile/edit', 'Admin\ProfileController@edit')->name('profile.edit');
        Route::patch('/profile/edit', 'Admin\ProfileController@update');
        Route::get('/profile/change-password', 'Admin\ProfileController@changePassword')->name('profile.password');
        Route::patch('/profile/change-password', 'Admin\ProfileController@updatePassword');

        //Api Access
        Route::get('api-access', 'Admin\ApiAccessController@index');
        Route::post('api-access', 'Admin\ApiAccessController@postPass');
        Route::post('api-access/regen', 'Admin\ApiAccessController@regenerateToken');

        // QR CODE
        Route::resource('qrcode', 'Admin\\QrcodeController');
        Route::get('/qrcode-data/', 'Admin\QrcodeController@datatable');

        // Language
        Route::resource('languages', 'Admin\\LanguagesController');

        //Settings
        Route::resource('settings', 'Admin\\SettingsController');
        Route::get('/settings-data/', 'Admin\SettingsController@datatable');


    });

});



// Add this route for checkout or submit form to pass the item into paypal
Route::get('/paypal/payment/',['as' => 'paypal.payment', 'uses' => 'PaypalController@processPayment' ]);

Route::get('order/paypal/callback/success', 'PaypalController@paypalCallbackSuccess');
Route::get('order/paypal/callback/cancel', 'PaypalController@paypalCallbackCancel');

Route::get('/renew-qrcode', function(){
    Artisan::call('RenewQrcode:renew_qrcode');
});

Route::get('/{code?}', 'QrcodeController@getQrcode');





