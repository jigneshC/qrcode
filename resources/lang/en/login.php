<?php
return[

    'login' => 'Log In',
    'signup' => 'Sign Up',
    'email_address' => 'E-Mail Address',
    'password' => 'Password',
    'remember_me' => 'Remember Me',
    'sign_in' => 'Sign In',
    'forgot_your_password' => 'Forgot your password ?',
    'new' => 'New ?',

    //Register
    'register' => 'Register',
    'name' => 'Name',
    'email' => 'E-Mail',
    'phone' => 'Phone',
    'confirm_password' => "Confirm Password",
    'already_register' => 'Already Register ?',

    //access denied
    'access_denied' => 'Access Denied',
    'dont_have_permission' => " You don't have permission for access this page.",

    //emails
    'reset_password' => 'Reset Password',
    'send_password_reset_link' => 'Send Password Reset Link',

    //reset




    
];