<?php
return[
    // language dropdown
    'language' => 'Langue',
    
    //buttons
    'create_a_qr_code_for_free' => 'Créer un QR Code gratuitement',
    'get_started' => 'Commencer Maintenant',
    'login' => 'Se connecter',
    'signup' => 'S\'inscrire',
    'welcome' => 'Bienvenue',
    'logout' => 'Se déconnecter',
    'buy_now' => 'Acheter maintenant',
    'view_all_packages' => 'Voir tous les Packs',
    'messages' => 'Messages',


    //Content
    'content' =>  [

        'top' => [
            'h1' => "Démarrer votre",
            'h2' => "Générateur de QR Codes",
            'p1' => "La façon instantanée de cibler",
            'p2' => "les clients en déplacement",
        ],
        
        
        'section_marketing' => [
                'heading' => "Votre marketing est juste devenu plus facile grâce aux QR Codes pouvant être mis à jour",
                'p' => [
                    "Des affiches, t-shirts, tasses, prospectus, brochures et catalogues aux cartes de visite, les QR Codes sont partout. En effet, les résultats parlent d'eux-mêmes: des millions d'analyses et de conversions de clients dans le monde entier. Les QR Codes sont rapides, amusants et efficaces.",
                    "Les clients n'ont besoin que d'un smartphone pour accéder instantanément à tous vos contenus numériques. Pour les entreprises avisées, les QR Codes sont devenus un outil de marketing de premier plan pour les aider à passer le mot et avoir un impact sur une population plus jeune.",
                    "Mais que se passe-t-il si vous voulez constamment mettre à jour votre contenu numérique? Avec la plupart des services de QR Codes, vous devrez créer et imprimer un tout nouveau QR Code.",
                    "C'est pourquoi nous avons créé la bibliothèque de QR Codes Dynamiques: C'est un générateur de QR Code renouvelable qui vous permet de changer le lien de destination de votre QR Code autant de fois que vous le souhaitez. Que vous changiez d'adresse URL, de vidéo, d'image ou de coordonnées, il vous suffit de vous connecter à un portail en ligne (avec API pour le QR Codes) et de mettre à jour les détails de votre QR Code. Pas de problème avec des résultats rapides.",                    
                ]

        ],

        'section_professional' => [
            'heading' => "Un générateur de QR Code professionnel",
            'tab_1' => [
                'li' => "Créer des QR Codes uniques",
                'p' => "Vous pouvez créer de nouveaux QR Codes qui relient votre site Web, formulaire de contact, vidéo ou autres médias numériques en quelques minutes. En utilisant juste un Smartphone, n'importe qui n'importe où peut alors scanner votre QR Code imprimé, interagir avec votre entreprise et aller plus loin avec votre liste de clients potentiels."
            ],
            'tab_2' => [
                'li' => "Mettre à jour les QR Codes lorsque vous en avez besoin",
                'p' => "Les campagnes en ligne peuvent changer en un instant. Avec un QR Code renouvelable, vous pouvez facilement mettre à jour et gérer votre lien de destination sans réimprimer votre QR Code. Il suffit de se connecter sur un portail en ligne (nous offrons un API pour les QR Codes) et de modifier l'URL ou le fichier lié autant de fois que vous le souhaitez. Il vous donne plus de flexibilité et de vitesse."
            ], 
            'tab_3' => [
                'li' => "Obtenez des résultats de haute qualité",
                'p' => "Concevez une campagne élégante et captivante grâce à nos QR Codes haute résolution 32 bits et 64 bits. Nous offrons des formats d'impression téléchargeables en Jpeg, Png, Eps et Svg pour des résultats étonnants. Vous pouvez créer la première impression parfaite avec des graphiques QR Codes de qualité professionnelle. "
            ],
            'tab_4' => [
                'li' => "Partagez votre compte avec d'autres",
                'p' => "Travailler en équipe? Ajoutez d'autres utilisateurs au portail qr Code Library avec nos fonctionnalités de partage de compte. Choisissez le niveau d'accès pour chaque utilisateur, tel que les droits de visualisation ou le statut administratif complet. Notre générateur de QR Code flexible vous aide à organiser et rationaliser votre campagne."
            ],
            'tab_5' => [
                'li' => "Obtenir de l'aide et du support utilisateur",
                'p' => "Obtenez de l'aide pour gérer votre bibliothèque de QR Codes. Pour toute question, conseil ou dépannage, contactez notre service clientèle par e-mail et nous vous répondrons dans les plus brefs délais. Nous travaillons pour vous donner la meilleure expérience de générateur de QR Code."
            ]

        
            ],

            'section_plan' => [
                'heading_2' => "Plans et prix",
                'heading_3' => "Tarification simple et transparente Sachez toujours ce que vous allez payer.",
                'free_trial' => "Essai gratuit",
                'most_popular' => "Le plus populaire",
                'business' => "Entreprise",
                'per_point' => "Par Point",
                'points_earn' => "Points que vous pouvez gagner",
                'per_account_user' => "Qr Code par compte utilisateurs",
                'scans' => "Nombre de scans",
                'formats' => "Formats JPEG, PNG, EPS et SVG",
                'customer_support' => "Service client",
                'api' => "API pour le QR Code",
                'tracking' => "Suivi de QR Code",

            ],

            'faq' => [
                'heading' => "Questions fréquemment posées",
                'q1' => "Qu'est-ce qu'un QR Code?",
                'ans1' => "Un QR Code (Quick Response Code) est un code composé de motifs de pixels en noir et blanc. Avec la capacité d'encoder jusqu'à plusieurs centaines de caractères, les QR Codes sont beaucoup plus puissants que les codes à barres traditionnels des supermarchés.",
                "ans1_a" => "Au cours de la dernière décennie, les QR Codes ont explosé en raison de l'essor des smartphones: les utilisateurs peuvent simplement pointer leur appareil photo vers un QR Code et, sans entrer d'URL, accéder directement au site Web, formulaire de contact, vidéo ou autres médias numériques. Les spécialistes du marketing en particulier ont vu le potentiel des QR Codes dans la promotion d'événements, de produits et de services.",

                'q2' => "Où puis-je mettre mon QR Code?",
                'ans2' => "Après avoir créé votre QR Code dans un générateur de QR Code, vous pouvez imprimer votre code sur des objets physiques tels que des affiches, des brochures et des catalogues, ainsi que des tasses, des t-shirts, des badges, des trousses et d'autres marchandises. Vous pouvez également envoyer des QR Codes directement aux clients.",

                'q3' => "Comment créer un QR Code renouvelable?",
                'ans3' => "Connectez-vous simplement à la bibliothèque de QR Codes, choisissez le support numérique qui se connecte à votre QR Code, puis téléchargez et imprimez votre QR Code qui peut être mis à jour. Cela prend juste une minute. Suivez ensuite les mêmes étapes si vous avez besoin de mettre à jour le lien associé à votre QR Code renouvelable.",

                'q4' => "Qu'est-ce qui différencie la bibliothèque de QR Codes des autres générateurs de QR Code?",
                'ans4' => "La bibliothèque de QR Codes offre des QR Codes renouvelables. Contrairement aux générateurs de QR Code traditionnels, vous n'avez pas besoin de continuer à réimprimer votre code lorsque votre lien de destination change. Au lieu de cela, vous pouvez mettre à jour votre lien de médias numériques QR Code en vous connectant sur le portail en ligne. Notre service professionnel, offrant une API pour le QR Code, gère vos codes de manière flexible et pratique.",

                'q5' => "Comment puis-je utiliser au mieux les QR Codes sur mes documents marketing?",
                'ans5' => "Engagez vos clients en leur faisant connaître les avantages de la numérisation du code. Par exemple, vous pouvez mettre un court texte à côté du code: 'Scanner pour obtenir votre remise de 15%'. Veillez également à dimensionner votre QR Code d'au moins deux centimètres de large et à afficher votre graphique dans la meilleure résolution possible. De cette façon, vos clients peuvent scanner avec succès à leur première tentative.",

                'q6' => "Puis-je donner accès à d'autres personnes à mon compte QR Code Library?",
                'ans6' => "Oui. Nous offrons plusieurs accès utilisateur à votre générateur de QR Code. Vous pouvez leur donner différents niveaux d'accès, tels que des droits de visionnement limités ou une puissance administrative complète. De plus, notre service professionnel gère l'ensemble de votre collection de QR Codes en utilisant l'API pour le QR Code.", 

            ]


    ]

    
];