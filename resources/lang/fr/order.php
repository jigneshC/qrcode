<?php

return [

    // Show
    'back' => 'Retour',
    'order' => 'Commande',
    'order_id' => "Numéro de commande",
    'user_name' => "Nom d'utilisateur",
    'user_email' => "Email de l'utilisateur",
    'user_phone' => "Téléphone de l'utilisateur",
    'item_details' => "Détails de l'article",
    'package_name' => 'Nom du Pack',
    'package_price' => 'Prix ​​du Pack',
    'number_of_qrcodes' => 'Nombre de Qrcodes',
    'order_details' => 'Détails de la commande',
    'order_quantity' => "Quantité d'ordre",
    'order_amount' => 'Montant de la commande',
    'order_discount' => 'Remise de commande',
    'order_net_amount' => 'Montant net de la commande',
    'order_status' => 'Statut de la commande',
    'payment_details' => 'Détails de paiement',
    'payment_type' => 'Type de paiement',
    'payment_date' => 'Date de paiement',
    'payment_reference_id' => 'Identifiant de référence de paiement',
    'payment_request_id' => 'Numéro de demande de paiement',
    'payment_status' => 'Statut du paiement',
    
    //Index
    'id' => 'ID',
    'order_placed_by' => 'Commande placée par',
    'no_of_qrcodes' => 'Nombre de QrCodes',
    'package_title' => 'Titre du Pack',
    'currency' => 'Devise',
    'actions' => 'Actions',
    'no_orders_found' => 'Aucune commande trouvée',
    'view' => 'Vue',
    'orders' => 'Ordres',
    'points' => 'Points',
    'total_no_of_points' => 'Nombre total de points',
    'points_you_can_earn' => 'Points que vous pouvez gagner',

    //Checkout
    'checkout_qrcode' => 'Commander - QR Code',
    'checkout' => 'Passez à la caisse',
    'billing_details' => 'Détails de la facturation',
    'first_name' => 'Prénom *',
    'last_name' => 'Nom de famille',
    'email' => 'Email *',
    'address' => 'Adresse',
    'phone' => 'Téléphone *',
    'your_order' => 'Votre commande',
    'product' => 'Produit',
    'product_name' => 'Nom du produit',
    'validity' => 'Validité',
    'grand_total' => 'Somme finale',
    'place_order' => 'Passer la commande',
    'total' => 'Total',

    //form
    'buy_now' => 'Achetez maintenant',
    'send_me_my_free_qrcode' => 'Envoyez-moi mon QrCode gratuit',


];