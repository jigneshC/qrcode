<?php

return [

    //show-list
    'package' => 'Pack',
    'back' => 'Retour',
    'id' => 'Id',
    'title' => 'Titre',
    'description' => 'Description',
    'qrcode_no' => 'Nombre de QR Codes',
    'expiry_year' => 'Année d\'expiration',
    'currency' => 'Devise',
    'created_on' => 'Crée le',
    'actions' => 'Actions',
    

    //Index
    'plan_and_pricing_qrcode' => 'Plan And Pricing - QR Code',
    'buy_now' => 'Achetez Maintenant',
    'number_of_renewable_qrcodes' => 'Nombre de QR Codes Renouvables',
    'number_of_account_users' => 'Nombre de Comptes Utilisateurs',
    'qr_code_validity' => 'Validité du QR Code',
    'total_points' => 'Total de Points',
    'create_a_qr_code_for_free' => "Créez un QR Code GRATUITEMENT",
    
    //content
    'content' =>  [
        'text_line_1' => "Prix Simples, transparents.",
        'text_line_2' => "Sachez toujours ce que vous payez.",
        
        'section_competition' => [
                'heading' => "Restez en tête de la compétition",
                'li' => [
                    "Atteignez plus de clients dans les espaces publics",
                    "Connectez-vous avec un groupe démographique plus jeune et technophile",
                    "Offrez un accès instantané à vos médias numériques",
                    "Générez plus de clics sur votre site Web",
                    "Déplacez les gens plus loin dans votre perspectives de ventes ",
                    "Créez vos QR codes en quelques minutes",
                    "Stratégie marketing simple, interactive et rentable",
                    "Les codes à vie actualisables qui n'expirent jamais",
                    "Télécharger, imprimer ou envoyer directement par email les QR codes",
                ]

                ],

        'section_question' => [
            'heading' => "Qu'est-ce que l'API pour le QR code?",
            'p' => "Que vous ayez besoin de gérer un inventaire ou simplement de suivre un colis, nous offrons l'accès à une API pour les QR codes avec des commandes groupées de plus de 50 codes. Notre outil professionnel permet à votre campagne marketing de rester organisée.",
            'li' => [
                "Commandez plus de 50 QR codes renouvelables.",
                "Gérez vos QR codes via notre API.",
                "Afficher les informations complètes sur le produit liées à vos QR codes achetés.",
                ]
            ]
        
        ]
    




];