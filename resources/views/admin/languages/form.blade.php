<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('lang_code') ? 'has-error' : ''}}">
    {!! Form::label('lang_code', 'Lang Code', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('lang_code', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('lang_code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('country') ? 'has-error' : ''}}">
    {!! Form::label('country', 'Country', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('country', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('country', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('country_code') ? 'has-error' : ''}}">
    {!! Form::label('country_code', 'Country Code', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('country_code', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('country_code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('currency_code') ? 'has-error' : ''}}">
    {!! Form::label('currency_code', 'Currency Code', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('currency_code', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('currency_code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('currency_symbol') ? 'has-error' : ''}}">
    {!! Form::label('currency_symbol', 'Currency Symbol', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('currency_symbol', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('currency_symbol', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', trans('company.label.active'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="checkbox">
            <label>{!! Form::radio('active', '1',true) !!} Yes</label>
        </div>
        <div class="checkbox">
            <label>{!! Form::radio('active', '0') !!} No</label>
        </div>
        {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
