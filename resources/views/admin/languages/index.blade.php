@extends('layouts.backend')


@section('title','Languages')


@section('pageTitle','Languages')


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        Languages
                    </div>

                </div>
                <div class="box-content ">


                    <div class="row">
                        <div class="col-md-6">
                            @if(Auth::user()->can('access.language.create'))
                            <a href="{{ url('/admin/languages/create') }}" class="btn btn-success btn-sm"
                               title="Add New Language">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add New
                            </a>
                            @endif
                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/languages', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            {!! Form::close() !!}

                        </div>

                    </div>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Lang Code</th>
                                <th>Country</th>
                                <th>Country Code</th>
                                <th>Currency Code</th>
                                <th>Currency Symbol</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($languages as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->lang_code }}</td>
                                    <td>{{ $item->country }}</td>
                                    <td>{{ $item->country_code }}</td>
                                    <td>{{ $item->currency_code }}</td>
                                    <td>{{ $item->currency_symbol }}</td>
                                    <td>
                                        @if($item->active == 1)
                                            <span class="label label-success">True</span>
                                        @else
                                            <span class="label label-danger">False</span>
                                        @endif
                                    </td>


                                    <td>
                                        <a href="{{ url('/admin/languages/' . $item->id) }}" title="View Language">
                                            <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                   aria-hidden="true"></i> View
                                            </button>
                                        </a>

                                        @if($item->lang_code != Auth::user()->language)
                                            @if(Auth::user()->can('access.language.edit'))
                                             <a href="{{ url('/admin/languages/' . $item->id . '/edit') }}"
                                               title="Edit Language">
                                                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                          aria-hidden="true"></i> Edit
                                                </button>
                                            </a>
                                            @endif
                                            @if(Auth::user()->can('access.language.delete'))
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/languages', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-danger btn-xs',
                                                    'title' => 'Delete Language',
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                            {!! Form::close() !!}
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $languages->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
