@extends('layouts.backend')

@section('content')
        <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                      <div class="box-header blue-background">
                          <div class="title">
                              <i class="icon-circle-blank"></i>
                              Checkout
                          </div>

                      </div>
                      <div class="box-content ">


                        <a href="{{ url('/admin/orders') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        @if($order->payment_type == "free")
                        {!! Form::open(['url' => '/freepackage', 'class' => 'form-horizontal', 'files' => true]) !!}
                        @else
                        {!! Form::open(['url' => '/paypal/payment/', 'class' => 'form-horizontal', 'files' => true]) !!}
                        @endif

                        <input type="hidden" name="order_id" value="{{ $order->id }}">
                           
                        <div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
                            {!! Form::label('first_name', 'First Name :', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
                            {!! Form::label('last_name', 'Last Name :', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                            {!! Form::label('address', 'Address :', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('address', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
                            {!! Form::label('phone', 'Phone :', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Submit', ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>

                        {!! Form::close() !!}




                    </div>
                </div>
            </div>
        </div>
@endsection


