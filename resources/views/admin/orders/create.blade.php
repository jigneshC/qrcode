@extends('layouts.backend')

@section('content')
        <div class="row">
            <div class="col-md-12">
             <div class="box bordered-box blue-border">
                    <div class="box-header blue-background">
                        <div class="title">
                            <i class="icon-circle-blank"></i>
                            Confirm Order
                        </div>

                    </div>
                    <div class="box-content ">

                        <a href="{{ url('/admin/orders') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                       
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr><th> Package </th><td> {{ $package->title }} </td></tr>
                                    <tr><th> Description </th><td> {{ $package->description }} </td></tr>
                                    <tr><th> Qrcode No </th><td> {{ $package->qr_code_no }} </td></tr>
                                    <tr><th> Expiry Year </th><td>  {{ $package->expiry_year }} </td></tr>
                                    <tr><th> Currency </th><td>  {{ $package->currency }} </td></tr>
                                   
                                </tbody>
                            </table>

                            {!! Form::open(['url' => '/admin/orders', 'class' => 'form-horizontal', 'files' => true]) !!}
                            <input type="hidden" name="package_id" value="{{ $package->id }}">
                            @include ('admin.orders.form')

                            {!! Form::close() !!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
