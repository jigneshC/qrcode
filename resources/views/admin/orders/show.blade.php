@extends('layouts.backend')

@section('content')
        <div class="row">
            <div class="col-md-12">
             <div class="box bordered-box blue-border">
                                          <div class="box-header blue-background">
                                                              <div class="title">
                                                                  <i class="icon-circle-blank"></i>
                                                               Order #{{ $order->id }}
                                                              </div>

                                           </div>
                                           <div class="box-content ">

                        <a href="{{ url('/admin/orders') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

                        {{$order->user->name}}
                        {{--
                        @if(Auth::user()->can('access.orders.edit'))
                        <a href="{{ url('/admin/packages/' . $package->id . '/edit') }}" title="Edit Qrcode"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                       @endif
                       

                       @if(Auth::user()->can('access.orders.delete'))
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/packages', $package->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete Package',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        @endif 
                        --}}

                        <br/>
                        <br/>
                           
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>Order ID</th><td>{{ $order->id }}</td>
                                    </tr>
                                    <tr>
                                        <th>----------User Details----------</th><td></td>
                                    </tr>
                                    <tr>
                                        <th>User Name</th><td>{{ $order->user->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>User Email</th><td>{{ $order->user->email }}</td>
                                    </tr>
                                    <tr>
                                        <th>User Phone</th><td>{{ $order->user->phone }}</td>
                                    </tr>
                                    <tr>
                                        <th>----------Item Details----------</th><td></td>
                                    </tr>
                                    <tr>
                                        <th>Package Name</th><td>{{ $order->package_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Package Price</th><td>{{ $order->package_price }}</td>
                                    </tr>
                                    <tr>
                                        <th>----------Order Details----------</th><td></td>
                                    </tr>
                                    <tr>
                                        <th>Order Quantity</th><td>{{ $order->order_qty }}</td>
                                    </tr>
                                    <tr>
                                        <th>Order Amount</th><td>{{ $order->order_amount }} {{ $order->order_currency }}</td>
                                    </tr>
                                    <tr>
                                        <th>Order Discount</th><td>{{ $order->order_discount }}</td>
                                    </tr>
                                    <tr>
                                        <th>Order Net Amount</th><td>{{ $order->order_net_amount }} {{ $order->order_currency }}</td>
                                    </tr>
                                    <tr>
                                        <th>Order Status</th><td>{{ $order->order_status }}</td>
                                    </tr>
                                    <tr>
                                        <th>----------Payment Details----------</th><td></td>
                                    </tr>
                                    <tr>
                                        <th>Payment Type</th><td>{{ $order->payment_type }}</td>
                                    </tr>
                                    <tr>
                                        <th>Payment Date</th><td>{{ $order->created_at }}</td>
                                    </tr>
                                    <tr>
                                        <th>Payment Reference ID</th><td>{{ $order->payment_reference_id }}</td>
                                    </tr>
                                    <tr>
                                        <th>Payment Request ID</th><td>{{ $order->payment_request_id }}</td>
                                    </tr>
                                    <tr>
                                        <th>Payment Status</th><td>{{ $order->payment_status }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
