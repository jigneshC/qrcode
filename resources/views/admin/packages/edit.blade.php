@extends('layouts.backend')

@section('content')
        <div class="row">
            <div class="col-md-12">
               <div class="box bordered-box blue-border">
                   <div class="box-header blue-background">
                                       <div class="title">
                                           <i class="icon-circle-blank"></i>
                                          Edit Package #{{ $package->title }}
                                       </div>

                    </div>
                    <div class="box-content ">


                        <a href="{{ url('/admin/packages') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($package, [
                            'method' => 'PATCH',
                            'url' => ['/admin/packages', $package->id],
                            'class' => 'form-horizontal',
                            'id'=>'form_id',
                            'files' => true
                        ]) !!}

                        @include ('admin.packages.form', ['submitButtonText' => 'Update','action'=>'edit'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
@endsection
