<div class="panel with-nav-tabs panel-primary">
    <div class="panel-heading">
        <ul class="nav nav-tabs">

            @foreach($languages as $language)

            <li {{ $loop->first ? 'class=active' : '' }}>
                <a href="#tab{{$language->name}}" data-toggle="tab">{{$language->name}}</a>
            </li>

            @endforeach

        </ul>
    </div>

    <div class="panel-body">
        <div class="tab-content">
            @foreach($languages as $language)
            <div class="tab-pane fade in {{ $loop->first ? 'active' : '' }}" id="tab{{$language->name}}">
                <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                    {!! Form::label('title', 'Title(*) :', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text($language->lang_code.'[title]', null, ['class' => 'form-control']) !!} {!! $errors->first('title', '
                        <p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                    {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::textarea($language->lang_code.'[description]', null, ['class' => 'form-control summernote']) !!} {!! $errors->first('description',
                        '
                        <p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            @endforeach
            <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
                {!! Form::label('type', 'Type', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    <label>{!! Form::radio('type','Free' , ['class' => 'form-control','id' => 'free']) !!} Free </label>
                    <label>{!! Form::radio('type','Pricing' , ['class' => 'form-control','id'=> 'price']) !!} Pricing </label>
                    <span style="color:blue;" class="expire"></span>
                    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('qr_code_no') ? 'has-error' : ''}}">
                {!! Form::label('qr_code_no', 'Points(*) :', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::number('qr_code_no', null, ['class' => 'form-control','id' => 'qrcode','min' => '1' , 'step' => 'any']) !!} {!! $errors->first('qr_code_no',
                    '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>
            {{--
            <div class="form-group {{ $errors->has('expiry_year') ? 'has-error' : ''}}">
                {!! Form::label('expiry_year', 'Expiry Years :', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::number('expiry_year', null, ['class' => 'form-control', 'id' => 'expiry_year','Placeholder'=>'Enter number of Years','min'=>'1',
                    'max'=>'99' ,'step'=>'1']) !!} {!! $errors->first('expiry_year', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('expiry_months') ? 'has-error' : ''}}">
                {!! Form::label('expiry_months', 'Expiry Months :', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::number('expiry_months', null, ['class' => 'form-control','id' => 'expiry_months','Placeholder'=>'Enetr Number of
                    Months','min'=>'1', 'max'=>'12' ,'step'=>'1']) !!} {!! $errors->first('expiry_months', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>
            --}}

            <div class="form-group {{ $errors->has('amount') ? 'has-error' : ''}}">
                {!! Form::label('amount', 'Amount (*) :', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('amount', null, ['class' => 'form-control', 'id' => 'amount']) !!} {!! $errors->first('amount', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('currency') ? 'has-error' : ''}}">
                {!! Form::label('currency', 'Currency :', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::select('currency',['USD' => 'USD'], null, ['class' => 'form-control','id'=>'currency']) !!} {!! $errors->first('currency',
                    '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>
            
            <div class="form-group ">
                {!! Form::label('', '', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    <label>{!! Form::checkbox('is_feature','1',isset($package) && $package->is_feature == 1 ? true : false ) !!} Show on Home Page as Most Popular Package </label><br>
                    <label>{!! Form::checkbox('is_feature','2',isset($package) && $package->is_feature == 2 ? true : false) !!} Show on Home Page as Bussiness Package</label>
                </div>
            </div>


            <div class="form-group">
                <div class="col-md-offset-4 col-md-4">
                    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>

        </div>
    </div>
</div>
@push('js')
<script>

    $("input:checkbox").click(function() {
        if ($(this).is(":checked")) {
            var group = "input:checkbox[name='" + $(this).attr("name") + "']";
            $(group).prop("checked", false);
            $(this).prop("checked", true);
        } else {
            $(this).prop("checked", false);
        }
    });

    $(document).ready(function() {
        $('.summernote').summernote({
            height: 300,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: true,                  // set focus to editable area after initializing summernote
            callbacks: {
                onImageUpload: function(image) {
                    uploadImage(image[0]);
                },
                onMediaDelete: function($target, editor, $editable) {
                    deleteImage($($target[0]).attr("data-name"));
                    $target.remove();
                }
            }
        });

        jQuery(window).on("load", function(){
            var type = $("input[name=type]:checked").val();
            if(type == "Free"){
                document.getElementById("amount").disabled = true; 
                document.getElementById("currency").disabled = true; 
                document.getElementById("qrcode").disabled = true; 
                $('#qrcode').val('0');
                $('#amount').val('0');
                $(".expire").html("Free Qrcodes will get Expire in 15 Days"); 
            }
        });
        
        $('input[type=radio][name=type]').change(function() {
            var package_type = this.value; 
            if (this.value == 'Free') {
                document.getElementById("amount").disabled = true; 
                document.getElementById("currency").disabled = true; 
                document.getElementById("qrcode").disabled = true; 
                $('#qrcode').val('0');
                $('#amount').val('0');
                $(".expire").html("Free Qrcodes will get Expire in 15 Days");

            }
            else if (this.value == 'Pricing') {
                var qrcode = '' ;
                var amount = '' ;
                @if(Route::currentRouteName() == 'packages.edit')
                    qrcode = '{{$package->qr_code_no}}';
                    amount = '{{$package->amount}}';
                @endif
                document.getElementById("amount").disabled = false; 
                document.getElementById("currency").disabled = false; 
                document.getElementById("qrcode").disabled = false; 
                $(".expire").html("");
                $('#qrcode').val(qrcode);
                $('#amount').val(amount);
            }
        });

        $("#form_id").validate({
          
        rules: { 
                title: {
                     required: true
                },
                type:{
                    required:true
                },
                qr_code_no:{
                    required:true
                },
                amount:{
                    required: true
                },
                currency:{
                    required:true
                }
        
            },
            messages: {
                title: {
                    required: "Title is required",
                },
                type: {
                    required: "Please Select The Type of Package",
                },
                qr_code_no: {
                    required: "Enter the Number of Qrcodes",
                },
                amount: {
                    required: "Enter the amount of Package",
                },
                currency: {
                    required: "Select the Currency",
                },
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });

</script>



@endpush