@extends('layouts.backend')
@section('title','Edit QR Code')
@section('content')
        <div class="row">
            <div class="col-md-12">
               <div class="box bordered-box blue-border">
                   <div class="box-header blue-background">
                                       <div class="title">
                                           <i class="icon-circle-blank"></i>
                                          Edit QR Code #{{ $qrcode->id }}
                                       </div>

                    </div>
                    <div class="box-content ">


                        <a href="{{ url('/admin/qrcode') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($qrcode, [
                            'method' => 'PATCH',
                            'url' => ['/admin/qrcode', $qrcode->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('admin.qrcode.form', ['submitButtonText' => 'Update','action'=>'edit'])

                        

                        {!! Form::close() !!}

                        

                    </div>
                </div>
            </div>
        </div>
@endsection
