<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-md-1 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('redirectlink') ? 'has-error' : ''}}">
    {!! Form::label('redirectlink', 'Redirectlink', ['class' => 'col-md-1 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('redirectlink', null, ['class' => 'form-control']) !!}
        {!! $errors->first('redirectlink', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if(isset($action) && $action=="edit")
<div class="form-group">
    {!! Form::label('Hash Code', 'hash', ['class' => 'col-md-1 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('hash', null, ['class' => 'form-control','disabled'=>'disabled']) !!}
    </div>
</div>
@endif

<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'col-md-1 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('status',['1' => 'Active','0'=>'Inactive'], null, ['class' => 'form-control']) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="col-md-6 col-sm-12 col-xs-12 pull-right">
    <div class="row-1 size text-center">
        <img src="{!! asset('uploads/qrcode') !!}/{{ $qrcode->hash }}.jpg" alt="barcode"  height="300" style="margin-top:-200px;
        " />
    </div>

</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
