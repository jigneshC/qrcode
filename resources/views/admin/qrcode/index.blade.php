@extends('layouts.backend')
@section('title','QR Code')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        QR Code
                    </div>

                </div>
                <div class="box-content ">

                    @if(Auth::user()->can('access.qrcode.create'))
                    <a href="{{ url('/admin/qrcode/create') }}" class="btn btn-success btn-sm" title="Add New Qrcode">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add New
                    </a>
                    @endif
{{-- 
                    {!! Form::open(['method' => 'GET', 'url' => '/admin/qrcode', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                    <div class="input-group" style="max-width: 300px">
                        <input type="text" class="form-control" name="search" placeholder="Search...">
                        <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                    </div>
                    {!! Form::close() !!} --}}

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table table-borderless" id="qrcode-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Hash</th>
                                <th>Redirectlink</th>
                                <th>Purchased By</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            {{-- <tbody>
                            @if(count($qrcode) == 0)
                            <tr><td colspan=6>No Qr Codes Found</td></tr>
                            @endif
                            @foreach($qrcode as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->hash }}</td>
                                    <td>{{ $item->redirectlink }}</td>
                                    <td>{{ $item->user->name or null }}</td>
                                    <td>
                                        <a href="{{ url('/admin/qrcode/' . $item->id) }}" title="View Qrcode">
                                            <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                   aria-hidden="true"></i> View
                                            </button>
                                        </a>
                                        @if(Auth::user()->can('access.qrcode.edit'))
                                        <a href="{{ url('/admin/qrcode/' . $item->id . '/edit') }}" title="Edit Qrcode">
                                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                      aria-hidden="true"></i> Edit
                                            </button>
                                        </a>
                                        @endif
                                        @if(Auth::user()->can('access.qrcode.delete'))
                                        {!! Form::open([
                                            'method'=>'DELETE',
                                            'url' => ['/admin/qrcode', $item->id],
                                            'style' => 'display:inline'
                                        ]) !!}


                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs',
                                                'title' => 'Delete Qrcode',
                                                'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        @endif
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody> --}}
                        </table>
                        {{-- <div class="pagination-wrapper"> {!! $qrcode->appends(['search' => Request::get('search')])->render() !!} </div> --}}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

    <script>

        var url ="{{ url('admin/qrcode-data') }}";
        var edit_url = "{{ url('admin/qrcode') }}";

        //Permissions
        var edit_qrcode ="{{ Auth::user()->can('access.qrcode.edit') }}";
        var delete_qrcode ="{{ Auth::user()->can('access.qrcode.delete') }}";
    
        
        datatable = $('#qrcode-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]], 
            ajax: {
                url:url,
                type:"get",
                data: function (d)  {
                   d.user_id = "{{ $user_id }}";
                }
            },
                columns: [
                    { data: 'id', name: 'id',  "orderable":true,searchable:true },
                    { data: 'name', name: 'name', "orderable":true, searchable:true },
                    { data: 'hash', name: 'hash', "orderable":true, searchable:true },
                    { data: 'redirectlink', name: 'redirectlink', "orderable":true, searchable:true },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "defaultContent":'-',
                        "render": function (o) {
                            if(o.user){
                                return o.user.name;
                            }else{
                                return '-';
                            }
                            
                        }
                    
                    
                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var v="" ; var e=""; var d=""; 
                            
                            v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i>View</button></a>&nbsp;";

                            if(edit_qrcode){
                                e= "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" class='edit-item' ><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>Edit</button></a>&nbsp;";
                            }
                               
                            if(delete_qrcode){
                                d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" ><button class='btn btn-danger btn-xs'><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a>&nbsp;";
                            }
                            

                            
                            return v+e+d;
                        }
                    
                    
                    }
                ]
        });

        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            url = "{{url('admin/qrcode')}}/" + id;
            var r = confirm("Are you sure you want to delete this Qr Code ?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(data.messages)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            }
        });

    </Script>
    
@endpush
