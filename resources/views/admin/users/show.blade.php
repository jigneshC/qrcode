@extends('layouts.backend')

@section('title','Show User')


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">User</div>
                <div class="panel-body">

                    <a href="{{ url('/admin/users') }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </button>
                    </a>
                    <a href="{{ url('/admin/users/' . $user->id . '/edit') }}" title="Edit User">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            Edit
                        </button>
                    </a>
                    {!! Form::open([
                        'method' => 'DELETE',
                        'url' => ['/admin/users', $user->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete User',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>


                    <?php
                    $role = join(' + ', $user->roles()->pluck('label')->toArray());
                    ?>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                            <tr>
                                <td>ID</td>
                                <td>{{ $user->id }}</td>
                            </tr>


                            <tr>
                                <td>Name</td>
                                <td>{{ $user->name }}</td>
                            </tr>


                            <tr>
                                <td>Email</td>
                                <td>{{ $user->email }}</td>
                            </tr>

                            <tr>
                                <td>Role</td>
                                <td>{{ $role }}</td>
                            </tr>

                            <tr>
                                <td>Total Points Earned</td>
                                <td>{{ $points_earned  }}</td>
                            </tr>

                            <tr>
                                <td>Points Remaining</td>
                                <td>{{ $user->points }}</td>
                            </tr>

                            <tr>
                                <td>Total Points Used</td>
                                <td>{{ $points_used  }}</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection