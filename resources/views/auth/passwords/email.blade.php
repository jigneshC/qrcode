@extends('layouts.app')


@section('title',__('login.reset_password') )


@section('content')

<div class="card">
        <h3 class="text-center">@lang('login.reset_password')</h3>

        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <form class='validate-form' role="form" method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}
        <div class='form-group{{ $errors->has('email') ? ' has-error' : '' }}'>
            <div class='controls with-icon-over-input'>
                <input value="" placeholder=@lang('login.email') class="form-control" data-rule-required="true"
                       name="email" type="text" value="{{ old('email') }}"/>
                <i class='icon-user text-muted'></i>
            </div>
            @if ($errors->has('email'))
                <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
            @endif
        </div>


        <button class='btn btn-block' type="submit"> @lang('login.send_password_reset_link') </button>
    </form>
    <div class='text-center'>
    
            <i class='icon-user'></i>
            @lang('login.new')
        <a href='{!! route('register') !!}'>
            <strong> @lang('login.signup')</strong>
        </a>
    </div>
</div>

{{--
    <div class='login-container'>
        <div class='container'>
            <div class='row'>

                <div class='col-sm-4 col-sm-offset-4'>

                    <h1 class='text-center title'>Reset Password</h1>

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class='validate-form' role="form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}
                        <div class='form-group{{ $errors->has('email') ? ' has-error' : '' }}'>
                            <div class='controls with-icon-over-input'>
                                <input value="" placeholder="E-mail" class="form-control" data-rule-required="true"
                                       name="email" type="text" value="{{ old('email') }}"/>
                                <i class='icon-user text-muted'></i>
                            </div>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>


                        <button class='btn btn-block' type="submit">Send Password Reset Link</button>
                    </form>
                    {{--
                    <div class='text-center'>
                        <hr class='hr-normal'>
                        <a href='{{ route('password.request') }}'>Forgot your password?</a>
                    </div> 
                </div>

            </div>
        </div>
    </div>
    <div class='login-container-footer'>
        <div class='container'>
            <div class='row'>
                <div class='col-sm-12'>
                    <div class='text-center'>
                        <a href='{!! route('register') !!}'>
                            <i class='icon-user'></i>
                            New?
                            <strong>Sign up</strong>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    --}}

@endsection

{{--<div class="container">--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-8 col-md-offset-2">--}}
            {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">Reset Password</div>--}}
                {{--<div class="panel-body">--}}
                    {{--@if (session('status'))--}}
                        {{--<div class="alert alert-success">--}}
                            {{--{{ session('status') }}--}}
                        {{--</div>--}}
                    {{--@endif--}}

                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">--}}
                        {{--{{ csrf_field() }}--}}

                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                            {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--Send Password Reset Link--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}