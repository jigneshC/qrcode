@extends('layouts.app')


@section('title',__('login.register'))


@section('content')


<div class="card">
        <h3 class="text-center">@lang('login.register')</h3>
        <form class='validate-form' role="form" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <div class='form-group{{ $errors->has('name') ? ' has-error' : '' }}'>

                    <div class='controls with-icon-over-input'>
                        <input value="" placeholder="@lang('login.name')" class="form-control" data-rule-required="true"
                               name="name" type="text" value="{{ old('name') }}"  required autofocus/>
                        <i class='icon-user text-muted'></i>
                    </div>
                    @if ($errors->has('name'))
                        <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                    @endif
                </div>


                <div class='form-group{{ $errors->has('email') ? ' has-error' : '' }}'>
                    <div class='controls with-icon-over-input'>
                        <input value="" placeholder="@lang('login.email')" class="form-control" data-rule-required="true"
                               name="email" type="email" value="{{ old('email') }}" required/>
                        <i class='icon-user text-muted'></i>
                    </div>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                    @endif
                </div>

                <div class='form-group{{ $errors->has('phone') ? ' has-error' : '' }}'>
                    <div class='controls with-icon-over-input'>
                        <input value="" placeholder="@lang('login.phone')" class="form-control" data-rule-required="true"
                               name="phone" type="phone" value="{{ old('phone') }}" required/>
                        <i class='icon-phone text-muted'></i>
                    </div>
                    @if ($errors->has('phone'))
                        <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                    @endif
                </div>


                <div class='form-group{{ $errors->has('password') ? ' has-error' : '' }}'>
                    <div class='controls with-icon-over-input'>
                        <input value="" placeholder="@lang('login.password')" class="form-control" data-rule-required="true"
                               name="password" type="password"/>
                        <i class='icon-lock text-muted'></i>
                    </div>
                    @if ($errors->has('password'))
                        <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                    @endif
                </div>

                <div class='form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}'>
                    <div class='controls with-icon-over-input'>
                        <input value="" placeholder="@lang('login.confirm_password')" class="form-control" data-rule-required="true"
                               name="password_confirmation" type="password" required />
                        <i class='icon-lock text-muted'></i>
                    </div>
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                    @endif
                </div>
                <button class='btn btn-block' type="submit">@lang('login.register')</button>
            </form>
            <div class='text-center'>
                <hr class='hr-normal'>
                <a href='{{ route('password.request') }}'>@lang('login.forgot_your_password')</a><br>
                
                        <i class='icon-user'></i>
                        @lang('login.already_register')
                    <a href='{!! route('login') !!}'>
                        <strong>@lang('login.login')</strong>
                    </a>
            </div>
</div>


{{--
    <div class='login-container'>
        <div class='container'>
            <div class='row'>

                <div class='col-sm-4 col-sm-offset-4'>
                    <h1 class='text-center title'>Register</h1>
                    <form class='validate-form' role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class='form-group{{ $errors->has('name') ? ' has-error' : '' }}'>

                            <div class='controls with-icon-over-input'>
                                <input value="" placeholder="Name" class="form-control" data-rule-required="true"
                                       name="name" type="text" value="{{ old('name') }}"  required autofocus/>
                                <i class='icon-user text-muted'></i>
                            </div>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>


                        <div class='form-group{{ $errors->has('email') ? ' has-error' : '' }}'>
                            <div class='controls with-icon-over-input'>
                                <input value="" placeholder="E-mail" class="form-control" data-rule-required="true"
                                       name="email" type="email" value="{{ old('email') }}" required/>
                                <i class='icon-user text-muted'></i>
                            </div>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class='form-group{{ $errors->has('phone') ? ' has-error' : '' }}'>
                            <div class='controls with-icon-over-input'>
                                <input value="" placeholder="Phone" class="form-control" data-rule-required="true"
                                       name="phone" type="phone" value="{{ old('phone') }}" required/>
                                <i class='icon-phone text-muted'></i>
                            </div>
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                            @endif
                        </div>


                        <div class='form-group{{ $errors->has('password') ? ' has-error' : '' }}'>
                            <div class='controls with-icon-over-input'>
                                <input value="" placeholder="Password" class="form-control" data-rule-required="true"
                                       name="password" type="password"/>
                                <i class='icon-lock text-muted'></i>
                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class='form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}'>
                            <div class='controls with-icon-over-input'>
                                <input value="" placeholder="Confirm Password" class="form-control" data-rule-required="true"
                                       name="password_confirmation" type="password" required />
                                <i class='icon-lock text-muted'></i>
                            </div>
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif
                        </div>


                        <button class='btn btn-block' type="submit">Register</button>
                    </form>
                    <div class='text-center'>
                        <hr class='hr-normal'>
                        <a href='{{ route('password.request') }}'>Forgot your password?</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class='login-container-footer'>
        <div class='container'>
            <div class='row'>
                <div class='col-sm-12'>
                    <div class='text-center'>
                        <a href='{!! route('login') !!}'>
                            <i class='icon-user'></i>
                            Already register?
                            <strong>Login</strong>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

--}}



@endsection




{{--<div class="container">--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-8 col-md-offset-2">--}}
            {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading">Register</div>--}}
                {{--<div class="panel-body">--}}
                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                        {{--{{ csrf_field() }}--}}

                        {{--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">--}}
                            {{--<label for="name" class="col-md-4 control-label">Name</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"--}}
                                       {{--required autofocus>--}}

                                {{--@if ($errors->has('name'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('name') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                            {{--<label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control" name="email"--}}
                                       {{--value="{{ old('email') }}" required>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
                            {{--<label for="password" class="col-md-4 control-label">Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password-confirm" type="password" class="form-control"--}}
                                       {{--name="password_confirmation" required>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="col-md-6 col-md-offset-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--Register--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}