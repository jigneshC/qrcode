@extends('layouts.frontend')

{{--  @section('title','Dashboard')  --}}

@section('content')


<!-- Main content -->
<div class="">
    
     
        <!-- Content area -->
        <div class="content">
            <!-- Form horizontal -->
            
            
            <!-- dashboard row 1 -->
            <div class="row">
                <!-- start dashboard row 1 -->
                <section class="content-section">
                    <div class="row">
                        
                        <div class="col-lg-4 col-md-6 col-sm-6 " >

                            <!-- start col-1 -->
                            <div class="panel text-center">
                                <div class="panel-body">
                                    <div class="heading-elements">
                                    
                                    </div>

                                    <!-- Progress counter -->
                                    <div class="content-group-sm svg-center position-relative text-color-custom-1" id="hours-available-progress">
                                        <div class="round-1"> 
                                            <div class="loader"></div>
                                            <span class="span-number">{{$points_earned}}</span>
                                            <h3 class="mt-15 mb-5 uppercase">POINTS EARNED</h3>
                                            <div class="uppercase mb-10">{{$points_earned}} Points</div>
                                            
                                    
                                            
                                        </div>
                                    </div>
                                    <!-- /bars -->

                                <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
                            </div>
                            <!-- end of col-1 -->

                        </div>
                        <!-- end of col-1 -->
                        
                        <div class="col-lg-4 col-md-6 col-sm-6">

                            <!-- start col-2 -->
                            <div class="panel panel-height text-center">
                                <div class="panel-body">
                                    <div class="heading-elements">
                                    
                                    </div>

                                    <!-- Progress counter -->
                                    <div class="content-group-sm svg-center position-relative text-color-custom-2" id="hours-available-progress">
                                        <div class="round-1"> 
                                            <div class="loader-2"></div>
                                            <span class="span-number">{{Auth::user()->points}}</span>
                                            <h3 class="mt-15 mb-5 uppercase">REMAINING POINTS</h3>
                                            <div class="uppercase mb-10">{{Auth::user()->points}} Points</div>
                                    
                                    <!-- /progress counter -->

                                    
                                    </div>

                                <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
                            </div>
                            </div>
                            <!-- end of col-2 -->

                        </div>
                        <!-- end of col-2 -->
                        
                        <div class="col-lg-4 col-md-6 col-sm-6">

                            <!-- start col-3 -->
                            <div class="panel panel-height text-center">
                                <div class="panel-body">
                                    <div class="heading-elements">
                                    
                                    </div>

                                    <!-- Progress counter -->
                                    <div class="content-group-sm svg-center position-relative text-color-custom-3" id="hours-available-progress">
                                        <div class="round-1"> 
                                                <div class="loader-3"></div>
                                                <span class="span-number">{{$points_used}}</span>
                                                <h3 class="mt-15 mb-5 uppercase">POINTS USED</h3>
                                                <div class="uppercase mb-10">{{$points_used}} Points</div>
                                        </div>
                                    </div>
                                    <!-- /progress counter -->

                                    <!-- Bars -->
                                    

                                <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
                            </div>
                            <!-- end of col-3 -->

                        </div>
                        <!-- end of col-3 -->
                        
                        <div class="col-lg-4 col-md-6 col-sm-6">

                            <!-- start col-4 -->
                            <div class="panel panel-height text-center">
                                <div class="panel-body">
                                    <div class="heading-elements">
                                    
                                    </div>

                                    <!-- Progress counter -->
                                    <div class="content-group-sm svg-center position-relative text-color-custom-4" id="hours-available-progress">
                                        <div class="round-1"> 
                                            <div class="loader-4"></div>
                                            <span class="span-number">{{$total_qrcode}}</span>
                                            <h3 class="mt-15 mb-5 uppercase">TOTAL QR CODE</h3>
                                            <div class="uppercase mb-10">{{$total_qrcode}} Qr Codes</div>
                                        </div>
                                    
                                    </div>
                                    <!-- /progress counter -->

                                    

                                <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
                            </div>
                            <!-- end of col-4 -->

                        </div>
                        <!-- end of col-4 -->
                        
                        <div class="col-lg-4 col-md-6 col-sm-6">

                            <!-- start col-5 -->
                            <div class="panel panel-height text-center">
                                <div class="panel-body ">
                                    <div class="heading-elements">
                                    
                                    </div>

                                    <!-- Progress counter -->
                                    <div class="content-group-sm svg-center position-relative text-color-custom-5" id="hours-available-progress">
                                        <div class="round-1"> 
                                            <div class="loader-4"></div>
                                            <span class="span-number">{{$qrcode_expired}}</span>
                                            <h3 class="mt-15 mb-5 uppercase">QR CODE EXPIRED</h3>
                                            <div class="uppercase mb-10">{{$qrcode_expired}} Qr Codes</div>
                                        </div>
                                    </div>
                                    <!-- /progress counter -->

                                    

                                <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
                            </div>
                            <!-- end of col-1 -->

                        </div>
                        <!-- end of col-1 -->
                        
                    </div>	
                </section>
                <!-- end of dashboard row 1 -->
                    
                <!-- Footer -->
                <div class="footer text-muted">
                    &copy; <a href="#" target="_blank"> QR Code Generator 2018 </a>
                </div>
                <!-- /footer -->
            
            <!-- /content area -->
        </div>
        <!-- /main content -->


{{--

<!-- dashboard row 1 -->
<div class="row">
        <!-- start dashboard row 1 -->
        <section class="content-section">
            <div class="row">
                
                <div class="col-lg-2 col-md-4 col-sm-6">

                    <!-- start col-1 -->
                    <div class="panel text-center">
                        <div class="panel-body">
                            <div class="heading-elements">
                            
                            </div>

                            <!-- Progress counter -->
                            <div class="content-group-sm svg-center position-relative text-color-custom-1" id="hours-available-progress"><svg width="76" height="76"><g transform="translate(38,38)"><path class="d3-progress-background" d="M0,38A38,38 0 1,1 0,-38A38,38 0 1,1 0,38M0,36A36,36 0 1,0 0,-36A36,36 0 1,0 0,36Z" style="fill: rgb(238, 238, 238);"></path><path class="d3-progress-foreground" filter="url(#blur)" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill: rgb(240, 98, 146); stroke: rgb(240, 98, 146);"></path><path class="d3-progress-front" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill:rgb(66, 165, 245); fill-opacity: 1;"></path></g></svg><h3 class="mt-15 mb-5 uppercase">Points Earned</h3><span class="span-absolute">{{$points_earned}}</span><div class="uppercase">{{$points_earned}} Points</div></div>
                            <!-- /progress counter -->

                            <!-- Bars -->
                            <div id="hours-available-bars"><svg width="208.484375" height="40"><g width="208.484375"><rect class="d3-random-bars" width="6.005722736625515" x="2.5738811728395063" height="34" y="6" style="fill:rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="11.153485082304528" height="38" y="2" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="19.73308899176955" height="20" y="20" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="28.31269290123457" height="26" y="14" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="36.89229681069959" height="32" y="8" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="45.47190072016461" height="20" y="20" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="54.05150462962963" height="28" y="12" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="62.631108539094654" height="38" y="2" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="71.21071244855968" height="24" y="16" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="79.7903163580247" height="36" y="4" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="88.36992026748972" height="20" y="20" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="96.94952417695474" height="36" y="4" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="105.52912808641976" height="32" y="8" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="114.10873199588478" height="26" y="14" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="122.6883359053498" height="28" y="12" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="131.26793981481484" height="30" y="10" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="139.84754372427983" height="26" y="14" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="148.42714763374488" height="32" y="8" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="157.00675154320987" height="34" y="6" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="165.58635545267492" height="36" y="4" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="174.16595936213992" height="28" y="12" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="182.74556327160496" height="26" y="14" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="191.32516718106996" height="22" y="18" style="fill: rgb(66, 165, 245)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="199.904771090535" height="40" y="0" style="fill: rgb(66, 165, 245)"></rect></g></svg></div>
                            <!-- /bars -->

                        <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
                    </div>
                    <!-- end of col-1 -->

                </div>
                <!-- end of col-1 -->
                
                <div class="col-lg-2 col-md-4 col-sm-6">

                    <!-- start col-2 -->
                    <div class="panel text-center">
                        <div class="panel-body">
                            <div class="heading-elements">
                            
                            </div>

                            <!-- Progress counter -->
                            <div class="content-group-sm svg-center position-relative text-color-custom-2" id="hours-available-progress"><svg width="76" height="76"><g transform="translate(38,38)"><path class="d3-progress-background" d="M0,38A38,38 0 1,1 0,-38A38,38 0 1,1 0,38M0,36A36,36 0 1,0 0,-36A36,36 0 1,0 0,36Z" style="fill: rgb(238, 238, 238);"></path><path class="d3-progress-foreground" filter="url(#blur)" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill: rgb(240, 98, 146); stroke: rgb(240, 98, 146);"></path><path class="d3-progress-front" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill:rgb(239, 83, 80); fill-opacity: 1;"></path></g></svg><h3 class="mt-15 mb-5 uppercase">Remaining Points <Points></Points></h3><span class="span-absolute">{{Auth::user()->points}}</span><div class="uppercase">{{Auth::user()->points}} Points</div></div>
                            <!-- /progress counter -->

                            <!-- Bars -->
                            <div id="hours-available-bars"><svg width="208.484375" height="40"><g width="208.484375"><rect class="d3-random-bars" width="6.005722736625515" x="2.5738811728395063" height="34" y="6" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="11.153485082304528" height="38" y="2" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="19.73308899176955" height="20" y="20" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="28.31269290123457" height="26" y="14" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="36.89229681069959" height="32" y="8" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="45.47190072016461" height="20" y="20" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="54.05150462962963" height="28" y="12" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="62.631108539094654" height="38" y="2" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="71.21071244855968" height="24" y="16" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="79.7903163580247" height="36" y="4" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="88.36992026748972" height="20" y="20" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="96.94952417695474" height="36" y="4" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="105.52912808641976" height="32" y="8" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="114.10873199588478" height="26" y="14" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="122.6883359053498" height="28" y="12" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="131.26793981481484" height="30" y="10" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="139.84754372427983" height="26" y="14" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="148.42714763374488" height="32" y="8" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="157.00675154320987" height="34" y="6" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="165.58635545267492" height="36" y="4" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="174.16595936213992" height="28" y="12" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="182.74556327160496" height="26" y="14" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="191.32516718106996" height="22" y="18" style="fill: rgb(239, 83, 80);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="199.904771090535" height="40" y="0" style="fill: rgb(239, 83, 80);"></rect></g></svg></div>
                            <!-- /bars -->

                        <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
                    </div>
                    <!-- end of col-2 -->

                </div>
                <!-- end of col-2 -->
                
                <div class="col-lg-2 col-md-4 col-sm-6">

                    <!-- start col-3 -->
                    <div class="panel text-center">
                        <div class="panel-body">
                            <div class="heading-elements">
                            
                            </div>

                            <!-- Progress counter -->
                            <div class="content-group-sm svg-center position-relative text-color-custom-3" id="hours-available-progress"><svg width="76" height="76"><g transform="translate(38,38)"><path class="d3-progress-background" d="M0,38A38,38 0 1,1 0,-38A38,38 0 1,1 0,38M0,36A36,36 0 1,0 0,-36A36,36 0 1,0 0,36Z" style="fill: rgb(238, 238, 238);"></path><path class="d3-progress-foreground" filter="url(#blur)" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill: rgb(240, 98, 146); stroke: rgb(240, 98, 146);"></path><path class="d3-progress-front" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill:rgb(92, 107, 192); fill-opacity: 1;"></path></g></svg><h3 class="mt-15 mb-5 uppercase">Points Used</h3><span class="span-absolute">{{$points_used}}</span><div class="uppercase">{{$points_used}} Points</div></div>
                            <!-- /progress counter -->

                            <!-- Bars -->
                            <div id="hours-available-bars"><svg width="208.484375" height="40"><g width="208.484375"><rect class="d3-random-bars" width="6.005722736625515" x="2.5738811728395063" height="34" y="6" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="11.153485082304528" height="38" y="2" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="19.73308899176955" height="20" y="20" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="28.31269290123457" height="26" y="14" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="36.89229681069959" height="32" y="8" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="45.47190072016461" height="20" y="20" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="54.05150462962963" height="28" y="12" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="62.631108539094654" height="38" y="2" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="71.21071244855968" height="24" y="16" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="79.7903163580247" height="36" y="4" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="88.36992026748972" height="20" y="20" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="96.94952417695474" height="36" y="4" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="105.52912808641976" height="32" y="8" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="114.10873199588478" height="26" y="14" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="122.6883359053498" height="28" y="12" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="131.26793981481484" height="30" y="10" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="139.84754372427983" height="26" y="14" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="148.42714763374488" height="32" y="8" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="157.00675154320987" height="34" y="6" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="165.58635545267492" height="36" y="4" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="174.16595936213992" height="28" y="12" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="182.74556327160496" height="26" y="14" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="191.32516718106996" height="22" y="18" style="fill: rgb(92, 107, 192);"></rect><rect class="d3-random-bars" width="6.005722736625515" x="199.904771090535" height="40" y="0" style="fill: rgb(92, 107, 192);"></rect></g></svg></div>
                            <!-- /bars -->

                        <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
                    </div>
                    <!-- end of col-3 -->

                </div>
                <!-- end of col-3 -->
                
                <div class="col-lg-2 col-md-4 col-sm-6">

                    <!-- start col-4 -->
                    <div class="panel text-center">
                        <div class="panel-body">
                            <div class="heading-elements">
                            
                            </div>

                            <!-- Progress counter -->
                            <div class="content-group-sm svg-center position-relative text-color-custom-4" id="hours-available-progress"><svg width="76" height="76"><g transform="translate(38,38)"><path class="d3-progress-background" d="M0,38A38,38 0 1,1 0,-38A38,38 0 1,1 0,38M0,36A36,36 0 1,0 0,-36A36,36 0 1,0 0,36Z" style="fill: rgb(238, 238, 238);"></path><path class="d3-progress-foreground" filter="url(#blur)" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill: rgb(240, 98, 146); stroke: rgb(240, 98, 146);"></path><path class="d3-progress-front" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill:rgb(102, 187, 106); fill-opacity: 1;"></path></g></svg><h3 class="mt-15 mb-5 uppercase">Total QR Code</h3><span class="span-absolute">{{$total_qrcode}}</span><div class="uppercase">{{$total_qrcode}} Qr Codes</div></div>
                            <!-- /progress counter -->

                            <!-- Bars -->
                            <div id="hours-available-bars"><svg width="208.484375" height="40"><g width="208.484375"><rect class="d3-random-bars" width="6.005722736625515" x="2.5738811728395063" height="34" y="6" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="11.153485082304528" height="38" y="2" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="19.73308899176955" height="20" y="20" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="28.31269290123457" height="26" y="14" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="36.89229681069959" height="32" y="8" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="45.47190072016461" height="20" y="20" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="54.05150462962963" height="28" y="12" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="62.631108539094654" height="38" y="2" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="71.21071244855968" height="24" y="16" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="79.7903163580247" height="36" y="4" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="88.36992026748972" height="20" y="20" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="96.94952417695474" height="36" y="4" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="105.52912808641976" height="32" y="8" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="114.10873199588478" height="26" y="14" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="122.6883359053498" height="28" y="12" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="131.26793981481484" height="30" y="10" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="139.84754372427983" height="26" y="14" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="148.42714763374488" height="32" y="8" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="157.00675154320987" height="34" y="6" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="165.58635545267492" height="36" y="4" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="174.16595936213992" height="28" y="12" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="182.74556327160496" height="26" y="14" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="191.32516718106996" height="22" y="18" style="fill: rgb(102, 187, 106)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="199.904771090535" height="40" y="0" style="fill: rgb(102, 187, 106)"></rect></g></svg></div>
                            <!-- /bars -->

                        <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
                    </div>
                    <!-- end of col-4 -->

                </div>
                <!-- end of col-4 -->
                
                <div class="col-lg-2 col-md-4 col-sm-6">

                    <!-- start col-5 -->
                    <div class="panel text-center">
                        <div class="panel-body ">
                            <div class="heading-elements">
                            
                            </div>

                            <!-- Progress counter -->
                            <div class="content-group-sm svg-center position-relative text-color-custom-5" id="hours-available-progress"><svg width="76" height="76"><g transform="translate(38,38)"><path class="d3-progress-background" d="M0,38A38,38 0 1,1 0,-38A38,38 0 1,1 0,38M0,36A36,36 0 1,0 0,-36A36,36 0 1,0 0,36Z" style="fill: rgb(238, 238, 238);"></path><path class="d3-progress-foreground" filter="url(#blur)" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill: rgb(240, 98, 146); stroke: rgb(240, 98, 146);"></path><path class="d3-progress-front" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill:rgb(239, 83, 80); fill-opacity: 1;"></path></g></svg><h3 class="mt-15 mb-5 uppercase">QR Code expired</h3><span class="span-absolute">{{$qrcode_expired}} </span><div class="uppercase">{{$qrcode_expired}} Qr Codes</div></div>
                            <!-- /progress counter -->

                            <!-- Bars -->
                            <div id="hours-available-bars"><svg width="208.484375" height="40"><g width="208.484375"><rect class="d3-random-bars" width="6.005722736625515" x="2.5738811728395063" height="34" y="6" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="11.153485082304528" height="38" y="2" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="19.73308899176955" height="20" y="20" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="28.31269290123457" height="26" y="14" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="36.89229681069959" height="32" y="8" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="45.47190072016461" height="20" y="20" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="54.05150462962963" height="28" y="12" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="62.631108539094654" height="38" y="2" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="71.21071244855968" height="24" y="16" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="79.7903163580247" height="36" y="4" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="88.36992026748972" height="20" y="20" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="96.94952417695474" height="36" y="4" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="105.52912808641976" height="32" y="8" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="114.10873199588478" height="26" y="14" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="122.6883359053498" height="28" y="12" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="131.26793981481484" height="30" y="10" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="139.84754372427983" height="26" y="14" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="148.42714763374488" height="32" y="8" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="157.00675154320987" height="34" y="6" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="165.58635545267492" height="36" y="4" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="174.16595936213992" height="28" y="12" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="182.74556327160496" height="26" y="14" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="191.32516718106996" height="22" y="18" style="fill: rgb(239, 83, 80)"></rect><rect class="d3-random-bars" width="6.005722736625515" x="199.904771090535" height="40" y="0" style="fill: rgb(239, 83, 80)"></rect></g></svg></div>
                            <!-- /bars -->

                        <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
                    </div>
                    <!-- end of col-1 -->

                </div>
                <!-- end of col-1 -->
                
            </div>	
        </section>
        <!-- end of dashboard row 1 -->
</div>	

--}}

@endsection