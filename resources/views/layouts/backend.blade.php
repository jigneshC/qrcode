<!DOCTYPE html>
<html>
<head>
    <title>@yield('title','Home') | {{ config('app.name') }}</title>
    <link href="{{asset('assets/images/favicon.png')}}" rel="icon" type="image" />
    <link href="{{asset('assets/images/favicon.ico')}}" rel='icon' type='image/ico'>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta content='text/html;charset=utf-8' http-equiv='content-type'>
    <meta content='Flat administration template for Twitter Bootstrap. Twitter Bootstrap 3 template with Ruby on Rails support.'
          name='description'>

    <!-- / START - page related stylesheets [optional] -->
    <link rel="stylesheet" href="{!! asset('/assets/stylesheets/plugins/fuelux/wizard.css') !!}">

    <!-- / END - page related stylesheets [optional] -->
    <!-- / bootstrap [required] -->
    <link href="{!! asset('assets/stylesheets/bootstrap/bootstrap.css') !!}" media="all" rel="stylesheet"
          type="text/css"/>
    <link href="{!! asset('assets/stylesheets/plugins/select2/select2.css') !!}" media="all"
          rel="stylesheet"
          type="text/css"/>

    <link href="{!! asset('css/bootstrap-datetimepicker.css') !!}"
          media="all"
          rel="stylesheet"
          type="text/css"/>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <!-- / theme file [required] -->
    <link href="{!! asset('assets/stylesheets/light-theme.css') !!}" media="all" id="color-settings-body-color"
          rel="stylesheet" type="text/css"/>
    <!-- / coloring file [optional] (if you are going to use custom contrast color) -->
    <link href="{!! asset('assets/stylesheets/theme-colors.css') !!}" media="all" rel="stylesheet" type="text/css"/>
    <!-- / demo file [not required!] -->
    <link href="{!! asset('assets/stylesheets/demo.css') !!}" media="all" rel="stylesheet" type="text/css"/>

     {{-- Data Table--}}
     <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
     <link href="//cdn.datatables.net/responsive/2.2.0/css/responsive.dataTables.min.css" rel="stylesheet">

     {{-- Toastr --}}
     <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">

    <style>
        a:hover {
            text-decoration: none;
        }
    </style>

    @yield('headExtra')

    <!--[if lt IE 9]>
    <script src="{!! asset('assets/javascripts/ie/html5shiv.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('assets/javascripts/ie/respond.min.js') !!}" type="text/javascript"></script>
    <![endif]-->
</head>
<body class='contrast-red without-footer'>
<header>
    <nav class='navbar navbar-default'>
        <a class='navbar-brand' href='{!! url('/') !!}'>
           QR Code
        </a>
        <a class='toggle-nav btn pull-left' href='#'>
            <i class='icon-reorder'></i>
        </a>
        <ul class='nav'>
            @if(!Auth::check())
            <li class='dropdown light only-icon'>
                    <a href="{{route('login')}}">Login
                        </a> 
            </li>
            @endif
            {{--
            <li class='dropdown light only-icon'>
                <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                    <i class='icon-cog'></i>
                </a>
                <ul class='dropdown-menu color-settings'>
                    <li class='color-settings-body-color'>
                        <div class='color-title'>Body color</div>
                        <a data-change-to='{!! asset('assets/stylesheets/light-theme.css') !!}' href='#'>
                            Light
                            <small>(default)</small>
                        </a>
                        <a data-change-to='{!! asset('assets/stylesheets/dark-theme.css') !!}' href='#'>
                            Dark
                        </a>
                        <a data-change-to='{!! asset('assets/stylesheets/dark-blue-theme.css') !!}' href='#'>
                            Dark blue
                        </a>
                    </li>
                    <li class='divider'></li>
                    <li class='color-settings-contrast-color'>
                        <div class='color-title'>Contrast color</div>
                        <a data-change-to="contrast-red" href="#"><i class='icon-cog text-red'></i>
                            Red
                            <small>(default)</small>
                        </a>

                        <a data-change-to="contrast-blue" href="#"><i class='icon-cog text-blue'></i>
                            Blue
                        </a>

                        <a data-change-to="contrast-orange" href="#"><i class='icon-cog text-orange'></i>
                            Orange
                        </a>

                        <a data-change-to="contrast-purple" href="#"><i class='icon-cog text-purple'></i>
                            Purple
                        </a>

                        <a data-change-to="contrast-green" href="#"><i class='icon-cog text-green'></i>
                            Green
                        </a>

                        <a data-change-to="contrast-muted" href="#"><i class='icon-cog text-muted'></i>
                            Muted
                        </a>

                        <a data-change-to="contrast-fb" href="#"><i class='icon-cog text-fb'></i>
                            Facebook
                        </a>

                        <a data-change-to="contrast-dark" href="#"><i class='icon-cog text-dark'></i>
                            Dark
                        </a>

                        <a data-change-to="contrast-pink" href="#"><i class='icon-cog text-pink'></i>
                            Pink
                        </a>

                        <a data-change-to="contrast-grass-green" href="#"><i class='icon-cog text-grass-green'></i>
                            Grass green
                        </a>

                        <a data-change-to="contrast-sea-blue" href="#"><i class='icon-cog text-sea-blue'></i>
                            Sea blue
                        </a>

                        <a data-change-to="contrast-banana" href="#"><i class='icon-cog text-banana'></i>
                            Banana
                        </a>

                        <a data-change-to="contrast-dark-orange" href="#"><i class='icon-cog text-dark-orange'></i>
                            Dark orange
                        </a>

                        <a data-change-to="contrast-brown" href="#"><i class='icon-cog text-brown'></i>
                            Brown
                        </a>

                    </li>
                </ul>
            </li>
            --}}

            {{--@include('partials.language')--}}

          {{--  @include('partials.notifications') --}}

            <li class='dropdown dark user-menu'>
                <a class='dropdown-toggle' data-toggle='dropdown' href='#'>

                    @if(isset(Auth::user()->people->photo))
                        <img width="23" height="23" alt="{!! Auth::user()->name !!}"
                             src="{!! asset('uploads/'.Auth::user()->people->photo) !!}"/>
                    @endif
                    @if(\Auth::check())
                    <span class='user-name'>{!! Auth::user()->name !!}</span>
                    <b class='caret'></b>
                    
                <ul class='dropdown-menu'>
                    @if(Auth::user()->type == 'admin')
                    <li>
                        <a href='{!! url('/admin/profile') !!}'>
                            <i class='icon-user'></i>
                            Profile
                        </a>
                    </li>
                    @else
                    <li>
                            <a href='{!! url('/dashboard') !!}'>
                                <i class='icon-user'></i>
                                Dashboard
                            </a>
                        </li>
                    <li>
                            <a href='{!! url('/profile') !!}'>
                                <i class='icon-user'></i>
                                Profile
                            </a>
                        </li>
                    @endif
                    <li class='divider'></li>

                    <li>
                        <a href="{{ url('/logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class='icon-signout'></i>
                            Logout
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>

                </ul>
                @endif
            </li>
        </ul>
        {{--<form action='search_results.html' class='navbar-form navbar-right hidden-xs' method='get'>--}}
        {{--<button class='btn btn-link icon-search' name='button' type='submit'></button>--}}
        {{--<div class='form-group'>--}}
        {{--<input value="" class="form-control" placeholder="Search..." autocomplete="off" id="q_header" name="q"--}}
        {{--type="text"/>--}}
        {{--</div>--}}
        {{--</form>--}}
    </nav>
</header>
<div id='wrapper'>
    <div id='main-nav-bg'></div>

    @include('partials.sidebar')
    
    <section id='content'>
        <div class='container'>

            <div class='row' id='content-wrapper'>
                <div class='col-xs-12'>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="page-header">
                                <h1 class="pull-left">
                                    @yield('title','Home')

                                </h1>
                                <div class="pull-right">
                                    <ul class="breadcrumb">
                                        {{--<li>--}}
                                        {{--<a href="index.html">--}}
                                        {{--<i class="icon-bar-chart"></i>--}}
                                        {{--</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="separator">--}}
                                        {{--<i class="icon-angle-right"></i>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                        {{--UI Elements &amp; Widgets--}}
                                        {{--</li>--}}
                                        {{--<li class="separator">--}}
                                        {{--<i class="icon-angle-right"></i>--}}
                                        {{--</li>--}}
                                        {{--<li class="active">UI Elements</li>--}}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    @include('partials.page_notification')

                    @yield('content')


                </div>
            </div>
            <footer id='footer'>
                <div class='footer-wrapper'>
                    <div class='row'>
                        <div class='col-sm-6 text'>
                            Copyright Â© 2013 Your Project Name
                        </div>
                        <div class='col-sm-6 buttons'>
                            <a class="btn btn-link" href="http://www.bublinastudio.com/flatty">Preview</a>
                            <a class="btn btn-link"
                               href="https://wrapbootstrap.com/theme/flatty-flat-administration-template-WB0P6NR1N">Purchase</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </section>
</div>
<!-- / jquery [required] -->
<script src="{!! asset('assets/javascripts/jquery/jquery.min.js') !!}" type="text/javascript"></script>
<!-- / jquery mobile (for touch events) -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{!! asset('assets/javascripts/jquery/jquery.mobile.custom.min.js') !!}" type="text/javascript"></script>
<!-- / jquery migrate (for compatibility with new jquery) [required] -->
<script src="{!! asset('assets/javascripts/jquery/jquery-migrate.min.js') !!}" type="text/javascript"></script>
<!-- / jquery ui -->
<script src="{!! asset('assets/javascripts/jquery/jquery-ui.min.js') !!}" type="text/javascript"></script>
<!-- / jQuery UI Touch Punch -->
<script src="{!! asset('assets/javascripts/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js') !!}"
        type="text/javascript"></script>
<!-- / bootstrap [required] -->
<script src="{!! asset('assets/javascripts/bootstrap/bootstrap.js') !!}" type="text/javascript"></script>


<script src="{!! asset('js/bootstrap-datetimepicker.min.js') !!}"
        type="text/javascript"></script>

<script src="{!! asset('assets/javascripts/plugins/select2/select2.js') !!}" type="text/javascript"></script>
<!-- / modernizr -->

<!-- validate -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

<script src="{!! asset('assets/javascripts/plugins/modernizr/modernizr.min.js') !!}" type="text/javascript"></script>
<!-- / retina -->
<script src="{!! asset('assets/javascripts/plugins/retina/retina.js') !!}" type="text/javascript"></script>
<!-- / theme file [required] -->
<script src="{!! asset('assets/javascripts/theme.js') !!}" type="text/javascript"></script>
<!-- / demo file [not required!] -->
<script src="{!! asset('assets/javascripts/demo.js') !!}" type="text/javascript"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css" rel="stylesheet">

<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js"></script>

{{-- Data Table--}}
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/responsive/2.2.0/js/dataTables.responsive.min.js"></script>
{{-- Toastr --}}
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script>


    $(document).ready(function () {


        $('#flash-overlay-modal').modal();

        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.selectTag').select2({
            tokenSeparators: [",", " "],
        });


        $('.time_pick').datetimepicker({
            format: 'LT'
        });
    });


</script>

<script type="text/javascript">
    $( document ).ready(function() {
        $(document).on('click', '.btn-add', function(e) {
            e.preventDefault();

            var tableFields = $('.table-fields'),
                currentEntry = $(this).parents('.entry:first'),
                newEntry = $(currentEntry.clone()).appendTo(tableFields);

            newEntry.find('input').val('');
            tableFields.find('.entry:not(:last) .btn-add')
                .removeClass('btn-add').addClass('btn-remove')
                .removeClass('btn-success').addClass('btn-danger')
                .html('<span class="fa fa-minus"></span>');
        }).on('click', '.btn-remove', function(e) {
            $(this).parents('.entry:first').remove();

            e.preventDefault();
            return false;
        });

    });
</script>

@yield('footerExtra')
@stack('js')

<!-- / START - page related files and scripts [optional] -->

<!-- / END - page related files and scripts [optional] -->
</body>
</html>