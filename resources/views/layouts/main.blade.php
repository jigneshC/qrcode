<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" lang="en-us"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="en-us"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie10 lt-ie9" lang="en-us"> <![endif]-->
<!--[if IE 9]> <html class="no-js lt-ie10 lt-ie9" lang="en-us"> <![endif]-->
<!--[if lt IE 10]> <html class="no-js lt-ie10" lang="en-us"> <![endif]-->
<!--[if !IE]> > <![endif]-->
<html class='no-js' lang='en'>
<!-- <![endif] -->
<head>
<meta name="description" content="" />
<meta name="author" content="" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<title>@yield('title')</title>

<meta content='initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width' name='viewport' />
<meta content='yes' name='apple-mobile-web-app-capable'>
<meta content='translucent-black' name='apple-mobile-web-app-status-bar-style'>
{{-- <link href="{{asset('assets/images/favicon.png')}}" rel="icon" type="image" />
<link href="{{asset('assets/images/favicon.ico')}}" rel='icon' type='image/ico'> --}}
{{-- <link href="{{ asset('/logo-favicon.png')}}" rel="icon" type="image" size="16x16" /> --}}
<link href="{{ asset('/favicon-qr.ico')}}" rel="icon" type="image"  />

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet"> 

<link href="{{asset('assets/css/mystyle.css')}}" media="all" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/css/bootstrap.min.css')}}" media="all" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/css/font-awesome.min.css')}}" media="all" rel="stylesheet" type="text/css" />

<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">

<script src="{{asset('assets/js/modernizr.js')}}" type="text/javascript"></script>
    <!--[if IE]>
	<script src="js/html5.js"></script>
	<![endif]-->

</head>
<body class="overflowhidden">

<div id="wrapper">

	
	
    @yield('content')
 
    <footer>
    	<div class="footer-div clearfix">
        	 <div class="container">
             <div class="row">
                <span class="footer-txt-1"><p>&copy; QR Code Generator 2018</p></span>
                <span class="footer-txt-2"><p><i class="fa fa-envelope-o"></i> <a href="mailto:support@qrcodelibrary.info">support@qrcodelibrary.info</a> 
                    <!--
                    | 
               <span class="span-phone"><i class="fa fa-phone"></i> <a href="tel:123-456-7890">123-456-7890</a></span></p></span>
                    -->
            </div><!-- end of row -->
             </div><!-- end of container -->           
        </div>
    </footer>


    
<div id="back-to-top"><span><i class="fa fa-arrow-up"></i></span></div> 
</div><!-- end of wrapper -->


<script src="{{asset('assets/js/jquery.min.js')}}" type="text/javascript"></script> 
<script src="{{asset('assets/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/js/custom.js')}}"></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<!--validate-->
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
@stack('js')
<!-- Owl Carousel Assets -->
<link href="{{asset('assets/css/owl.carousel.css')}}" rel="stylesheet">


<script src="{{asset('assets/js/owl.carousel.js')}}"></script>
 <script>
    jQuery(document).ready(function() {
        jQuery("#plan-pricing-sliding").owlCarousel({
    autoPlay: false,
    items : 3,
    navigation:true,
    pagination:false,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [979,3]
  });

});
</script>

{{--  <script type="text/javascript">
    var userLang = navigator.language || navigator.userLanguage; 
    alert ("The language is: " + userLang);
</script>  --}}

<script>
    /*
    jQuery(document).ready(function() {

        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var email = jQuery('#email').val();
        if(reg.test(email) == false){
            alert('uihkjklm')
            jQuery('.error').innerHtml('Please Enter The valid Email Address');
        }else
            return true;
      
    });
    */

    $(document).on('click', '.open_model_for_freeqr', function (e) {
        $('#package_id').html($(this).attr('data-packageid'));
        var formid = $("#modal_form_id");
        $('#modal_id').modal('show');
        $("div").removeClass("modal-backdrop");
    });
    
    $("#modal_form_id").validate({
        rules: {
            email: {
                required: true,
            }
        },
        messages: {
            email: {
                required: "Please Enter the Email Address",
            }
        },
        submitHandler: function (form) {
            var url = "{{url('/freepackage')}}";
            var method = "post"
            $.ajax({
                type: method,
                url: url,
                data: $(form).serialize(),
                beforeSend: function () {
                },
                success: function (result)
                {   
                    if(result.code == 200){
                        if(result.msg != '')
                            toastr.success(result.msg)
                        if(result.new_user_msg != '')
                            toastr.success(result.new_user_msg)
                    }else{
                        toastr.error('Something Went Wrong, Try Again!')
                    }   
                    $('#modal_id').modal('hide');
                },
                error: function (error) {
                    $('#modal_id').modal('hide');
                }
            }); 
            return false;
        }
    });
    
    </script>


</body>
</html>
