
<!-- Free Qr Code Modal -->

<div class="modal" id="modal_id" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="account_form_model_lable">@lang('home.create_a_qr_code_for_free')</h5> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="bidder_list">
                    <div class=" bidder no-padding-left no-padding-right gutter-bottom">
                        
                        <div class=" clearfix details-container details-port-container">
                            <form method="post" id="modal_form_id" name="form">

                                <input  name="package_id"  id="package_id" type="hidden" value=""  >
                    
                                {{csrf_field()}} 

                                @if(Auth::check())
                                    <?php $email = Auth::user()->email ; ?>
                                @endif
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-12">
                                            {!! Form::label('email', trans('order.email')) !!}
                                            {!! Form::text('email', isset($email) ? $email : null , ['class' => 'form-control','id'=>'email']) !!}
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button class="btn btn-primary account_form_submit_button" type="submit" name="submit" value="Cash Add">@lang('order.send_me_my_free_qrcode')</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Free Qr Code Modal  Close-->
