@extends('layouts.frontend')

@section('title',__('order.orders'))

@section('content')

        <!-- order-list -->
            <div class="panel panel-flat">
                
                    <div class="panel-heading">
                        <h5 class="panel-title order">@lang('order.orders')</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body-1 clearfix">
                        <div class="col-lg-12 pull-right padding-10">
                            <div class="input-group">
                                    {!! Form::open(['method' => 'GET', 'url' => '/orders', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                                    <div class="input-group" style="max-width: 300px">
                                        <input type="text" class="form-control searchbtn" name="search" placeholder=@lang('order.search') value="{!! request()->get('search') !!}">
                                        <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </span>
                                    </div>
                                    {!! Form::close() !!} 
                            </div>
                       </div>
                    </div>
 

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr class="bg-blue">
                                    <th>@lang('order.id')</th>
                                    <th>@lang('order.order_placed_by')</th>
                                    <th>@lang('order.points')</th>
                                    <th>@lang('order.package_title')</th>
                                    <th>@lang('order.order_amount')</th>
                                    <th>@lang('order.order_status')</th>
                                    <th class="text-center">@lang('order.actions')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($orders) <= 0)
                                    <tr>
                                        <td colspan=8>@lang('order.no_orders_found')</td>
                                    </tr>
                                @endif
                                @foreach($orders as $item)
                                <?php $package_detail = json_decode($item->package_obj) ;
                                $user_detail = json_decode($item->user_obj) ; ?>
                                <tr>
                                    <td>{{$item->id}}</td>
                                    <td>{{ $user_detail->name }}</td>
                                    <td>{{ $package_detail->qr_code_no }}</td>
                                    <td>{{ $package_detail->title }}</td>
                                    <td>{{ $item->user_order_amount}} {{ $item->user_order_currency }}</td>
                                    <td>{{ $item->order_status }} </td>
                                    
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                 <a class="btn btn-blue" href="{{ url('/orders/' . $item->id) }}" ><i class="fa fa-eye"></i> @lang('order.view')</a>
                                            </li>
                                        </ul> 
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                    
                    <div class="panel-body-1 clearfix">
                        <div class="col-lg-12 pull-right padding-10">
                        <div class="col-lg-12 bg-white-custom-p">
                            <div class="pagination-div">
                                    {!! $orders->appends(['search' => Request::get('search')])->render() !!}
                            </div>

                        </div>
                        
                       </div>
                    </div>
                
            </div><!-- panel -->
            
          


				
@endsection
