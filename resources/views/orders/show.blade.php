@extends('layouts.frontend')

@section ('title',__('order.order'))

@section('content')


<div class="panel panel-flat">
        
            <div class="panel-heading">
                <h5 class="panel-title order">@lang('order.order_details')</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body-1 clearfix">
                <div class="col-lg-12 pull-right padding-10">
                    <div class="input-group">
                        
                            @if(Auth::check())
                            <a href="{{ url('/orders') }}" title="Back"><button class="btn btn-blue"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('order.back')</button></a>
                            @endif
                            @lang('order.order') #{{ $order->id }}                  
                </div>
            </div>
        </div>

        <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tbody>
                        <tr>
                            <th>@lang('order.order_id')</th><td>{{ $order->id }}</td>
                        </tr>
                        <tr>
                            <th>----------@lang('order.user_details')----------</th><td></td>
                        </tr>
                        <tr>
                            <th>@lang('order.user_name')</th><td>{{ $order->user->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('order.user_email')</th><td>{{ $order->user->email }}</td>
                        </tr>
                        <tr>
                            <th>@lang('order.user_phone')</th><td>{{ $order->user->phone }}</td>
                        </tr>
                        <tr>
                            <th>----------@lang('order.item_details')----------</th><td></td>
                        </tr>
                        <tr>
                            <th>@lang('order.package_name')</th><td>{{ $order->package_name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('order.package_price')</th><td>{{ $package_price['amount'] }} {{ $package_price['toCode'] }}</td>
                        </tr>
                        <tr>
                            <th>@lang('order.points')</th><td>{{ $order->package->qr_code_no }}</td>
                        </tr>
                        <tr>
                            <th>----------@lang('order.order_details')----------</th><td></td>
                        </tr>
                        <tr>
                            <th>@lang('order.order_quantity')</th><td>{{ $order->order_qty }}</td>
                        </tr>
                        <tr>
                            <th>@lang('order.order_amount')</th><td>{{ $order_amount['amount'] }} {{ $order_amount['toCode'] }}</td>
                        </tr>
                        <tr>
                            <th>@lang('order.order_discount')</th><td>{{ $order->order_discount }}</td>
                        </tr>
                        <tr>
                            <th>@lang('order.order_net_amount')</th><td>{{ $order_net_amount['amount'] }} {{ $order_net_amount['toCode'] }}</td>
                        </tr>
                        <tr>
                            <th>@lang('order.order_status')</th><td>{{ $order->order_status }}</td>
                        </tr>
                        @if($order->payment_type != "free")
                        <tr>
                            <th>----------@lang('order.payment_details')----------</th><td></td>
                        </tr>
                        <tr>
                            <th>@lang('order.payment_type')</th><td>{{ $order->payment_type }}</td>
                        </tr>
                        <tr>
                            <th>@lang('order.payment_date')</th><td>{{ $order->created_at }}</td>
                        </tr>
                        <tr>
                            <th>@lang('order.payment_reference_id')</th><td>{{ $order->payment_reference_id }}</td>
                        </tr>
                        <tr>
                            <th>@lang('order.payment_request_id')</th><td>{{ $order->payment_request_id }}</td>
                        </tr>
                        <tr>
                            <th>@lang('order.payment_status')</th><td>{{ $order->payment_status }}</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        

    </div>
    <!-- panel -->
@endsection
