@if (Session::has('flash_message'))
<div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert"
            aria-hidden="true">&times;</button><i class="fa fa-hand-o-up"></i> 
    {{ Session::get('flash_message') }}
</div>
@endif
@if (Session::has('flash_success'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert"
            aria-hidden="true">&times;</button><i class="fa fa-thumbs-up"></i>
    {{ Session::get('flash_success') }}
</div>
@endif
@if (Session::has('flash_warning'))
<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert"
            aria-hidden="true">&times;</button><i class="fa fa-bolt"></i>
    {{ Session::get('flash_warning') }}
</div>
@endif
@if (Session::has('flash_error'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert"
            aria-hidden="true">&times;</button><i class="fa fa-warning">
    {{ Session::get('flash_error') }}
</div>
@endif
@if (Session::has('success'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert"
            aria-hidden="true">&times;</button><i class="fa fa-thumbs-up"></i>
    {{ Session::get('success') }}
</div>
@endif
@if (Session::has('error'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert"
            aria-hidden="true">&times;</button><i class="fa fa-warning">
    {{ Session::get('error') }}
</div>
@endif