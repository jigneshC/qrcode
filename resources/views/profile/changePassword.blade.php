@extends('layouts.frontend')


@section('title',__('profile.change_password'))

@section('content')

				
					<!-- Form horizontal -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">@lang('profile.change_password')</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							
							<div class="col-md-6">
								{!! Form::open([
                                    'method' => 'PATCH',
                                    'class' => 'form-data'
                                ]) !!}

                                    <div class="form-group{{ $errors->has('current_password') ? ' has-error' : ''}}">
                                        {!! Form::label('current_password', trans('profile.current_password')) !!}
                                        {!! Form::password('current_password', ['class' => 'form-control','required'=>'required']) !!}
                                        {!! $errors->first('current_password', '<p class="help-block">:message</p>') !!}
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
                                        {!! Form::label('password',trans('profile.new_password')) !!}    
                                        {!! Form::password('password', ['class' => 'form-control','required'=>'required']) !!}
                                        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                                    </div>

                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : ''}}">
                                        {!! Form::label('password_confirmation', trans('profile.confirm_password')) !!}    
                                        {!! Form::password('password_confirmation', ['class' => 'form-control','required'=>'required']) !!}
                                        {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}    
                                    </div>

                                    <div class="text-right">
                                        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('profile.change_password'), ['class' => 'btn btn-1 pull-left']) !!}
                                    </div>
                                    
                                {!! Form::close() !!}
							</div>
						</div>
					</div>

@endsection