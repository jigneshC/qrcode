@extends('layouts.frontend')

@section('title',__('profile.edit_profile'))

@section('content')
    
<!-- Form horizontal -->
<div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">@lang('profile.edit_profile')</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            
            <div class="col-md-6">
                {!! Form::model($user,[
                    'method' => 'PATCH',
                    'class' => 'form-data',
                    'files'=>true
                ]) !!}
                    {{--
                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif 
                --}}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
                        {!! Form::label('name', trans('profile.name_*') )!!}
                        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
                        {!! Form::label('email', trans('profile.email_*')) !!}
                        {!! Form::email('email', isset($user->email)?$user->email:old('email'), ['class' => 'form-control', 'required' => 'required','disabled'=>true]) !!}
                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : ''}}">
                        {!! Form::label('phone', trans('profile.phone_*'))!!}
                        {!! Form::text('phone', isset($user->phone)?$user->phone:old('phone'), ['class' => 'form-control', 'required' => 'required']) !!}
                        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="text-right">
                        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('profile.update_profile'), ['class' => 'btn btn-1 pull-left']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
</div>                       
                            


@endsection