@extends('layouts.frontend')

@section('title',__('profile.my_profile'))

@section('content')

<style>
        .switch {
          position: relative;
          display: inline-block;
          width: 60px;
          height: 34px;
        }
        
        .switch input {display:none;}
        
        .slider {
          position: absolute;
          cursor: pointer;
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          background-color: #ccc;
          -webkit-transition: .4s;
          transition: .4s;
        }
        
        .slider:before {
          position: absolute;
          content: "";
          height: 26px;
          width: 26px;
          left: 4px;
          bottom: 4px;
          background-color: white;
          -webkit-transition: .4s;
          transition: .4s;
        }
        
        input:checked + .slider {
          background-color: #2196F3;
        }
        
        input:focus + .slider {
          box-shadow: 0 0 1px #2196F3;
        }
        
        input:checked + .slider:before {
          -webkit-transform: translateX(26px);
          -ms-transform: translateX(26px);
          transform: translateX(26px);
        }
        
        /* Rounded sliders */
        .slider.round {
          border-radius: 34px;
        }
        
        .slider.round:before {
          border-radius: 50%;
        }
        </style>


<div class="panel panel-flat">
        
            <div class="panel-heading">
                <h5 class="panel-title order">@lang('profile.my_profile')</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body-1 clearfix">
                <div class="col-lg-12 pull-right padding-10">
                    <div class="input-group">
                        <a href="{{ url('/profile/edit') }}" class="btn btn-blue"
                            title="Edit Profile">
                             <i class="fa fa-edit" aria-hidden="true"></i> @lang('profile.edit_profile')
                        </a>
                        <a href="{{ url('/profile/change-password') }}" class="btn btn-blue"
                            title="Change Password">
                             <i class="fa fa-lock" aria-hidden="true"></i> @lang('profile.change_password')
                        </a>              
                </div>
            </div>
        </div>

        <div class="table-responsive">
                
                 <table class="table table-borderless">

                     <tr>
                         <td class="col-md-2">@lang('profile.name')</td>
                         <td>{{$user->name}}</td>
                     </tr>

                     <tr>
                         <td>@lang('profile.email')</td>
                         <td>{{$user->email}}</td>
                     </tr>
                     <tr>
                         <td>@lang('profile.phone')</td>
                         <td>{{$user->phone}}</td>
                     </tr>

                     {{-- <tr>
                         <td>@lang('profile.photo')</td>
                         <td> 
                             @if($user->image)
                                 <div class="col-md-6">
                                     <img src="{!! asset('/images/'.$user->image) !!}" alt="" width="150px">
                                 </div>
                             @endif
                         </td>
                     </tr> --}}

                     <tr>
                         <td>@lang('profile.joined')</td>
                         <td>{{$user->created_at->diffForHumans()}}</td>
                     </tr>

                     <tr>
                         <td>Auto Renewable Qrcodes When Expired</td>
                         <td>
                             <label class="switch">
                                <input type="checkbox" name="renew" class="renew" @if($user->renew=='on') checked value="off" @else unchecked value="on" @endif>

                                {{-- <input type="checkbox" id="renew" name="renew"  class="renew" {{ old('renew') || $user->renew ? 'checked' : ''  }} /> --}}

                                <span class="slider round"></span>
                              </label>
                             
                        </td>
                     </tr>


                 </table>
             </div>
        

    </div>
    <!-- panel -->
@endsection

@push('js')
<script>
    $(".renew").click(function(){
        var method="GET";
        var url = "{{ url('/profile') }}";
        console.log(url);
        var renew = $(this).val();
        $.ajax({
                type: method,
                url: url,
                data: {renew: renew },
                beforeSend: function () {
                },
                success: function (result)
                {   
                    location.reload();    
                },
                error: function (error) {
                   
                }
            }); 
            return false;
        
    });


   
</script>

@endpush