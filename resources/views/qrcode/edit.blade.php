@extends('layouts.frontend')
@section('title',__('qrcode.edit_qr_code'))

@section('content')

<!-- Form horizontal -->
<div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">
                <a href="{{ url('/qrcode') }}" title="Back"><button class="btn btn-blue"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('qrcode.back')</button></a>
                @lang('qrcode.edit_qr_code')
            </h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            
            {{--  <div class="col-md-12 clearfix">  
                    <a href="{{ url('/qrcode') }}" title="Back"><button class="btn btn-blue"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>      
            </div>  --}}
            <div class="col-md-6 col-sm-12 col-xs-12 pull-right">
                <div class="row-1 size text-center">
                    <img src="{!! asset('uploads/qrcode') !!}/{{ $qrcode->hash }}.jpg" alt="barcode"  height="300" />
                </div>

            </div>
            
            <div class="col-md-6 col-sm-12 clearfix">

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
                
                {!! Form::model($qrcode, [
                    'method' => 'PATCH',
                    'url' => ['/qrcode', $qrcode->id],
                    'class' => 'form-horizontal qr_form',
                    'id' => 'form_id',
                    'files' => true
                ]) !!}

                @include ('qrcode.form', ['submitButtonText' => 'Update','action'=>'edit'])

                {!! Form::close() !!}

               

                
            </div>
        </div>
</div>

@endsection


