@extends('layouts.frontend')
@section('title',__('qrcode.view_qr_code'))
@section('content')

 <div class="panel panel-flat">
        
            <div class="panel-heading">
                <h5 class="panel-title order">@lang('qrcode.qrcode')</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body-1 clearfix">
                <div class="col-lg-12 pull-right padding-10">
                    <div class="input-group">
                        
                            <a href="{{ url('/qrcode') }}" title="Back"><button class="btn btn-blue"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('qrcode.back')</button></a>
                            @if($qrcode->expiry_date >= $today_date )
                            <a href="{{ url('/qrcode/' . $qrcode->id . '/edit') }}" title="Edit Qrcode"><button class="btn btn-blue"><i class="fa fa-edit" aria-hidden="true"></i> @lang('qrcode.edit') </button></a>
                            @endif                    
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/qrcode', $qrcode->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-blue',
                                        'title' => 'Delete Qrcode',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                ))!!}
                            {!! Form::close() !!}                      
                </div>
            </div>
        </div>

        <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tbody>
                        <tr>
                            <th>@lang('qrcode.id')</th><td>{{ $qrcode->id }}</td>
                        </tr>
                        <tr><th>@lang('qrcode.name') </th><td> {{ $qrcode->name }} </td></tr>
                        <tr><th> @lang('qrcode.hash_code') </th><td> {{ $qrcode->hash }} </td></tr>
                        <tr><th> @lang('qrcode.redirect_link') </th><td> {{ $qrcode->redirectlink }} </td></tr>
                        @if( $qrcode->expiry_date >= $today_date )
                        <tr><th> @lang('qrcode.qrcode')</th> <td> <img src="{!! asset('uploads/qrcode') !!}/{{ $qrcode->hash }}.jpg" alt="barcode"  height="300" /> </td></tr>
                        @endif
                        <tr>
                                <th>@lang('qrcode.created_on')</th><td>{{ $qrcode->created_at->format('Y-m-d') }}</td>
                        </tr>
                        <tr>
                                <th>@lang('qrcode.expiry_date')</th><td>{{ $qrcode->expiry_date}}</td>
                        </tr>
                        @if( $qrcode->expiry_date < $today_date )
                        <tr>
                                <th><label class="error">@lang('qrcode.qrcode_expired')</label></th><td></td>
                        </tr>
                        @endif
                    </tbody>
                </table>
        </div>
        

    </div>
    <!-- panel -->

@endsection
